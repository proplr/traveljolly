<?php
 
 require("search/iSDK/isdk.php");
 $app = new iSDK;
 $app->cfgCon("connection");

// get the HTTP method, path and body of the request
$method = $_SERVER['REQUEST_METHOD'];
//$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));

if (isset($_GET["action"]))
{
  switch ($_GET["action"])
    {
        case "searchByPhoneNumber":
            searchByPhoneNumber($app);
            break;
        case "searchByEmail":
            searchByEmail($app);
            break;
        case "updateContact":
            updateContact($app);
            break;
        case "addContact":
            addContact($app);
            break;
    }
}

function searchByPhoneNumber($app) 
{
    // InfusionSoft will always return the phone 
    // number formated as (xxx) xxx-xxxx so we 
    // need to strip what's entered & format it 
    // for the search
    // There's no match on less than 10 digits
    $input = json_decode(file_get_contents('php://input'),true);
    $ph = $input["phonenumber"];
    $ph = str_replace('-', '', $ph); 
    $ph = preg_replace('/[^0-9]/','',$ph);
    // This may be overkill, a hacked preg_replace
    // may have been enough
    if(strlen($ph) > 10) {
        $countryCode = substr($ph, 0, strlen($ph)-10);
        $areaCode = substr($ph, -10, 3);
        $nextThree = substr($ph, -7, 3);
        $lastFour = substr($ph, -4, 4);
        //	$ph = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour; // leave out country code
            $ph = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($ph) == 10) {
        $areaCode = substr($ph, 0, 3);
        $nextThree = substr($ph, 3, 3);
        $lastFour = substr($ph, 6, 4);
            $ph = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($ph) == 7) {
        $nextThree = substr($ph, 0, 3);
        $lastFour = substr($ph, 3, 4);
            $ph = $nextThree.'-'.$lastFour;
        }

    searchPhone($app, $ph);
}
function searchPhone($app, $phone) {
    $returnFields = array('Id','FirstName','LastName','Email','Phone1','Groups');
    $query = array('Phone1' => $phone);
    $contacts = $app->dsQuery("Contact",10,0,$query,$returnFields);
        
    // $myObj = new stdClass();
    // $myObj->count = count($contacts);
    // $myObj->name = "John";
    // $myObj->age = 30;
    // $myObj->city = "New York";
    // $myObj->method = $method;
    // $myObj->phonenumber = $input["phonenumber"];
    // $myJSON = json_encode($myObj);
    // echo $myJSON;
    returnData($contacts);
}
function searchByEmail($app) 
{
    $input = json_decode(file_get_contents('php://input'),true);
    $em = $input["email"];
    searchEmail($app, $em);
}
function searchEmail($app, $email) {
    $returnFields = array('Id','FirstName', 'LastName','Email','Phone1','Groups');
    $contacts = $app->findByEmail($email, $returnFields);
    returnData($contacts);
}
function updateContact($app) 
{
    $input = json_decode(file_get_contents('php://input'),true);
    $id = $input["id"];
    $fname = $input["fname"];
    $lname = $input["lname"];
    $phone1 = $input["phone1"];
    $phone1Type = "Mobile";
    if (isset($input["email"]))
        $email = $input["email"];
    //$groups = $input["Groups"];
    //$phone1Type = $input["phone1Type"];
    $update = array('Phone1' => $phone1,
                    'Phone1Type' => $phone1Type,
					'FirstName' => $fname,
                    'LastName' => $lname);
    $updateCon = $app->dsUpdate("Contact", $id, $update);
    searchEmail($app, $email);
}
function addContact($app) {
    //check if email exists first
    // The last variable determines how you 
	// want to match records. In this case 
	// I only matched based on 'Email', but 
	// we could have used 'EmailAndName', or
    // 'EmailAndNameAndCompany'
    $input = json_decode(file_get_contents('php://input'),true);
    $fname = $input["fname"];
    $lname = $input["lname"];
    $phone1 = $input["phone1"];
    $phone1Type = "Mobile";
    $email = $input["email"];
     //$groups = $input["Groups"];
     $update = array('FirstName' => $fname, 
     'LastName' => $lname, 
     'Email' => $email, 
     'Phone1' => $phone1,
     'Phone1Type' => $phone1Type);
    $app->addWithDupCheck($update, 'Email');
    searchEmail($app, $email);//to get the added record by email
}
function addTag($app) {
    $input = json_decode(file_get_contents('php://input'),true);
    $contactId = $fname = $input["id"]; 
    $tagId = $fname = $input["tagId"]; 
    $result = $app->grpAssign($contactId, $tagId);
}
function returnData($data) {
    if (is_array($data)){
        if (empty($data)) {
            echo json_encode(array(array("nodata" => $_GET["action"] . " failed to return data")));
        }
        else {
            echo json_encode($data);
        }
    }
    else {
        echo json_encode(array(array("error" => $_GET["action"] . " failed to retrieve data")));
    }
}
?>	