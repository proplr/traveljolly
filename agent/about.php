<?php
include('admin.php');

$obj=new Admin();


 $username = $_SESSION['username'];

 // echo $username;
 // exit();

if(!$username)
{
   echo '<script type="text/javascript">window.location ="login.php";</script>';
}


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
  <meta name="description" content="Free Web tutorials">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">
  <meta name="author" content="John Doe">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>About Us</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="shortcut icon" type="images/x-icon" href="about/img/favicon.png">
		<link rel="shortcut icon" type="images/x-icon" href="about/images/favicon.png">

  <link rel="stylesheet" type="text/css" href="about/css/about.css">
</head>
<body>
	<div class="container-fluid">
		 <div class="page-about">
		 	<h1>ABOUT US</h1>
		 	<div class="about_button">
		 		<a href="index.php">Back To Home</a>
		 	</div>
		 </div>
		 <div class="page-about-1">
		 	<p>Well let’s hope this helps! Understanding the story of us, as TravelJolly.com (TJ). This could take a while, bend your mind and make you scratch your head as ultimately, we are all simply an eclectic voice of likeminded, citizens bound together as teammates for social good.</p>

		 </div>
		</br>
		 <div class="page-about-2">
		 	<h3>TJ was designed for us, as in all of us, the masses, that includes you! It’s about community!</h3>
		 </div>
		 <div class="page-about-3">
		 	<p>     &#9679;      The Bible says, “You cannot help others without helping yourself.” So, with pure hearts for social good TJ was born on the principle of helping others and their communities.</p>
		 </div>
		 <div class="page-about-4">
		 	<p>Traveling and really enjoying life is important! We want you to enjoy this life on earth. “Life’s not a rehearsal”. You are very valuable to the community and we hope you take the time to learn more about TJ and how we can help you. We provide the tools for you to help yourself and others. You are generously compensated for your time and efforts.</p>
		 </div>
		 <div class="page-about-5">
		 	<p><strong>For those of you</strong> who have the resources to travel and enjoy life freely; Join us, we will save you a lot of time, save you a lot of money, enhance your life experiences and you will have more of them.</p>
		 	<p><strong>For those of you</strong> that do not travel or enjoy your life enough simply understand, we will help you travel more in life.  We will not waste your time. We can help you make a lot of money, save you a lot of money, and save you a lot of time. We have many ways to generously compensate you for any and all of your time and efforts.</p>
		 </div>
		 <div class="page-about-6">
		 	<p>&#9679; 	We are launching TJ and giving of ourselves to help others.</p>
		 	<p>&#9679; 	We are taking the first step and simply want to #ShareTheLove.</p>
		 </div>
		 <div class="page-about-7">
		 	<p>TJ is currently introducing itself by providing you, with the support of someone you know, by way of a no obligation complimentary $249 Booking Engine Gift Card now. The Gift Card is real and is used just like cash.  We are in hopes you simply take a look at how awesome TJ is. Read through our site to understand us, maybe watch a video and then #ShareTheLove with your friends, family and the world.</p>
		 	<p>(To make a friend you must first be a friend. If you want to put a smile on someone’s face and show them you care, there is no easier way then to send them a Gift Card… That always works!)</p>

		 </div>
		 <div class="page-about-8">
		 	<p>When you <strong>#ShareTheLove</strong> you are providing family, friends and others you know with $249 Gift Cards.  The Gift Cards are exactly like the Gift Card you may have received. The Cards you send will be unique as we want to make sure you get paid for supporting TJ’s Growth!</p>
		 	<p>&#9679; As a TJ Member you earn $50 for each full member when you #ShareTheLove! </p>
		 </div>
		</br>
		 <div class="page-about-9">
		 	<h3>When you are ready for our services we will be there!<h3>
		 		<p class="tj-paragraph">TJ asks that if you know your community, the good, the great and where those hidden gems are we simply pay you to share that wealth of knowledge about your local community. You help others to understand your community. You and the community benefit in multiple ways.</p>
		 </div>
		 <div class="page-about-10">
		 	<p>&#9679; Get paid to utilize our Power Engine Trip Planner. Our Power Engine makes it easy for you to create itineraries with built in treasure maps. You get paid when visitors utilize, select a pre-made itinerary that you’ve created, and pick up on the hidden gems you sprinkle within them, even if they customize it!</p>
		 	<p>&#9679; Get paid, helping us grow by Nudging & Incentivizing Businesses you know.</br><i>(Businesses where you have personally enjoyed the products and services they offer.)</i></p>
		 	<p>If you want to know more about how to easily make even more money with TJ <a href="http://traveljolly.com/agent/coming_soon_2a.php" style="text-decoration:none;" target="_blank">Click Here.</a></p>
		 </div>
		</br>
		 <div class="page-about-11">
		 	<h3>Back to more about us:</h3>
		 	<p>We are a bridge that connects the community together, a voice for social good!</p>
		 	<p>We are a group of giant hearted nerds of all types, ages, and backgrounds. Included are the tech guys who have played for those giant companies everyone knows, the bean counters who keep track and make sure there’s a nickel left, the cheesy group of know it all’s within the travel industry having supported billions of dollars in sales over the last few decades and a multitude of tree huggers and entrepreneurial global business development types, all gathered together as teammates. Ultimately a unique cast of characters wanting to do our part in bringing communities closer together to enhance the lives of each and every soul within the community.</p>
		 	<p>We are honored to be associated with the <a href="http://www.rgreenconsortium.com/" target="_blank">rGreen Consortium</a>. We find every initiative the consortium is in pursuit of to be a very worthy initiative we stand behind.</p>
		 	<p>&#9679; Combined the initiatives protect all the children in a community, provide each child a fully funded higher education and save the planet all in one. Really?</p>
		 </div>
		 <div class="page-about-12">
		 	<p><strong>Yes;</strong> Nothing could be better than that, other than humbly traveling the planet as you wish, emptying your bucket list, and in doing so at the same time with just a little extra effort having it all paid for.</p>
		 	<p><i>(As a byproduct without any additional effort you become a big supporter in helping to save the planet while also protecting and providing every child within your community a fully funded higher education.)</i></p>
		 	<p>&#9679; Needless to say: TJ is fully supportive and will use every resource at its disposal to help fund the <a href="http://www.rgreenconsortium.com/" target="_blank">rGreen Consortium</a> Initiatives.</p>
		 </div>
		 <div class="page-about-13">
		 	<p>There is a lot of synergy between TJ & The <a href="http://www.rgreenconsortium.com/" target="_blank">rGreen Consortium</a> both have common interest in the need to recruit top societal influencers. Top societal influencers to be “Ambassadors”. Ambassadors that among other things become a presence in the local community by attending community meetings and events. These Ambassadors are Building and organizing resources that are shared between citizens, local business, Local industry, and Local government. rGreen initiates conversations and initiatives that create Crowd (funded) economies within each local community. One by one each community becomes a global movement to redefine success in business enriching every life.</p>
		 	<p>We have been granted permission to share in and adopt the <a href="http://www.rgreenconsortium.com/" target="_blank">rGreen Consortium’s</a> Mission Statement. It will now also be used as our own. An epic Mission Statement that to the soul of TJ makes so much sense, provides a worthwhile goal and a rather epic mission!</p>
		 	<P><strong>Mission Statement:</strong> “To protect a parent’s most valuable investment – Their children; and the community’s most valuable investment – its future!”</P>
		 </div>
		 <div class="page-about-14">
		 	<p>TJ, the <a href="http://www.rgreenconsortium.com/" target="_blank">rGreen Consortium</a> and our closest relationships are all working on initiatives that will soon pay you for everything you do in today’s connected world. Whether you do it on our site(s), on your site, on 3rd party sites, in apps, data of all types from both the online and offline world, your rewards, your awards, your miles, you name it we simply do not care where the data is from.</p>
		 	<p>Big business has made trillions selling your data. Big business treats data as a commodity and so do we. The difference is we plan to give those trillions of dollars back to you.</p>
		 	<p>Recognizing the true value of an individual’s data and paying them for it is completely different than how the mass of businesses out in the world do business, we are different. We first ask for your permission to use specific types of data coming from your everyday life. We plan to instantly compensate you for your time, efforts, data and even the garbage coming from your home among other things. You have value and big business has kept it from you, huge value in all you do every day. It is time you get paid for everything you do, everything!</p>
		 </div>
		 <div class="page-about-15">
		 	<p>&#9679;TravelJolly.com is not just about traveling and really enjoying life it’s also about giving of yourself and getting paid for your time and efforts.</p>
		 	<p>&#9679;You can simply profit by saving yourself a lot of time and money.</p>
		 	<p>&#9679;We desire to simply help everyone have a better life with better times!</p>
		 </div>
		 <div class="page-about-16">
		 	<p>We dream that one day all companies will compete, not only to be the best in the world, but the best for the world. Together we are producing disruptive revolutionary processes. This is just the beginning of some of the rocket fuel initiatives we have coming to you…!!!</p>
		 	<p>We need your help, currently we would like you to take the first step and do one thing, Get the <strong>“Travel Jolly Prime”</strong> App & <strong>#ShareTheLove!</strong></p>
		 </div>
		 <div class="col-md-4">
		 	
		 </div>
		 <div class="col-md-2">
		 	<div class="google_play">
		 	<img src="about/images/google_play.png">
		 	</div>
		 </div>
		 <div class="col-md-2">
		 	<div class="App_Store">
		 		<img src="about/images/App_store.png">
		 	</div>
		 </div>
		 <div class="clearfix"></div>
		 <div class="col-md-4">
		 	<h3>Thank you, Team TJ</h3>
		 </div>
		</div>
	<!-- Start of REVE Chat Script-->
	<script type='text/javascript'>
    window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account='9571569';w.__revechat_version=2;
      r._= []; var rc = d.createElement('script'); rc.type = 'text/javascript'; rc.async = true; rc.setAttribute('charset', 'utf-8');
      rc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.revechat.com/widget/scripts/new-livechat.js?'+new Date().getTime();
      var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(rc, s);
    })(document, window);
    </script>
    <!-- End of REVE Chat Script -->
</body>
</html>