<?php
include('admin.php');

$obj=new Admin();



if(isset($_POST['submit']))
{

 $chk_user=$obj->chk_user($_POST);

 $username = $_SESSION['username'];

 // echo $username;
 // exit();

if($username)
{
   echo '<script type="text/javascript">window.location ="index.php";</script>';
}
else
{
	 echo '<script type="text/javascript">alert("Please Enter Correct Username And Password");window.location ="login.php""</script>';
}


}



?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
  <meta name="description" content="Free Web tutorials">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">
  <meta name="author" content="John Doe">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Agent | Login</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="shortcut icon" type="images/x-icon" href="about/img/favicon.png">
		<link rel="shortcut icon" type="images/x-icon" href="about/images/favicon.png">

		<style>
body {font-family: Arial, Helvetica, sans-serif;}
form {border: 3px solid #f1f1f1;}

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}
</style>

</head>
<body>
<form method="post">
  <div class="imgcontainer">
    <img src="img/logo-tj-new.png">
  </div>

  <div class="container">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <label for="uname"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="uname" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required>
        
    <button type="submit" name="submit">Login</button>
    </div>
    </div>

 
</form>
<!-- Start of REVE Chat Script-->
<script type='text/javascript'>
    window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account='9571569';w.__revechat_version=2;
      r._= []; var rc = d.createElement('script'); rc.type = 'text/javascript'; rc.async = true; rc.setAttribute('charset', 'utf-8');
      rc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.revechat.com/widget/scripts/new-livechat.js?'+new Date().getTime();
      var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(rc, s);
    })(document, window);
    </script>
    <!-- End of REVE Chat Script -->
</body>
</html>