<?php
include('admin.php');

$obj=new Admin();


 $username = $_SESSION['username'];

 // echo $username;
 // exit();

if(!$username)
{
   echo '<script type="text/javascript">window.location ="login.php";</script>';
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Landing Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link rel="stylesheet" href="green_page/css/slider/style.css">
    <link rel="stylesheet" type="text/css" href="green_page/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="green_page/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="green_page/css/style-1.css">
    <link rel="stylesheet" type="text/css" href="green_page/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="shortcut icon" type="images/x-icon" href="green_page/img/favicon.png">
   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

      <script type="text/javascript" src="green_page/js/bootstrap.min.js"></script>


    <!-- <script type="text/javascript" src="jquery.sticky.js"></script> -->
    <style type="text/css">
      .ui-slider-handle{
        top: -7px !important;
        border-radius: 50%;
      }
      .ui-slider{
            border: 1px solid #5e5e5e !important;
            background-color: #757575;
            transition: all .3s;
            height: 6px;
                
      }
      
      .ui-widget-header
      {
        background-color: #189670;
      }
      .ui-state-disabled
      {
        opacity: 1 !important;
      }
    </style>
    <style type="text/css">
  .circle {
  
  position: relative;
}


.circle .border {
  /* content: ''; */
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: transparent;
  border-radius: 50%;
  border-top: 3px solid rgba(204, 204, 204);
  border-right: 3px solid rgba(204, 204, 204);
  border-bottom: 3px solid rgba(204, 204, 204);
  border-left: none;
  
  -webkit-animation-name: Rotate;
  -webkit-animation-duration: 2s;
  -webkit-animation-iteration-count: infinite;
  -webkit-animation-timing-function: linear;
  -moz-animation-name: Rotate;
  -moz-animation-duration: 2s;
  -moz-animation-iteration-count: infinite;
  -moz-animation-timing-function: linear;
  -ms-animation-name: Rotate;
  -ms-animation-duration: 2s;
  -ms-animation-iteration-count: infinite;
  -ms-animation-timing-function: linear;
}

@-webkit-keyframes Rotate {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
  }
}
@-moz-keyframes Rotate {
  from {
    -moz-transform: rotate(0deg);
  }
  to {
    -moz-transform: rotate(360deg);
  }
}
@-ms-keyframes Rotate {
  from {
    -ms-transform: rotate(0deg);
  }
  to {
    -ms-transform: rotate(360deg);
  }
}
</style> 
</head>
  <body style="background: #72da00;">
 
      <div class="green_page">
          <div class="gp_top_img">
            <center><img src="green_page/img/green_page/tj.jpg"></center>
          </div>      
          <div class="gp_helping">
            <img src="green_page/img/green_page/helping.png">
            <h2>HELPING MY FRIENDS WHILE HELPING MYSELF</h2>
          </div>
          <div class="gp_calculator">
            <img src="green_page/img/green_page/calculator.jpg">
            <h2>CALCULATOR</h2>
          </div>
          <div class="clearfix"></div>
          <div class="guest_pass">
            <center><h2>SELECT ON AVERAGE HOW OFTEN A GUEST PASS WILL BE USED</h2></center>
             <div class="gp_dates">
             <div class="gp_date">
              <p>DAILY</p>
             <select class="selectpicker" data-style="btn-primary" id="minbeds">
                <option data-icon="glyphicon glyphicon-music">0</option>
                <option data-icon="glyphicon glyphicon-star">1</option>
                <option data-icon="glyphicon glyphicon-heart">2</option>
                <option data-icon="glyphicon glyphicon-heart">3</option>
                <option data-icon="glyphicon glyphicon-heart">4</option>
                <option data-icon="glyphicon glyphicon-heart">5</option>
                <option data-icon="glyphicon glyphicon-heart">6</option>
                <option data-icon="glyphicon glyphicon-heart">7</option>
                <option data-icon="glyphicon glyphicon-heart">8</option>
                <option data-icon="glyphicon glyphicon-heart">9</option>
                <option data-icon="glyphicon glyphicon-heart">10</option>
            </select>
            </div>
            <div class="gp_date">
              <p>WEEKLY</p>
             <select class="selectpicker" data-style="btn-primary" id="minbeds_weekly">
                <option data-icon="glyphicon glyphicon-music">0</option>
                <option data-icon="glyphicon glyphicon-star">1</option>
                <option data-icon="glyphicon glyphicon-heart">2</option>
                <option data-icon="glyphicon glyphicon-heart">3</option>
                <option data-icon="glyphicon glyphicon-heart">4</option>
                <option data-icon="glyphicon glyphicon-heart">5</option>
                <option data-icon="glyphicon glyphicon-heart">6</option>
                <option data-icon="glyphicon glyphicon-heart">7</option>
                <option data-icon="glyphicon glyphicon-heart">8</option>
                <option data-icon="glyphicon glyphicon-heart">9</option>
                <option data-icon="glyphicon glyphicon-heart">10</option>
            </select>
            </div>
            <div class="gp_date">
              <p>MONTHLY</p>
             <select class="selectpicker" data-style="btn-primary" id="minbeds_monthly">
                <option data-icon="glyphicon glyphicon-music">0</option>
                <option data-icon="glyphicon glyphicon-star">1</option>
                <option data-icon="glyphicon glyphicon-heart">2</option>
                <option data-icon="glyphicon glyphicon-heart">3</option>
                <option data-icon="glyphicon glyphicon-heart">4</option>
                <option data-icon="glyphicon glyphicon-heart">5</option>
                <option data-icon="glyphicon glyphicon-heart">6</option>
                <option data-icon="glyphicon glyphicon-heart">7</option>
                <option data-icon="glyphicon glyphicon-heart">8</option>
                <option data-icon="glyphicon glyphicon-heart">9</option>
                <option data-icon="glyphicon glyphicon-heart">10</option>
            </select>
            </div>
            <div class="gp_date">
              <p>ANNUALLY</p>
             <select class="selectpicker" data-style="btn-primary" id="minbeds_annually">
                <option data-icon="glyphicon glyphicon-music">0</option>
                <option data-icon="glyphicon glyphicon-star">1</option>
                <option data-icon="glyphicon glyphicon-heart">2</option>
                <option data-icon="glyphicon glyphicon-heart">3</option>
                <option data-icon="glyphicon glyphicon-heart">4</option>
                <option data-icon="glyphicon glyphicon-heart">5</option>
                <option data-icon="glyphicon glyphicon-heart">6</option>
                <option data-icon="glyphicon glyphicon-heart">7</option>
                <option data-icon="glyphicon glyphicon-heart">8</option>
                <option data-icon="glyphicon glyphicon-heart">9</option>
                <option data-icon="glyphicon glyphicon-heart">10</option>
            </select>
            </div> 
            </div>
            <div class="gp_slider">
                   <!-- <div class="RangeSlider" id="RangeSlider">
                     <div class="RangeSlider_ParticipantName"></div>
                      <div class="RangeSlider_ClickArea">
                        <div class="RangeSlider_Track">
                          <div class="RangeSlider_TrackFill">
                            <div class="RangeSlider_Thumb"></div>
                          </div>
                        </div>
                        
                      </div>
                    </div> -->
                    <div id="slider"></div>
                    <style type="text/css">
                      .RangeSlider_Point1:before {
                            content: "";
                            display: block;
                            height: 5px;
                            width: 1px;
                            background-color: #666;
                            margin: -49px auto -29px;
                      }
                                        .RangeSlider_Point1:hover {
                                            color: #fff;
                                        }
                                        .RangeSlider_Point1 {
                                            font-size: 18px !important;
                                            color: #ddd !important;
                                            cursor: pointer;
                                            width: 20px;
                                            text-align: center;
                                            font-weight: 800;
                                        }
                    </style>
                    <div class="RangeSlider_Points">
                          <div class="RangeSlider_Point ">0</div>
                          <div class="RangeSlider_Point">1</div>
                          <div class="RangeSlider_Point">2</div>
                          <div class="RangeSlider_Point">3</div>
                          <div class="RangeSlider_Point">4</div>
                          <div class="RangeSlider_Point">5</div>
                          <div class="RangeSlider_Point">6</div>
                          <div class="RangeSlider_Point">7</div>
                          <div class="RangeSlider_Point">8</div>
                          <div class="RangeSlider_Point">9</div>
                          <div class="RangeSlider_Point">10</div>
                        </div><!-- /.RangeSlider_Points -->
            </div>


            <input type="hidden" name="rangeslider1" id="rangeslider1">


            <!-- second slider -->
            <div class="second_slider"> 
              <center><h2 style="font-size: 30px;font-weight: 700;">SELECT THE AMOUNT OF MONEY A GUEST PASS USER SAVES ON AVERAGE?</h2></center>
            </div>
            <div class="gp_slider">
            <div id="slider1"></div>
                   <div class="RangeSlider_Points">
                          <div class="RangeSlider_Point slider1 ">$50</div>
                          <div class="RangeSlider_Point1 slider1">$100</div>
                          <div class="RangeSlider_Point slider1">$150</div>
                          <div class="RangeSlider_Point1 slider1">$200</div>
                          <div class="RangeSlider_Point slider1">$250</div>
                          <div class="RangeSlider_Point1 slider1">$300</div>
                          <div class="RangeSlider_Point slider1">$350</div>
                          <div class="RangeSlider_Point1 slider1">$400</div>
                          <div class="RangeSlider_Point slider1">$450</div>
                          <div class="RangeSlider_Point1 slider1">$500</div>
                          <div class="RangeSlider_Point slider1">$550</div>
                          <div class="RangeSlider_Point1 slider1">$600</div>
                          <div class="RangeSlider_Point slider1">$650</div>
                          <div class="RangeSlider_Point1 slider1">$700</div>
                          <div class="RangeSlider_Point slider1">$750</div>
                          <div class="RangeSlider_Point1 slider1">$800</div>
                          <div class="RangeSlider_Point slider1">$850</div>
                          <div class="RangeSlider_Point1 slider1">$900</div>
                          <div class="RangeSlider_Point slider1">$950</div>
                          <div class="RangeSlider_Point1 slider1">$1000</div>
                        </div><!-- /.RangeSlider_Points -->
            </div>
             <input type="hidden" name="amount_slider1" id="amount_slider1" value="50">
          </div>


          <div class="boomerang">
              <center>
                <h2 style="font-size: 30px;font-weight: 700;">
                  CHOOSE THE BOOMERANG REWARD OPTION IF DESIRED & THE AMOUNT OF SUB-RIDERS TO BE ADDED TO THE LICENSE?
                </h2>
              </center>
              <div class="membership">
                <div class="col-md-7 col-md-offset-2">
                    <div class="m_license">
                        <h2>Membership License</h2>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="m_license">
                     <select class="selectpicker" id="membership_license" data-style="btn-primary">
                        <!-- <option data-icon="glyphicon glyphicon-music">0</option> -->
                        <option data-icon="glyphicon glyphicon-star">1</option>
                        <!-- <option data-icon="glyphicon glyphicon-heart">2</option>
                        <option data-icon="glyphicon glyphicon-heart">3</option>
                        <option data-icon="glyphicon glyphicon-heart">4</option>
                        <option data-icon="glyphicon glyphicon-heart">5</option>
                        <option data-icon="glyphicon glyphicon-heart">6</option>
                        <option data-icon="glyphicon glyphicon-heart">7</option>
                        <option data-icon="glyphicon glyphicon-heart">8</option>
                        <option data-icon="glyphicon glyphicon-heart">9</option>
                        <option data-icon="glyphicon glyphicon-heart">10</option> -->
                    </select>
                      </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                    <div class="membership">
                <div class="col-md-7 col-md-offset-2">
                    <div class="m_license">
                        <h2>Boomerang Rewards</h2>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="m_license">
                    <div class="checkbox">
                        <input id="boomerang_rewards" type="checkbox" value="1000">
                    </div>
                      </div>
                    </div>
                </div>    
                <div class="clearfix"></div>
              <div class="membership">
                <div class="col-md-7 col-md-offset-2">
                    <div class="m_license">
                        <h2>Sub-Rider Licenses</h2>
                        <p>(Each License is allowed up to 9 Sub-Riders)</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="m_license">
                     <select class="selectpicker" id="sub_rider" data-style="btn-primary">
                        <option data-icon="glyphicon glyphicon-music" selected="">0</option>
                        <option data-icon="glyphicon glyphicon-star">1</option>
                        <option data-icon="glyphicon glyphicon-heart">2</option>
                        <option data-icon="glyphicon glyphicon-heart">3</option>
                        <option data-icon="glyphicon glyphicon-heart">4</option>
                        <option data-icon="glyphicon glyphicon-heart">5</option>
                        <option data-icon="glyphicon glyphicon-heart">6</option>
                        <option data-icon="glyphicon glyphicon-heart">7</option>
                        <option data-icon="glyphicon glyphicon-heart">8</option>
                        <option data-icon="glyphicon glyphicon-heart">9</option>
                    </select>
                      </div>
                    </div>
                </div>
                </div>
                <div class="clearfix"></div>
                <div class="gp_buttons">
                  <center>
                    <button type="button" id="calculate1">
                      Calculate
                    </button>
                    <button type="button" id="reset_page" class="reset_page">
                      Reset
                    </button>
                  </center>
                </div>

                <div class="clearfix"></div>


                <div class="gp_after_cal" style="display: none">
                  <div class="gpa_slider">
                    <div class="gp_slider">
                    <div id="slider2"></div>
                       <div class="RangeSlider_Points">
                              <div class="RangeSlider_Point slider2 ">$500</div>
                              <div class="RangeSlider_Point1 slider2">$1000</div>
                              <div class="RangeSlider_Point slider2">$1500</div>
                              <div class="RangeSlider_Point1 slider2">$2000</div>
                              <div class="RangeSlider_Point slider2">$2500</div>
                              <div class="RangeSlider_Point1 slider2">$3000</div>
                              <div class="RangeSlider_Point slider2">$3500</div>
                              <div class="RangeSlider_Point1 slider2">$4000</div>
                              <div class="RangeSlider_Point slider2">$4500</div>
                              <div class="RangeSlider_Point1 slider2">$5000</div>
                              <div class="RangeSlider_Point slider2">$5500</div>
                              <div class="RangeSlider_Point1 slider2">$6000</div>
                              <div class="RangeSlider_Point slider2">$6500</div>
                              <div class="RangeSlider_Point1 slider2">$7000</div>
                              <div class="RangeSlider_Point slider2">$7500</div>
                              <div class="RangeSlider_Point1 slider2">$8000</div>
                              <div class="RangeSlider_Point slider2">$8500</div>
                              <div class="RangeSlider_Point1 slider2">$9000</div>
                              <div class="RangeSlider_Point slider2">$9500</div>
                              <div class="RangeSlider_Point1 slider2">$10000</div>
                              <div class="RangeSlider_Point slider2">$10500</div>
                            </div><!-- /.RangeSlider_Points -->
                    </div>
                  </div>
                  <div class="gpa_investment">
                    <center>
                      <h2>MEMBERSHIP INVESTMENT TOTAL: $<span id="total_mem_inst">______</span></h2>
                    </center>
                    <div class="with_selection">
                      <h2>With the selections above:</h2>
                      <p>
                        <span id="gpid" style="font-weight: 600;"></span> Guest Pass(es) would need to be used to receive a 100% return on your membership investment.<br>
                        <span id="wklid" style="font-weight: 600;"></span> <span id="weeks">Week(s)</span> would be needed to obtain a 100% return on your membership investment.<br>
                        <span id="crid" style="font-weight: 600;"></span> Credit Rewards will be earned over the course of 1st year.<br>
                        <span id="ddid" style="font-weight: 600;"></span> Is the direct dollar coversion value the Credit Rewards earned during the course of the 1st year.<br>
                        <span id="amid" style="font-weight: 600;"></span> Is the amount of money you saved those within the circles of your friends, family.
                      </p>
                      <br>
                      <p class="gpa_offer">
                        As an example <span>25,000</span> Credit Rewards Equals <span>$250.00</span><br>
                        Credit Rewards have a direct dollar conversion value.

                      </p>
                      <br><br>
                      <p class="gpa_note">
                        NOTE : Once earned all Credit Rewards go to the original full membership License account and can be converted into Gift Cards and or travel anywhere within the full membership portal of traveljolly.com. 
                      </p>

                    <div class="gp_buttons">
                      <center>
                        <button type="button" class="reset_page" style="width: 190px !important;">
                          Re-Calculate
                        </button>
                        
                      </center>
                    </div>

                      <div  class="reveal">
                          <h2 style="margin-top: 80px;font-size: 40px;"> To Reveal Membership Discount & Pricing  </h2>
                            <div class="reveal_bottom">
                              <div class="gps_margin80"></div>
                               <div class="col-md-6">
                                  <h2>Enter Gift Card Number:</h2>
                               </div>
                               <div class="col-md-6">
                                  <input type="text" id="gift_card_number" name="gift_card_number" placeholder="Enter Here" maxlength="16">
                                </div>
                                <span id="errmsgAddress"></span>
                                <div class="clearfix"></div>
                                <div class="gps_margin50"></div>
                              <div class="col-md-6">
                                  <h2>Pin:</h2>
                              </div>
                              <div class="col-md-6">
                                 <p># </p><input type="text" id="pin" name="pin" placeholder="Enter Here" maxlength="4">
                              </div>
                          </div>
                      </div>
                       <div class="clearfix"></div>
                <div class="gp_buttons">
                  <center>
                    <button type="button" id="calculate2" style="width: 560px !important;">
                      Reveal Discounted Full Membership Pricing
                    </button>
                    
                  </center>
                </div>
                    </div>
                  </div>

                </div>  

                <div class="third_part" >

                <div class="third_part1" style="display: none;">
                  <div class="logo1">
                   <center> <img src="green_page/img/green_page/logo1.jpg" style="height: 160px;"></center>
                  </div>
                  <div class="gp1_circle ">
                    <div class="border"></div>
                    <p>INVESTMENT PRICING IS CALCULATED AND BASED ON EACH GIFT CARDS UNIQUE LINEAGE, PAST PURCHASE & THE INVESTMENT IN TIME YOU HAVE SPENT WITH US SO FAR.<br><br>#ShareTheLove</p>
                  </div>

                   <!-- <div class="logo1">
                   <center> <img src="green_page/img/green_page/logo1.jpg"></center>
                  </div> -->
                  </div>
                  <div class="clearfix"></div>
                  <div class="third_part2" style="display: none;">
                  <center><h2 style="font-size: 40px;color:#2d2d2d;">FULL PLATINUM MEMBERSHIP CALCULATED</h2></center>
                  <div class="gp1_margin"></div>
                  <center><h3 style="color: #2d2d2d;"><span style="text-decoration: underline;">95.5%</span> Discount Confirmed Net Price <span style="text-decoration: underline;">$499</span></h3></center>
                  <center><h3 style="color: #2d2d2d;">VIP Immediate Activation Required - Discount Pricing Recorded</h3></center>
                  <div class="gp_buttons">
                  <center>
                   <button type="button" id="reset_page" class="reset_page">
                      Reset
                    </button>
                  </center>
                </div>
                  <div class="gp1_footer" style="margin-top: 50px !important;">
                    <center><p>#traveljolly<span>membership</span></p></center>
                  </div>
                  
                </div> 
              </div>

              </div>
            
    
      

      <!-- <script type="text/javascript" src="js/jquery.js"></script>
       <script type="text/javascript" src="jquery.sticky.js"></script>
       <script  src="js/index.js"></script> -->
      
      <script type="text/javascript">
      $(document).ready(function(){
        var weekly =0;
        var weekly_s =0;
        $('#slider').slider({ disabled: true });
        $('#minbeds').val(0);
        $( "#minbeds_weekly" ).val(0);
        $( "#minbeds_monthly" ).val(0);
        $( "#minbeds_monthly" ).val(0);
        $( "#sub_rider" ).val(0);
        $( "#boomerang_rewards" ).prop('checked', false);;
      });
      var SliderPoints = $('.RangeSlider_Points').find('.RangeSlider_Point');
    $(function() {
        // var select = $( "#minbeds" );
        
        $( "#slider" ).slider({
          range:"min",
          animate:true,
            value:0,
            min: 0,
            max: 10,
            step: 1,
            slide: function( event, ui ) {
                $( "#rangeslider1" ).val(ui.value );
                // SliderPoints.removeClass('RangeSlider_PointActive');
                // SliderPoints.eq( ui.value ).addClass('RangeSlider_PointActive');

                // console.log($('#minbeds').is(':disabled'));
                // console.log($('#minbeds_weekly').is(':disabled'));

                if($('#minbeds').is(':disabled')==false)
                {
                  $( "#minbeds" ).val(ui.value );
                  
                }
                if($('#minbeds_weekly').is(':disabled')==false)
                {
                  $( "#minbeds_weekly" ).val(ui.value );
                }
                if($('#minbeds_monthly').is(':disabled')==false)
                {
                  $( "#minbeds_monthly" ).val(ui.value );
                }
                if($('#minbeds_annually').is(':disabled')==false)
                {
                  $( "#minbeds_annually" ).val(ui.value );
                }
            }
        });
        $( "#rangeslider1" ).val($( "#slider" ).slider( "value" ) );
        
  // DAILY dropdown value   
    $( "#minbeds" ).on( "change", function() {
      
      $("#weeks").text("Daily");
       $('#slider').slider({ disabled: false });
      $('#minbeds').attr('disabled', false);
      $('#minbeds_weekly').attr('disabled', true);
      $('#minbeds_monthly').attr('disabled', true);
      $('#minbeds_annually').attr('disabled', true);
      $( "#slider" ).slider("value", $(this).val());
      $( "#rangeslider1" ).val( $(this).val());
      // SliderPoints.removeClass('RangeSlider_PointActive');
      // SliderPoints.eq( $(this).val() ).addClass('RangeSlider_PointActive'); 

      
      weekly = $("#rangeslider1").val() * 365;
      weekly_s = $("#rangeslider1").val();
    });

    // minbeds_weekly dropdown value   
    $( "#minbeds_weekly" ).on( "change", function() {
      // console.log(select[0].selectedIndex);
      $("#weeks").text("Weekly");
      $('#slider').slider({ disabled: false });
      $('#minbeds').attr('disabled', true);
      $('#minbeds_weekly').attr('disabled', false);
      $('#minbeds_monthly').attr('disabled', true);
      $('#minbeds_annually').attr('disabled', true);
      $( "#slider" ).slider("value", $(this).val());
      $( "#rangeslider1" ).val( $(this).val());
      // SliderPoints.removeClass('RangeSlider_PointActive');
      // SliderPoints.eq( $(this).val() ).addClass('RangeSlider_PointActive');
       weekly = $("#rangeslider1").val() * 52;
       weekly_s = $("#rangeslider1").val();
    });

    // minbeds_monthly dropdown value   
    $( "#minbeds_monthly" ).on( "change", function() {
      // console.log(select[0].selectedIndex);
      $("#weeks").text("Monthly");
      $('#slider').slider({ disabled: false });
      $('#minbeds').attr('disabled', true);
      $('#minbeds_weekly').attr('disabled', true);
      $('#minbeds_monthly').attr('disabled', false);
      $('#minbeds_annually').attr('disabled', true);
      $( "#slider" ).slider("value", $(this).val());
      $( "#rangeslider1" ).val( $(this).val());
      // SliderPoints.removeClass('RangeSlider_PointActive');
      // SliderPoints.eq( $(this).val() ).addClass('RangeSlider_PointActive');
       weekly = $("#rangeslider1").val() * 12;
       weekly_s = $("#rangeslider1").val();
    });

    // minbeds_annually dropdown value   
    $( "#minbeds_annually" ).on( "change", function() {
      // console.log(select[0].selectedIndex);
      $("#weeks").text("Passes Annually");
      $('#slider').slider({ disabled: false });

      $('#minbeds').attr('disabled', true);
      $('#minbeds_weekly').attr('disabled', true);
      $('#minbeds_monthly').attr('disabled', true);
      $('#minbeds_annually').attr('disabled', false);
      $( "#slider" ).slider("value", $(this).val());
      $( "#rangeslider1" ).val( $(this).val());
      // SliderPoints.removeClass('RangeSlider_PointActive');
      // SliderPoints.eq( $(this).val() ).addClass('RangeSlider_PointActive');
       weekly = $("#rangeslider1").val();
       weekly_s = $("#rangeslider1").val();
    });

    var SliderPoints1 = $('.RangeSlider_Points').find('.slider1');
        $( "#slider1" ).slider({
          range:"min",
          animate:true,
            value:50,
            min: 50,
            max: 1000,
            step: 50,
            slide: function( event, ui ) {
              
              
                $( "#amount_slider1" ).val( ui.value );
                 SliderPoints1.removeClass('RangeSlider_PointActive');
                 // SliderPoints1.eq( ui.value ).addClass('RangeSlider_PointActive');
            }
        });

        $( "#slider2" ).slider({
           range:"min",
           animate:true,
            value:500,
            min: 500,
            max: 10500,
            step: 500,
            disabled: true,
            slide: function( event, ui ) {
              
              var SliderPoints2 = $('.RangeSlider_Points').find('.slider2');
                // $( "#amount_slider1" ).val( ui.value );
                 SliderPoints2.removeClass('RangeSlider_PointActive');
                 // SliderPoints1.eq( ui.value ).addClass('RangeSlider_PointActive');
            }
        });
        
 });

    $('#calculate1').click(function(){
      


      var membership_license = $('#membership_license').val();
      var boomerang_rewards = $('#boomerang_rewards').is(":checked");
      if(boomerang_rewards=="")
      {
        // alert("Please Select Boomerang Rewards");
        // return false;
        var boomerang_val = 0;
      }
      else
      {
        var boomerang_val = boomerang_rewards = $('#boomerang_rewards').val();
      }

      var sub_rider = $('#sub_rider').val();
      if(sub_rider ==null)
        {
        alert("Please Select Sub-Rider Licenses");
        return false;
      }
      var total = parseInt(membership_license * 5000) + parseInt(boomerang_val) + parseInt(sub_rider * 500);
      // console.log(total);
      // return total;
      $(".gp_after_cal").css("display", "block");
      $("#total_mem_inst").text((total).toFixed(2));
      

      var offset = $("#calculate1").offset();
      $(window).scrollTop(parseInt(offset.top)-20);

      $( "#slider2" ).slider("value", total);
      var SliderPoints2 = $('.RangeSlider_Points').find('.slider2');
      SliderPoints2.removeClass('RangeSlider_PointActive');

      var amount_slider1 = $("#amount_slider1").val();
      var guest_pass = total / amount_slider1;
      var quotient = Math.floor(total/amount_slider1);
      var remainder = total % amount_slider1;
      if (remainder > 0)
        guest_pass = quotient + 1;
      var weekly_dd  = total / amount_slider1;
      var weekly_d  = weekly_dd / weekly_s;
      var weeksQuotient = Math.floor(weekly_dd/weekly_s);
      var weeksRemainder = weekly_dd % weekly_s;
      //if (weeksRemainder > 0)
      //  weekly_d = weeksQuotient + 1;
      var crid  = weekly * amount_slider1;
      // quotient = Math.floor(weekly*amount_slider1);
      // remainder = weekly % amount_slider1;
      // if (remainder > 0)
      //   crid = quotient + 1;
      var ddid  = crid * 100;
      var amid  = ddid / 100;
      // console.log(weekly);

      $("#gpid").text((guest_pass).toFixed(0));
      var timePeriod= "Days";
      if ($("#minbeds_weekly" ).val() > 0)
        timePeriod="Weeks";
      if ($("#minbeds_monthly" ).val() > 0)
        timePeriod="Months";
      if ($("#minbeds_annually" ).val() > 0)
        timePeriod="Years";
      if (weekly_d > 1)
        $("#weeks").text(timePeriod);
      else
        $("#weeks").text(timePeriod);
      if (weeksRemainder > 0)
        $("#wklid").text((weekly_d).toFixed(2));
      else
        $("#wklid").text((weekly_d).toFixed(0));
      $("#ddid").text('$'+(crid).toLocaleString());
      $("#crid").text((ddid).toLocaleString());
      $("#amid").text('$'+(crid).toLocaleString());
    });

    $('#calculate2').click(function(){
      if($('#gift_card_number').val()=="")
      {
        alert("Please Enter Gift Card Number");
        return false;
      }
      if($('#pin').val()=="")
      {
        alert("Please Enter Pin");
        return false;
      }
      var offset1 = $("#calculate2").offset();
       
      $(".third_part1").css("display","block");
      $(".gp1_circle").addClass("circle");
      $(".gp1_circle").toggleClass("gp1_circle , gp1_circle1");
      // $(window).scrollTop(offset1.top);
      calculate2_button(offset1);
       $(window).scrollTop(parseInt(offset1.top)+320);
    });

    $(".reset_page").click(function(){
       window.location.reload();
    });

    function calculate2_button(offset1) {
    setTimeout(function(){ 
      $(".third_part2").css("display","block");
      $(".gp1_circle1").removeClass("circle");
      $(".gp1_circle1").toggleClass("gp1_circle1 , gp1_circle");
     // $(window).scrollTop(parseInt(offset1.top)+400);
     $('html, body').animate({scrollTop:$(document).height()}, 'slow');
    }, 14000);


}
$("#gift_card_number").keypress(function(e){
  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       return false;
       }
});

$("#pin").keypress(function(e){
  // console.log(e.which);
  // return false;
  if (e.which != 8 && e.which != 0 && (e.which < 97 || e.which > 122) && (e.which < 65 || e.which > 90)) {
       return false;
       }
});


    </script>
    </body>
    </html>