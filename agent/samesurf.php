<?php
include('admin.php');

$obj=new Admin();


 $username = $_SESSION['username'];

 // echo $username;
 // exit();

if(!$username)
{
  // echo '<script type="text/javascript">window.location ="login.php";</script>';
}


?>
<!DOCTYPE html>
<html>
<head>
  <title>Travel Jolly World</title>
  <style type="text/css">
            body, html
            {
                margin: 0; padding: 0; height: 100%; overflow: hidden;
            }

            #content
            {
                position:absolute; left: 0; right: 0; bottom: 0; top: 0px; 
            }
        </style>

<script src="js/jquery-3.3.1.min.js"></script>
</head>
<html>
<body>
<form id="shareCodeForm" >
    <input type="hidden" name="share_code" value="<?php print $_POST['share_code'] ?>">
    <input type="hidden" name="qs" value="<?php print $_POST['qs'] ?>">
</form>
<div id="busyIndicator" style="background-color: green; width:100%; height:100%; display: block;">
  <span style="font-weight: bold;">Travel Jolly is fetching your session...</span>
</div>
<div id="content">
  <iframe id="travelJollySameSurfFrame" width="100%" height="100%" frameborder="0" ></iframe>
</div>  
<script type="text/javascript">
$(document).ready(function(){
  
  getSameSurfURL();

}); 
function getSameSurfURL() {
  //alert($('input[name=share_code]').val());
  var success = false;
  var somedata = {
    room: $('input[name=share_code]').val()
  };
  $.ajax({
    url: "https://api.samesurf.com/api/v3/find",
    dataType: "json",
    method: "POST",
    cache: "false",
    data: JSON.stringify(somedata),
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("Authorization", "Bearer " + $('input[name=qs]').val());
    },
    error: function (xhr,status,error) {
        // error handler
        success = false;
        $('#busyIndicator').text('Failed');
        //alert("error:" + xhr.status + " " + xhr.statusText);
    },
    success: function (data) {
      if (data.publicinvitation)
        success = true;
      $('#busyIndicator').attr('display', 'hidden');
      //$('#travelJollySameSurfFrame').attr('display', "block");
      $('#travelJollySameSurfFrame').attr('src', data.publicinvitation);

    },
    complete: function(data) {
      if (!success)
        $('#busyIndicator').text('Apologies from Travel Jolly but that Share Code does appear to be correct. Please close this window and try again.');
    }

  });
}
</script>

</body>
</html> 
 </body>
</html>