<?php
include('admin.php');

$obj=new Admin();


 $username = $_SESSION['username'];

 // echo $username;
 // exit();

if(!$username)
{
  // echo '<script type="text/javascript">window.location ="login.php";</script>';
}


?>
<!DOCTYPE html>
<html>
<head>
    <title>Landing Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
     <link rel="stylesheet" type="text/css" href="style_for_dropdown.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/style-1.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" type="images/x-icon" href="img/favicon.png">
    <script type="text/javascript" src="jquery.sticky.js"></script>
    <script type="text/javascript" src="jquery.min.js"></script>
    
    <style type="text/css">
      .ab {
    background-color: rgba(0, 0, 0, 0.88);
   
   
      }
      .modal-footer a{
        background: none;
        border: none;
      }
      
     
.container {
    position: relative;
    width: 50%;
}

.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.container:hover .image {
  opacity: 0.3;
}

.container:hover .middle {
  opacity: 1;
}

.text {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  padding: 16px 32px;
}

    </style>

</head>
<body>


<!-- <div class="header">   -->
  <!-- <div id="header-sroll">  -->
    <div class="head" style="display:none">
      <div class="container-fluid">
        <div class="col-md-2 col-sm-4 col-xs-6 logo-md">
          <div class="head-img">
            <img src="img/logo.png" class="img-responsive">
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 h-sma">
          <div class="head-mid">
            <h3>The World’s  Newest & Greatest Booking Engine!</h3>
            <div class="mid-bottom">
              <p class="offer">Coming The Summer 2018!</p>
              <p class="fat"><span>FAT SAVINGS </span>SLIM PRICES</p>
            </div>
            
          </div>
        </div>
        <div class="col-md-2 col-sm-2 padding-0 top-button">
          <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-6 padding-0 m-login">
                <a href="https://wholesale.traveljolly.com/" target="_blank" class="btn btn-default header-button">Member Login</a>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6 padding-0 l-guest">
                <a href="http://travelsavingmoney.traveljolly.com/" target="_blank" class="btn btn-default header-button">Login as Guest</a>
              </div>
          </div>
        </div>
        <div class="col-md-2 right-md">
          <p class="head-p-r">Booking Engine Gift Cards are useable  for 1 year from date of issue</p>
        </div>
      </div>
    </div>
    <div class="up_part">
      <div class="navbar navbar-default tj-navbar" role="navigation">
            <div class="container-fluid">
                  <div class="sec-logo">
                    <img src="img/logo-1.png">
                  </div>
                  <div class="second-head">
                    <h3>The World’s  Newest & Greatest Booking Engine!</h3>
                    <p>UPTO 90% OFF ON NEARBY DEALS - BRILLIANT TREASURE MAPS ALONGSIDE MONEY & TIME SAVING 
                      <span class="bk_me">Coming The Summer 2018!</span><span class="bk_me1"> (Exact Date TBD) </span>
                      <span class="bk_me1" style="cursor: pointer !important;"> <a href="logout.php">Agent logout</a></span>
                    </p>
                  </div>

                  <div class="second-button">
            <div class="col-md-6 col-sm-6 col-xs-6 padding-0 m-login">
                <a href="https://wholesale.traveljolly.com/" target="_blank" class="btn btn-default header-button" style="margin-top: 25px;">Member Login</a>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6 padding-0 l-guest">
                <a href="http://travelsavingmoney.traveljolly.com/" target="_blank" class="btn btn-default header-button" style="margin-top: 25px;">Login as Guest</a>   
                
              </div>
              </div>
             
                <div class="collapse navbar-collapse tj-collapse" >
                    <ul class="nav navbar-nav ul_dropdown" style="margin-top: 79px; margin-left: -50px;" >
                        <li class="dropdown mega-dropdown">
                           <a href="#" class="dropdown-toggle header-button" style="width:50px;" data-toggle="dropdown" align="right"><img src="img/dots.png"> </a>
                             <ul class="dropdown-menu mega-dropdown-menu">
                                <li class="col-sm-3 col-xs-12">
                                    <ul class="tj-border-r">
                                    <li class="dropdown-header dh">All Types </li>
                                    <li><a href="" data-toggle="modal" data-target="#myModalVideos">Videos</a></li>
                                    <li><a href="about.php" target="_blank">About Us</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#myModal4">Get The TJ Prime App</a></li>
                                    <li><a href="#">Replacing Your Income</a></li>
                                    <li><a href="green_page.php" target="_blank">Whats in it for you!</a></li>
                                    <li><a href="#">Need A Plan We Will Help </a></li>
                                    <li><a href="#">We Love To Partner</a></li>
                                    <li><a href="#">Investors Wanted</a></li>
                                    <!-- <li><a href="#">Four Columns Grid</a></li> -->

                                    
                                  </ul>
                                </li>
                                <li class="col-sm-9 rj-restaurant small-hidden">
                                  <ul>
                                    <li class="dropdown-header dropdown-border">
                                      <div class="dropdown-header-left">
                                        <h3>Browse the beauty of things soon to come.</h3>
                                      </div>
                                      <div class="dropdown-header-right">
                                        <a href="merge_pages.php" target="_blank" class="rj-a">See All</a>
                                      </div>
                                      <div class="clearfix"></div>
                                    </li>
                                    <!-- <li class="dropdown-border"></li> -->
                                    <li>
                                      <div class="row dropdown-body">
                                        <div class="col-md-3"><a target="_blank" href="pre_landing.php"><img src="img/map/dropdown/1.png"></a></div>
                                        <div class="col-md-3"><a target="_blank" href="http://traveljolly.i2space.co.in"><img src="img/map/dropdown/2.png"></a></div>
                                        <div class="col-md-3"><a href="" data-toggle="modal" data-target="#myModal3"><img src="img/map/dropdown/3.png"></a></div>
                                        <div class="col-md-3"><a target="_blank" href="coming_soon.php"><img src="img/map/dropdown/4.png"></a></div>
                                        <div class="clearfix"></div>
                                      </div>
                                      <div class="row dropdown-body">
                                        <div class="col-md-3"><a target="_blank" href="coming_soon_3.php"><img src="img/map/dropdown/5.png"></a></div>
                                        <div class="col-md-3"><a target="_blank" href="coming_soon_2a.php"><img src="img/map/dropdown/6.png"></a></div>
                                        <div class="col-md-3"><a target="_blank" href="map.php"><img src="img/map/dropdown/7.png"></a></div>
                                        <div class="col-md-3"><a target="_blank" href="coming_soon_2a.php?hire=yes"><img src="img/map/dropdown/8.png"></a></div>
                                      </div>
                                    </li>
                                    
                                  </ul>
                                </li>
                              
                              </ul>       
                           </li>
                         </ul>


                </div>
            </div>
        </div>
       
      <div class="third-p">
        <div class="banner">
            <img src="img/banner.png">
          </div>
          <div class="banner-content">
            <h3><span>Orlando Florida<br>
              Mega Startup Event</span><br>
              (Let us pay for your trip or part of it!)</h3>
          </div>
          <div class="banner-bottom">
            <h3>CLAIM YOUR GIFT CARD NOW AND RECEIVE A VIP INVITATION TO THE PARTY EVENT OF THE YEAR!</h3>
            <p>WORLD CLASS EVERYTHING & EVERYBODY!  &nbsp;&nbsp;-&nbsp;&nbsp;  DO NOT MISS THIS EVENT OR YOU WILL REGRET IT FOR THE REST OF YOUR LIFE!  &nbsp;&nbsp;-&nbsp;&nbsp;  REMEMBER WOODSTOCK!</p>
          </div>
      </div>
    
    
    
      <div class="fourth">
        <div class="container-fluid">
          <h3>As Seen In & What Other Are Saying: </h3>
          <p>Traveljolly.com has delivered beyond my wildest expectation - Revolutionary - Utterly Amazing - Everything has changed travel will never be the same! - Samed my whole group time and money - As described.</p>
          <div class="fourth-bottom">
            <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
              <div class="fourth-1">
                <img src="img/c-1.png">
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
              <div class="fourth-1">
                <img src="img/c-2.png">
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
              <div class="fourth-1">
                <img src="img/c-3.png">
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
              <div class="fourth-1">
                <img src="img/c-4.png">
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
              <div class="fourth-1">
                <img src="img/c-5.png">
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
              <div class="fourth-1">
                <img src="img/c-6.png">
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

      <div class="down_part" style="display: none;">
      <div class="fifth">
        <div class="container-fluid">
          <div class="gift-section g-sec">
            <div class="col-md-8 col-sm-12 col-xs-12">
              <div class="col-md-6 col-sm-6 col-xs-12 act-now-xs">
                <div class="act-now">
                  <img src="img/card.png">
                </div>
              </div>
               <div class="col-md-6 col-sm-6 col-xs-12 free-gift-xs">
                <div class="free-gift">
                  <img src="img/free-gift.png">
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12 g-det">
                <div class="g-detail">
                  <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                    <img src="img/g-1.png">
                  </div>
                   <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                    <img src="img/g-2.png">
                  </div>
                   <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                    <img src="img/g-3.png">
                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                    <img src="img/g-4.png">
                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                    <img src="img/g-5.png">
                  </div>
                   <div class="col-md-2 col-sm-4 col-xs-6 small-comp smallcom-p">
                    <p>.. and many MORE!</p>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
                <div class="g-day">
                  <div class="g-day-left">
                    <h3>TODAY IS A GREAT DAY!</h3>
                    <p>NOTE: NO OBLIGATION NECESSARY TO  CLAIM GIFT</p>
                  </div>
                  <div class="g-day-right">
                    <div class="g-day-right-1">
                      <img src="img/choose.png">  
                    </div>
                    <div class="g-day-right-2">
                      <h3><span>YOU </span><br>CHOOSE</h3>
                    </div>
                  </div>
                </div>
              <div class="clearfix"></div>
              <div class="doors">
                <div class="col-md-4 col-sm-4 doors-l">
                  <div class="tj-o">
                   
                    <img src="img/tj.png">
                   
                  </div>
                </div>
                <div class="col-md-8 col-sm-8 doors-p"> 
                  <div class="door-1">
                    <a href="" data-toggle="modal" data-target="#myModal">
                      <img src="img/exit-door.png" title="click to open the door">
                    </a> 
                    <!-- <div class="open-door">
                      <a href="" data-toggle="modal" data-target="#myModal"><p>Click To Open The Door</p></a>
                    </div>  -->
                  </div>
                  <div class="door-2">
                    <a href="" data-toggle="modal" data-target="#myModal1">
                      <img src="img/no-add.png" title="click to open the door">
                    </a>
                   <!--  <div class="open-door">
                      <a href="" data-toggle="modal" data-target="#myModal1"><p>Click To Open The Door</p></a>
                    </div>  -->
                  </div>

                </div>
              </div>
            </div>

           
             <div class="col-md-4 col-sm-12 col-xs-12">
              <div class="card">
                <img src="img/act-now.png">
              </div>
              <div class="card-bottom">
                <div class="cb-top">
                  <img src="img/dollar.png">
                  <p>WORLDS TOP BOOKING ENGINES!</p>
                </div>
                <div class="cb-bottom">
                  <div class="cb-checkbox">
                    <div class="cb-content">
                      <img src="img/checkbox.png">
                      <p>400,000 TOP BRANDS HOTELS & RESORTS</p>
                    </div>
                     <div class="cb-content">
                      <img src="img/checkbox.png">
                      <p>800,000 RENTAL CAR LOCATION</p>
                    </div>
                     <div class="cb-content">
                      <img src="img/checkbox.png">
                      <p>ACTIVITIES, ATTRACTIONS, EXCURSIONS & MORE </p>
                    </div>
                     <div class="cb-content">
                      <img src="img/checkbox.png">
                      <p>110% BEST PRICE GUARANTEE</p>
                    </div>
                    <!-- <div class="cb-content">
                      <img src="img/checkbox.png">
                      <p>110% BEST PRICE GUARANTEE</p>
                    </div> -->
                  </div>
                  <div class="g-door-small">
                    <div class="g-door-2">
                      <h3>Gift Card Redemption For Door 2 Expires at anytime.</h3>
                      <p>( Redemption is based on limited BETA availability )</p>
                    </div>
                    <div class="cb-invites">
                      <button type="button" class="bt-lg">Email Request</button>
                      <input type="email" name="" placeholder="For an Invitation & Gift Card">
                      <button type="button" class="bt-small">Requets an Invitation</button>
                    </div>
                  </div>
                   <div class="visitors" id="visitors" style="display: none;">
                    <div class="visitors-c">
                      <p><span class="visitor_no">100</span> other people are on this page right now</p>
                      <a href=""><div class="visitors-a">
                        X  
                      </div></a>
                    </div>
                  </div>
                </div>

              </div>
            </div>
              <a href="" data-toggle="modal" data-target="#myModal2"><div class="back-to-home">
           
              <img src="img/back.png">
              Back To Home
            
          </div></a>
            <div class="clearfix"></div>
          </div>     

        </div>      
      </div>
    </div>
    
    <div class="footer">
      <div class="container-fluid">
        <p><img src="img/copyright.png">&nbsp;2018 Travel Jolly. All Right Reserved </p>
      </div>
    </div>

  <!-- </div> -->

<!-- body -->


  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->

<!-- <div class="bottomMenu">
  <button data-toggle="modal" data-target="#myModal">Door 1</button>
  <button data-toggle="modal" data-target="#myModal">Door 2</button>
</div> -->



<!-- Modal1-->
<div id="myModal" class="modal fade ab" role="dialog">
  <div class="modal-dialog modal2-dialog">

    <!-- Modal content-->
    <div class="modal-content">
            <!-- <div class="modal-body"> -->
        <div class="fifth fifth-modal">
      <div class="container-fluid">
        <div class="gift-section">
          <div class="col-md-12">
           
            <div class="doors doors-sm doors-xxs">
             
              <div class="col-md-12 doors-p"> 
               <div class="door-1-lap">
                  <div class="dl-head">
                    <h3>Register, Claim & Save Your Gift Card While Supplies Last! </h3>
                    <p>Once Registered Gift Cards are redeemable for 1 year from date of issue</p>
                  </div>
                  <div class="dl-body">
                    <div class="col-md-6 col-sm-6 col-xs-6 now-small">
                      <img src="img/now-never.png">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 now-small">
                      <div class="dl-body-r">
                        <h3>Your Gift Card Number :</h3>
                        <input type="" name="" placeholder="4789 - 9869 - 8989 - 7878">
                        <p>Unique to you, One card per registration</p>
                        <div class="dl-body-r-bottom">
                          <h4>PIN : </h4><input type="text" name="" placeholder="#PCGH">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="dl-body-p">
                    <center><p>NOTE: NO OBLIGATION NECESSARY TO  CLAIM GIFT</p></center>
                  </div>
                  <div class="clearfix"></div>
                  <div class="dl-body-bottom">
                    <div class="col-md-6 col-sm-6 col-xs-6 dl-b-yout dl-b-yout-small">
                      <a href="http://traveljolly.com/explainervideo.html" target="_blank"><img src="img/youtube-1.png"></a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 dl-b-r-small1">
                      <div class="dl-body-bottom-r">
                        <div class="dl-body-f">
                          <h4>Login To :</h4><img src="img/facebook.png">
                        </div>
                        <div class="dl-body-b">
                          <h4>Or Sign In With :</h4>
                          <div class="dl-body-ul">
                            <ul>
                              <li><img src="img/twitter.png"></li>
                              <li><img src="img/gmail.png"></li>
                              <li><img src="img/mail.png"></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="dl-body-p dl-p-xs">
                    
                      <p class="dl-p1">WE NEVER POST WITHOUT PERMISSION </p><p class="dl-p2"><a href="" class="d-h-a">DONT HAVE ACCOUNT?</a>&nbsp;&nbsp;<a href="">CREATE AN ACCOUNT</a></p>
                    
                  </div>
               </div>

              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-modal" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal2 -->
<div id="myModal1" class="modal fade ab" role="dialog">
  <div class="modal-dialog modal2-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <!-- <div class="modal-body"> -->
        <!-- <div class="fifth door2-bg"> -->
          <div class="fifth fifth-modal">
      <div class="container-fluid">
        <div class="gift-section">
          <div class="col-md-12 xs">
           
              <div class="g-day">
                
                
              </div>
            <div class="clearfix"></div>
            <div class="doors ds doors-xxs">
             
              <div class="col-md-12 col-xs-12"> 
               <div class="door-1-lap door-l">
                <div class="d2-content">
                  <div class="d2-head">
                    <a href="http://traveljolly.com/explainervideo.html" target="_blank"><img src="img/play.png"></a>
                  </div>
                 <div class="d2-phone">
                  <div class="d2-img">
                    <img src="img/phone.png"> 
                  </div>
                  <div class="d2-p">
                   <p>1-800-611-3161</p>
                  </div>
                 </div>
                 <div class="d2-phone d2-phone-xs">
                   <input type="text" name="share_code" placeholder="Call For Share Code Now!">
                   <button type="button" id="share_codebtn">Go</button>
                 </div>
               </div>
              
               <div class="dl-body-p dl-mar">
                    <center><p>NOTE: NO OBLIGATION NECESSARY TO  CLAIM GIFT</p></center>
                  </div>
               </div>

               <!--  <div class="door-1">
                  <a href="" data-toggle="modal" data-target="#myModal">
                    <img src="img/d-11.png" title="click to open the door">
                  </a>  
                </div>
                <div class="door-2">
                  <a href="" data-toggle="modal" data-target="#myModal1">
                    <img src="img/d-22.png" title="click to open the door">
                  </a>
                </div> -->

              </div>
            </div>
          </div>

         
          
        </div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-modal" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- modal 3 -->
<!-- Modal3-->
<div id="myModal2" class="modal fade ab" role="dialog">
  <div class="modal-dialog modal2-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title"><span>FAT SAVINGS</span> SLIM PRICES</h4>
      </div>
            <!-- <div class="modal-body"> -->
      <div class="fifth fifth-modal card-s">
      <div class="container-fluid">
        <div class="gift-section">
          <div class="col-md-12">
           
            <div class="doors doors-sm doors-xxs ">
             
              <div class="col-md-12 doors-p"> 
               <div class="door-1-lap cards-section border-none">
                 <div class="cards-left">
                   <img src="img/card-vip.png">
                 </div>
                 <div class="cards-right">
                   <h3>INDUSTRIES</h3>
                   <h4>UPGRADED</h4>
                   <div class="cr-client">
                     <div class="cr-client-l">
                       <img src="img/g-1.png">
                     </div>
                     <div class="cr-client-r">
                       <img src="img/g-2.png">
                     </div>
                   </div>
                   <div class="cr-client">
                     <div class="cr-client-l">
                       <img src="img/g-3.png">
                     </div>
                     <div class="cr-client-r">
                       <img src="img/g-4.png">
                     </div>
                   </div>
                   <div class="cr-client">
                     <div class="cr-client-l">
                       <img src="img/g-5.png">
                     </div>
                     <div class="cr-client-r">
                      <p>.. and many MORE!</p>
                     </div>
                   </div>
                 </div>
                <div class="clearfix"></div>
                 <div class="cl-r-btm">
                    <img src="img/top-deal.png">                   
                 </div>
                 <div class="cl-r-btm-r">
                   <img src="img/travel.png">
                 </div>

               </div>

              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
    </div>
      </div>
      <div class="modal-footer">
       <a href="indexVideo.php"> <button type="button" class="btn btn-default btn-modal">Close</button></a>
      </div>
    </div>

  </div>
</div>

<div id="myModal3" class="modal fade ab" role="dialog">
  <div class="modal-dialog modal2-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h2 class="modal-title" style="font-weight: 700; font-size: 40px;"><span>COMING SOON</span></h2>
      </div>
            <!-- <div class="modal-body"> -->
      <div class="fifth fifth-modal card-s">
      <div class="container-fluid">
        <div class="gift-section">
          <div class="col-md-12">
           
            <div class="doors doors-sm doors-xxs ">
             
              <div class="col-md-12 doors-p"> 
               <div class="door-1-lap cards-section border-none">
                
                  <center><h2 style="font-weight: 700; color: #000; font-size: 35px;">SOON AT HOME & AWAY YOU'LL</h2> </center> 
                  <br>
                  <center><h2 style="font-weight: 700; color: #000; font-size: 35px;">SAVE UP TO 90% OFF ON</h2> </center> 
                  <br>
                  <center><h4 style="font-weight: 700; color: #000; font-size: 23px">RESTAURANTS, ACTIVITIES, BARS/PUBS,</h4> </center> 
                  <center><h4 style="font-weight: 700; color: #000; font-size: 23px">NIGHTLIFE, THINGS TO DO & PLACES TO SEE!</h4> </center> 
                  <br>
                  <center><h2 style="font-weight: 700; color: #000; font-size: 35px;">NEARBY WHEREVER YOU ARE!!!</h2> </center> 

               </div>

              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
    </div>
      </div>
      <div class="modal-footer">
       <a href="indexVideo.php"> <button type="button" class="btn btn-default btn-modal">Close</button></a>
      </div>
    </div>

  </div>
</div>

<div id="myModal4" class="modal fade ab" role="dialog">
  <div class="modal-dialog modal2-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h2 class="modal-title" style="font-weight: 800; font-size: 40px;"><span>TJ PRIME</span></h2>
      </div>
            <!-- <div class="modal-body"> -->
      <div class="fifth fifth-modal card-s">
      <div class="container-fluid">
        <div class="gift-section">
          <div class="col-md-12">
           
            <div class="doors doors-sm doors-xxs ">
             
              <div class="col-md-12 doors-p"> 
               <div class="door-1-lap cards-section border-none">
                
                  <center><h2 style="font-weight: 700; color: #000; font-size: 40px;">DOWNLOAD THE TJ PRIME <br> GIFT CARD APP</h2> </center> 
                  <br>
                  <center><h4 style="font-weight: 700; color: #000; font-size: 23px;">SEND COMPLIMENTARY $249 BOOKING ENGINE</h4> </center> 
                  <center><h4 style="font-weight: 700; color: #000; font-size: 23px;">GIFT CARDS TO EVERYONE OF YOUR CONTACTS!</h4> </center>
                  
                  <center><h2 style="font-weight: 700; color: #000; font-style: italic;">#ShareTheLove! #MakeMoreFriends!</h2> </center> 
                  
                  <center><h4 style="font-weight: 700; color: #000; font-size: 23px; padding:10px 0 0 0;">GET PAID $50.00 FOR EVERY FULL MEMBER</h4> </center>
                  <div class="cr-client">

                     <div class="cr-client-l">
                       <img  style="width:140px; margin:15px 150px;" src="img/google-app-btn_1.png">
                     </div>
                     <div class="cr-client-r">
                       <img style="width:140px; margin:15px 0px; " src="img/ios-app-store-button.png">
                     </div>
                   </div>
               </div>

              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
    </div>
      </div>
      <div class="modal-footer">
       <a href="indexVideo.php"> <button type="button" class="btn btn-default btn-modal">Close</button></a>
      </div>
    </div>

  </div>
</div>

<div id="myModalVideos" class="modal fade ab" role="dialog">
  <div class="modal-dialog modal2-dialog">
	  
    <!-- Modal content-->
    <div class="modal-content">

            <!-- Image Map Generated by http://www.image-map.net/ -->
            <img src="img/videospop.png"  width="695" height="581" alt="videos" usemap="#video-map" class="videoMap">

            <map name="video-map">
            
           
                <area title="noobligationnecessary" href="noobligationnecessary.html" shape="rect" coords="228,234,60,136" target="noobligation" alt="noobligationnecessary" >
                <div class="container"> <area title="yoda" href="yoda.html" shape="rect" coords="467,118,635,214" target="yoda" alt="yoda">
                <div class="container"> <area title="lovethis" href="lovethis.html" shape="rect" coords="274,236,444,331" target="lovethis" alt="lovethis">
                <div class="container"> <area title="saveaton" href="saveaton.html" shape="rect" coords="58,331,230,428" target="saveaton" alt="saveaton">
                 <div class="container"><area title="juicy" href="juicy.html" shape="rect" coords="458,359,629,450" target="juicy" alt="juicy">
                 <div class="container"><area title="close" href="indexVideo.php" shape="rect" coords="296,524,395,560" target="" alt="close">
            </div>
            </map>

    </div>
  </div>
</div>

    </body>
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
     <script type="text/javascript" src="jquery.sticky.js"></script>
     <script type="text/javascript" src="js/jquery.maphilight.js"></script>
    <script type="text/javascript">
        $(function () { 
            $('.videoMap').maphilight();
        });
    </script>
    <script type="text/javascript">
      
      $(document).ready(function(){
         
        var visitors=98;
        setInterval(function(){

          $(".visitors").fadeIn("slow");
          if(visitors>300)
          {
            $(".visitor_no").text(100);
            visitors=100;
          }
          else{
            $(".visitor_no").text(visitors);
          }
          visitors+=13;
          $(".visitors").fadeOut(4000);
        }, 5000);


        var tech = window.location.search.substring(1);          
          if(tech == 'gift_section'){
            $(".down_part").show();
            $(".up_part").hide();
            $(".head").show();

          }else{
            var lastScrollTop = 0;
            $(window).scroll(function(event){
               var st = $(this).scrollTop();
               if (st > lastScrollTop){
                   // downscroll code 
                  window.setTimeout(function() {                 
                    window.location.replace("http://traveljolly.com/agent/indexVideo.php?gift_section");
                  }, 2000);
               } 
               
               lastScrollTop = st;
            });
          }
        $('#share_codebtn').click(function(){
          var share_code = $('input[name=share_code]').val();
          // alert(share_code); 
          if(share_code=="")
          {
            alert("Please enter your code");
            return false;
           
          }
          else
          {
            window.open("https://join.shoretel.com/conference/"+share_code,'_blank'); 
          }
        });


      });
      
    </script>
    <script type="text/javascript">
      (function blink() { 
  $('.blink_me').fadeOut(500).fadeIn(500, blink); 
})();
    </script>

 <!-- Start of REVE Chat Script-->
 <script type='text/javascript'>
 window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account='9571569';w.__revechat_version=2;
   r._= []; var rc = d.createElement('script'); rc.type = 'text/javascript'; rc.async = true; rc.setAttribute('charset', 'utf-8');
   rc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.revechat.com/widget/scripts/new-livechat.js?'+new Date().getTime();
   var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(rc, s);
 })(document, window);
</script>
<!-- End of REVE Chat Script -->

 </body>
</html>