<!DOCTYPE html>
<html>
<head>
    <title>Landing Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link rel="stylesheet" type="text/css" href="merge_pages/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="merge_pages/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="merge_pages/css/style-1.css">
    <link rel="stylesheet" type="text/css" href="merge_pages/css/responsive.css">
    <!-- for second  -->
    <link rel="stylesheet" type="text/css" href="merge_pages/css/coming_soon.css">
    <link rel="stylesheet" type="text/css" href="merge_pages/css/responsive1.css">
    <!-- for third -->
    <link rel="stylesheet" type="text/css" href="merge_pages/css/coming_soon1.css">
    <link rel="stylesheet" type="text/css" href="merge_pages/css/style-12.css">
    <link rel="stylesheet" type="text/css" href="merge_pages/css/responsive2.css">


    <!-- for fourth -->
    <link rel="stylesheet" type="text/css" href="merge_pages/css/style-13.css">
    <link rel="stylesheet" type="text/css" href="merge_pages/css/responsive3.css">
     <link rel="stylesheet" type="text/css" href="merge_pages/css/coming_soon2.css">
    <!--  for fifth -->
 <link rel="stylesheet" type="text/css" href="merge_pages/css/style-14.css">
    <link rel="stylesheet" type="text/css" href="merge_pages/css/coming_soon3.css">
    <link rel="stylesheet" type="text/css" href="merge_pages/css/responsive4.css">


    

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" type="images/x-icon" href="merge_pages/img/favicon.png">
    <script type="text/javascript" src="merge_pages/jquery.sticky.js"></script>
    <style type="text/css">
           #custom_carousel .item {
            color: #000;
            background-color: #eee;
            padding: 20px 0;
        }
        
        #custom_carousel .controls {
            overflow-x: auto;
            overflow-y: hidden;
            padding: 0;
            margin: 0;
            white-space: nowrap;
            text-align: center;
            position: relative;
            background: #ddd
        }
        
        #custom_carousel .controls li {
            display: table-cell;
            width: 1%;
            max-width: 90px
        }
        
        #custom_carousel .controls li.active {
            background-color: #eee;
            border-top: 3px solid orange;
        }
        
        #custom_carousel .controls a small {
            overflow: hidden;
            display: block;
            font-size: 10px;
            margin-top: 5px;
            font-weight: bold
        }
        @media(min-width: 1600px){
      .media{
        padding-right: 0px!important;  
        margin: 0px !important;
      }
      .popover {
      padding-right: 0px!important;
      width: 400px!important;
      margin: 0px !important;
      text-align: center;
        }
        .third-map{
    width : auto;
    height: 456px;
  }
  .google_map{
    height: 834px;
  }
      }
      @media (max-width: 1600px){
.third-map img {
    width: auto;
    height: 456px;
}
}

      @media (max-width: 1439px){
.third-map img {
    width: 100%;
    height: 456px;
}
}

    </style>

   
</head>
  <body>
     <div class="pre-landing">
      <div class="pl-top pre">
        <div class="container-fluid">
          <div class="pre-top">
            <center><h4>Connected in more and more ways than you have ever witnessed before</h4></center>
          </div>
          <div class="pre-header">
            <center><h3>
              You decide where you want top go! Let us supply the editable intenerary!<br>
              You have a great time, save money and enjoy yourself MORE!
            </h3></center>
          </div>

          <!-- <div class="pl-toph">
            <h3>
              You decide where you want top go! Let us supply the editable intenerary!<br>
              You have a great time, save money and enjoy your self MORE!
            </h3>
          </div>
          <div class="pl-top-strip">
            <p>Connected in more and more ways than you have ever witnessed before</p>
          </div> -->
        </div>
      </div>

      <div class="pl-content abbh">
        <div class="container-fluid">
          <div class="pl-c abhi">
            <p>Travel Local Nearby Deals at Upto 90% off Treasure Map Itineraries, 3 Million Points of Interest, 250,000,000 Data Points, Local Support, Tourist Support, Multi Currency including Bitcoins, Easy to use Trip Planner, 900+ Scheduled Chartered & No Frill Airlines, 800,000 + Hotels & Resorts, 25,000 + Car Hire Locations, 80 + Bed Banks, 140,000 Destinations, 30,00 + Transfer Locations, I5,000 + World's best Attractions, Major Cruise Lines, Multi Language, Membership, Restaraunts, Bars / Pubs, Nightlife, Activities, Shopping, Gift Cards & Hidden Gems, If you own a business let us incentivize you and your employess to create a deal we can market for you! We will make you money and you will be ecstatic wth the relationship you have with TravelJolly.com and our team of waorries! Please be aware we are hiring in your area, help us put the planet at your feet on a silver platter and on an affordable budget so you can have a lot more fun in life!</p>
          </div>    
        </div>
      </div>

      <div class="pl-offers">
        <div class="container-fluid"> 
          <div class="">
              <div class="col-md-4 col-sm-12 col-xs-12 pl-o">
                <div class="o1">
                  <h3>  Upto 90% Off</h3>
                  <p>Places to Stay - Your Itinerary<br> 
                    Places to See - Things to Do<br>
                    Theme Parks - Attractions<br>
                    Eats / Drinks - Shopping
                  </p>
                </div>
               <div class="o2">
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-1">
                      <h4>25,000+</h4>
                      <p>Car hire locations <br>/ worldwide</p>  
                  </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-r">
                    <div class="o2-content c-2">
                      <h4>30,000+</h4>
                      <p>Transfer loctions <br>/  Worldwide</p>  
                    </div>
                  </div>
                
                   <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-3">
                      <h4>140,000+</h4>
                      <p>Destinations</p>  
                    </div>
                   </div>
                   <div class="col-md-6 col-sm-6 col-xs-6 o2-r">
                      <div class="o2-content c-4">
                        <p>Major</p>  
                        <h4>Cruise lines</h4>
                      </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 o2-bottom">
                      <div class="o2-b c-5">
                        <h4>180+ Countries</h4>
                      </div>
                    </div>
                </div>
              </div>
              <!-- second second -->
              
              <div class="col-md-4 col-sm-12 col-xs-12 pl-o">
                <!-- <div class="o-3">
                  <h3>  Upto 90% Off</h3>
                  <p>Places to Stay - Your Itinerary<br> 
                    Places to See - Things to Do<br>
                    Theme Parks - Attractions<br>
                    Eats / Drinks - Shopping
                  </p>
                </div> -->
               
                  
                <div class="o-3">
                  <div class="col-md-5 col-sm-5 col-xs-5 o-3-sections1">
                      <div class="o-3-content c-6">
                        <h4>800,000+</h4>
                        <p>Hotels, Villas and<br>Appartments</p>  
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-3 o-3-sections2">
                      <div class="o-3-content c-1">
                        <h4>80+</h4>
                        <p>bed banks</p>  
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4 o-3-sections1">
                      <div class="o-3-content c-6 c-6a">
                        <h4>Multi <br>Currency</h4>
                        <!-- <p>Car hire locations <br>/ worldwide</p>   -->
                    </div>
                  </div>
                </div>

                  <div class="o2">
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-7">
                      <h4>900+</h4>
                      <p>Scheduled, Chartered <br>& no-frills airlines</p>  
                  </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-r">
                    <div class="o2-content c-8">
                      <h4>Bitcoin </h4>
                      <p>Currencey</p>  
                    </div>
                  </div>
                
                   <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-9">
                      <h4>15,000+</h4>
                      <p>Worlds best <br>attractions</p>  
                    </div>
                   </div>
                   <div class="col-md-6 col-sm-6 col-xs-6 o2-r">
                      <div class="o2-content c-10">
                        <p>Tourist Support</p>  
                        <h4>Been There Done That</h4>
                      </div>
                    </div>
                    <div class="for_lg">
                    <div class="col-md-12 col-sm-12 col-xs-12 o3-bottom">
                     <div class="o1 c-12">
                      <h3>  Easy To Use Trip Planner</h3>
                      <p>4 easy steps to Ian your trip itinerary
                      </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- third section -->
              
              <div class="col-md-4 col-sm-12 col-xs-12 pl-o">
               <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="o3-main c-13">
                 <h3>Nearby Deals</h3>
                 <p>Restaraunts <br>
                    Things To Do<br>
                    Activities, Nightlife<br>
                    Bars/Pubs, Monuments<br>
                    Theme Parks Museams
                  </p>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6 ">
                 <div class="o3-main-r1 c-14">
                   <h3>3 Million+</h3>
                   <p>Points of Interest</p>
                 </div>
                 <div class="o3-main-r2 c-15 clearfix">
                   <h3>Nearby </h3>
                   <p>Thats it!</p>
                 </div>
               </div>
               <div class="clearfix"></div>
               <div class="o2">
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-16">
                      <h4>Multi <br> Language</h4>
                      <!-- <p>Scheduled, Chartered <br>& no-frills airlines</p>   -->
                  </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 c-16 o2-r">
                    <div class="o2-content c-8">
                     <h4>Iteneraries </h4>
                      <p>Pre-Made</p>    
                    </div>
                  </div>
              </div>
              <div class="o2">
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-7">
                      <h4>Local Support</h4>
                      <p>Been There <br>Done That</p>  
                  </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-r">
                    <div class="o2-content c-17">
                      <h4>Treasure Maps </h4>
                      <p>Editable</p>  
                    </div>
                  </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="pre_load_img">
                  <img src="merge_pages/img/pre_landing.jpg">
                </div>
              </div>
              <div class="for_xs">
                 <div class="col-md-12 col-sm-12 col-xs-12 o3-bottom">
                     <div class="o1 c-12">
                      <h3>  Easy To Use Trip Planner</h3>
                      <p>4 easy steps to Ian your trip itinerary
                      </p>
                      </div>
                    </div>
              </div>

          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="straight-line"></div>
    </div>

    <div class="trip-planner-section">
      <div class="container-fluid">
        <div class="trip-planner">
          <div class="tp-top">
            <center><h4>We provide everyone the world's best trip planner, saving you hours of frustration in just a few minutes, Simply modify one of our itineraries and be filled with excitment on every single step of your journey!</h4></center>
          </div>
          <div class="tp-body">
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="merge_pages/img/icon-1.png">
              </div>
              <div class="arrow">
                  <img src="merge_pages/img/arrow.png">
              </div>
              <center><p><b>Tell as where you want to go?</b><br><br>Simply answer a few questions about the trip you want to plan.</p></center>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="merge_pages/img/icon-2.png">
              </div>
              <div class="arrow">
                  <img src="merge_pages/img/arrow.png">
              </div>
               <center><p><b>Choose a Treasure Map Iteneary based on your needs?</b></p></center>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="merge_pages/img/icon-3.png">
              </div>
              <div class="arrow">
                  <img src="merge_pages/img/arrow.png">
              </div>
              <center><p><b>Need to add anything, Daily?</b><br><br>A complete day-by-day itinerary based on your preferences. If it is missing anything, add it!</p></center>
              
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="merge_pages/img/icon-4.png">
              </div>
              <div class="arrow">
                  <img src="merge_pages/img/arrow.png">
              </div>
              <center><p><b>Customize it </b><br><br>best routes and schedules <br>Refine your plan. We'll find that</p></center>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="merge_pages/img/icon-5.png">
              </div>
              <div class="arrow1">
                  <img src="merge_pages/img/arrow.png">
              </div>
              <center><p><b>Book it</b><br><br>Choose your activities, hotel, restaurants, Nightlife & transportation upto 90% off Make ticket, hotel reservations</p></center>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="merge_pages/img/icon-6.png">
              </div>
              <center><p><b>Manage it online & Use it offline</b><br><br>Everything in one place.<br>
                Everyone on the same page.<br>Download the plan as your on-the go!</p></center>
            </div>
            <div class="clearfix"></div>
            <div class="tp-footer">
              <center><h4>Customizable Treasure Map Itineraries created by both locals and tourist, claiming to have been there, done that and bought the T-Shirt!</h4></center>
            </div>

          </div>
        </div>
      </div>
    </div>
      
    
      <div class="footer">
        <div class="container-fluid">
          <center><p><b>How Travel Jolly's Trip Planner Works</b><br>
Discover how to create ycur trip itinerary in 6 steps
 </p></center>
        </div>
      </div>

          <!-- second  -->


          <div class="second2">
            <div class="coming_soon">
        <div class="cs_banner">
            <div class="cs_banner_content">
                <!-- <img src="img/coming_soon_bg.png"> -->
                <div class="cs_tj">
                    <img src="merge_pages/img/tj-1.png">
                    <h3>HOW TJʼS TRIP PLANNER <br>WORKS</h3>
                        
                        <div class="csb_left">
                            <div class="csb_lc">
                                <h4>Discover the ultimate Trip Itinerary in<br>
                                    <span>6 EASY STEPS</span>
                                </h4>
                                <p>We simply ask you a series questions to create a trip profile for you using over 250,000.000 data points and based on the results we match that profile to a list of customizable itineraries with built in treasure maps to choose from.</p>
                               
                            </div>
                            <div class="csb_arrow">
                                <img src="merge_pages/img/tj-right.png">
                            </div>
                        </div>
                </div>
            </div>

        </div>

        <div class="cs_form_content">
        <div class="cs_form">
            <div id="first-slider">
                            <div id="carousel-example-generic" class="carousel slide carousel-fade" data-interval="false">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                </ol>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <!-- Item 1 -->
                                    <div class="item active slide1">
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <img style="max-width: 100%;"  data-animation="animated zoomInLeft" src="merge_pages/img/coming_soon/1.png">
                                            </div>
                                         
                                        </div>
                                           
                                            <a class="right carousel-control first-c" href="#carousel-example-generic" role="button" data-slide="next">
                                               <div class=""> <span>START EXPLORING</span>
                                                </div>
                                            </a>

                                      
                                     </div> 
                                    <!-- Item 2 -->
                                    <div class="item slide2">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                 <img style="max-width: 100%;"  data-animation="animated zoomInLeft" src="merge_pages/img/coming_soon/2.png">
                                             </div>
                                            <!-- <div class="col-md-5 text-right">
                                                <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="http://s20.postimg.org/sp11uneml/rack_server_unlock.png">
                                            </div> -->
                                        </div>
                                             <a class="right carousel-control first-c" href="#carousel-example-generic" role="button" data-slide="next">
                                               <div class=""> <span>KEEP EXPLORING</span>
                                                </div>
                                            </a>
                                        </div>
                                    <!-- </div> -->
                                    <!-- Item 3 -->
                                    <div class="item slide3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                 <img style="max-width: 100%;"  data-animation="animated zoomInLeft" src="merge_pages/img/coming_soon/3.png">
                                             </div>
                                              
                                        </div>
                                        <a class="right carousel-control first-c" href="#carousel-example-generic" role="button" data-slide="next">
                                               <div class=""> <span>KEEP EXPLORING</span>
                                                </div>
                                            </a>
                                        </div>
                                    <!-- </div> -->
                                    <!-- Item 4 -->
                                    <div class="item slide4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                 <img style="max-width: 100%;"  data-animation="animated zoomInLeft" src="merge_pages/img/coming_soon/4.png">
                                             </div>
                                           
                                        </div>
                                         <a class="right carousel-control first-c" href="#carousel-example-generic" role="button" data-slide="next">
                                               <div class=""> <span>KEEP EXPLORING</span>
                                                </div>
                                            </a>
                                        </div>
                                   
                                    <!-- End Item 4 -->
                            
                                </div>
                                <!-- End Wrapper for slides-->
                                
                            </div>
                </div>
             </div>
        </div>
        <div class="b-bottom">
            <h3>HAVE A MUCH MORE EXCITING TRIP - SAVE A LOT OF TIME - SAVE A LOT OF MONEY</h3>
        </div>
    </div>


    <!-- step 2 -->

    <div class="step-second">
        
        <div class="col-md-6 col-md-push-6 ">
            <div class="second-right">
                <div class="second-circle"><p>2</p></div>
                <h3>CHOOSE AN ITINERARY</h3>
                <div class="second-content">
                    <div class="days">
                        <div class="days-left">
                            <p>1-Day</p>
                        </div>
                        <div class="days-right">
                            <p>ON A BUDGET!</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>2-Day</p>
                        </div>
                        <div class="days-right">
                            <p>ON A BUDGET!</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>3-Day</p>
                        </div>
                        <div class="days-right">
                            <p>OF LUXURY</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>4-Day</p>
                        </div>
                        <div class="days-right">
                            <p>OF REST AND RELAXATION ON THE BEACH</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>5-Day</p>
                        </div>
                        <div class="days-right">
                            <p>OF CHEAP BREAKFAST & INCREDIBLE NIGHTLIFE</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>7-Day</p>
                        </div>
                        <div class="days-right">
                            <p>IN AN EXOTIC LAND</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>8-Day</p>
                        </div>
                        <div class="days-right">
                            <p>WITH NO KIDS IN A LAND FAR FAR AWAY</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>10-Day</p>
                        </div>
                        <div class="days-right">
                            <p>AWAY FROM MY FAMILY ALONE ANYWHERE</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>13-Day</p>
                        </div>
                        <div class="days-right">
                            <p>IN YOUR FAVORITE CITY</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>14-Day</p>
                        </div>
                        <div class="days-right">
                            <p>ON A SAFARI</p>
                        </div>
                    </div>

                    <div class="second-content-bottom">
                        <h3>If you can dream it and have a desire to go, <br>
                            Let us help you make it happen!</h3>
                            <h3 class="scb">We want to make your dreams come true!</h3>
                    </div>

                </div>

            </div>
        </div>
        <div class="col-md-6 col-md-pull-6">
            <div class="second-left">
                <h3 class="smain">AFTER A FEW QUICK QUESTIONS</h3>
                <div class="smain2">
                    <h3 class="smain2">
                        We simply <span class="h-pink">Match</span> your
                        <span class="h-green">Trip Profile</span> to highly reviewed 
                        pre-made <span class="h-red">Customizable</span> 
                        <span class="h-dblue">Itineraries</span> that already have <span class="h-blue">Built In </span>
                        <span class="h-yellow">Treasure Maps </span> 
                        loaded with <span class="h-lgreen">Hidden Gems</span>, 
                        <span class="h-blue">Golden Places </span>- Planned<span class="h-pink"> Transportation</span> and 
                        <span class="h-lred">Exciting Things to See and Do</span>.
                    </h3>
                </div>
                <div class="sl-offer">
                    <h3>
                        Save up to 65% on Travel & up to 90% off <br>
                        on Restaurants, Pubs/Clubs & Activities!
                    </h3>   
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
        
        <!-- Step 3 -->

        <div class="step-third">
                
                <div class="col-md-6 col-md-push-6">
                    <div class="third-right">
                        <div class="second-circle thirdc"><p>3</p></div>
                        <div class="third-content">
                            <h3>Now edit the Treasure Map Itinerary <br>
                            of choice to your exact personal desires.</h3>
                            <p>Have fun and customize the Day-by-day Itinerary. <br>
                                The schedule is customizable and already has a built-in <br>
                                treasure map. The itinerary plan includes your desired <br>
                                types of travel activities, attractions, restaurants, tours,<br> 
                                nightlife, events, transportation needs <br>
                                and accommodations that altogether fit your <br>
                                trip profile desires.
                            </p>
                            <p class="p-bold">TJ’s Power Planning Engine continuously scans<br> and optimizes for such details as shown to the far left.<br>
                                Our revolutionary Power Planning Engine has <br>
                                even considered the weather throughout <br>
                                the entire process.</p>
                        </div>
                    </div>
                </div>  
                <div class="col-md-6 col-md-pull-6">
                    <div class="third-left">
                        <div class="col-md-2 tl-img">
                            <img src="merge_pages/img/coming_soon/1a.png">
                            <img src="merge_pages/img/coming_soon/2a.png">
                            <img src="merge_pages/img/coming_soon/3a.png">
                            <img src="merge_pages/img/coming_soon/4a.png">
                            <img src="merge_pages/img/coming_soon/5a.png">
                            <img src="merge_pages/img/coming_soon/6a.png">
                        </div>
                        <div class="col-md-10">
                            <div class="third-map">
                                <!-- <iframe src="https://maps.google.com/maps?q=orlando&t=&z=13&ie=UTF8&iwloc=&output=embed"></iframe> -->
                                <img src="merge_pages/img/map2.jpg">
                                <h3>You are <span>NOW EMPOWERED</span> with 
                                    Super Human Trip Planning Powers!</h3>
                            </div>
                        </div>
                    </div>
                </div>         
        </div>
        <div class="clearfix"></div>
        <!-- Step - 4 -->

        <div class="step-fourth">
            
            
            <div class="col-md-6 ">
                <div class="fourth-left">
                    <div class="sixthc fourthc"><p>4</p></div>
                    <h3>CUSTOMIZE YOUR PLAN TO FIT YOUR NEEDS!</h3>
                    <h4>Make necessary changes regarding various features of your plan.</h4>
                    <p>Move any feature of the itinerary along the plan backward and forward. <br>
                        Check to see when any feature of your itinerary is open or closed.<br>
                        Extend or Shorten the duration of any feature of the itinerary.<br>
                        Invite or add co-travelers to access your itinerary if you want.<br> 
                        Receive recommendations on when the best time for visit is.<br> 
                        Minimize travel time and enjoy every moment.<br>
                        Optimize your Itinerary to your hearts content.<br>
                        Attach notes to every feature of the itinerary.<br>
                        Add or Remove any feature of the itinerary.
                    </p>
                        <h4 class="fourth-color">
                            Swap out restaurants to Sushi or 
                            Mexican VS American as an example 
                            or swap out the Night Life and attractions 
                            for a better fit to your needs and budget.
                             It’s that simple!
                        </h4>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="fourth-right">
                    <div class="second-circle fourthc fourthc1"><p>4</p></div>
                    <div class="fourth-content">
                        <img src="merge_pages/img/coming_soon/4th-img.png">
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        
        <!-- step fifth  -->

        <div class="step-five">
           
            <div class="col-md-6 col-md-push-6">
                
                 <div class="fifth-right">
                    <div class="second-circle fifthc"><p>5</p></div>
                    <div class="fifth-content">
                        <img src="merge_pages/img/5th_step.jpg">
                        <!-- <h4>Now Invest in Your Treasure Map, complete with  Fat Savings & Slim Prices!<br>
                            You have more time, to have a lot more fun!<br>
                            And you have more money for  Souvenirs to bring home!<br>
                            Book the entire itinerary while supplies last and SAVE additional 15% now!<br>
                            Minimum 7 days advance purchase of  your departure dates required!</h4> -->
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <div class="five-left">
                    
                <div class="fifth-content">
                        <img src="merge_pages/img/HowtheTJmap.jpg">
                        
                    </div>

                <!-- <div class="fifthr-bottom">
                    
                    <h3>Orlando Trip Planner</h3>
                    <div class="stars">
                       <ul>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-full"></i></li>
                        </ul>
                        <p>(335 399)</p>
                            
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <div class="stars-right">
                        
                    </div>
                </div> -->
            </div>

            </div>
        </div>
<div class="clearfix"></div>
         <!-- sixth step -->

        <div class="step-six">
            <div class="col-md-6">
                <div class="step-sixth-border"></div>
                <div class="six-left">
                    <div class=""><div class="sixthc sixc"><p>6</p></div>
                    </div>
                    <h3>Your complete plan in one place!</h3>
                    <h4>Congratulations</h4>
                    <h5>Its time you get paid back for your time and efforts.</h5>
                    <p>You have Completed yet another step in a rewarding Journey with TJ. Feel free to keep your itinerary completely private or share it both online and offline. We encourage you to share your itinerary with your friends, family, and the world! Download your itinerary and not only use it but share it offline!
                    We designed TJ for all of us. & we want you to know how much we value your time and efforts.
                    By combining our efforts, we continue to build on what is to be a wonderful ongoing journey together. A journey that’s full of excitement, provides you & the TJ Community as a whole with the ability to earn additional revenue, play, travel & support our communities as a whole </p><br>
                    <p><b>Once you have traveled and used your itinerary improve upon it if you can.</b><br>
                    •   Challenge yourself to create the very best itinerary with the very best treasure map built in.<br>
                    •   Add that extra hidden gem you may have found while traveling to make it even better, update the details or tell your story about a spec ific product, service, ticket, or place.<br>
                    •   All this helps you reap rewards from your itinerary and makes sure others really enjoy their travels as well.
                    </p><br>
                    <p><b>TJ Members generate revenue when sharing their itineraries that contain built in Treasure Maps.</b>  <a href="" class="six-button" data-toggle="modal" data-target="#myModal">Read More..</a></p><br>
                    <p>

                </div>
                <!-- <div class="six-btn"> 
                 <a href="" class="six-button" data-toggle="modal" data-target="#myModal">Read More..</a>
                </div> -->
                <div class="six-share">
                    <h4>Share On</h4>
                    <ul>
                        <li><a href=""><img src="merge_pages/img/facebook-1.png"></a></li>
                        <li><a href=""><img src="merge_pages/img/twitter1.png"></a></li>
                        <li><a href=""><img src="merge_pages/img/instagram1.png"></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="six-right">
                    <div class="second-circle abc"><p>6</p></div>
                     <div class="sixth-content">
                        <img src="merge_pages/img/coming_soon/map.png">
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="coming_soon_boottom">
            <center><h3>www.traveljolly.com</h3></center>
        </div>




<!--  -->
<!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                  <div class="modal-dialog coming_soon_modal">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Your complete plan in one place!</h4>
                      </div>
                      <div class="modal-body cs-body">
                        
                               <p> •   Treasure Maps are filled with TJ Pin Partner Businesses that TJ Members have recommended TJ Support with TJ's Marketing services.<br>

                                •   These businesses have agreed to discount their products and or services to the local community & tourist visiting their area. These discounts become valuable treasures and hidden gems within TJ.<br>

                                •   Agreements produce treasures and hidden gems of up to 90°/o off at Restaurants, Pubs I Clubs, Activities, Things to Do & Places to See within a

                                community . <br>Agreements I Deals are then available to the local community and visiting tourist via TravelJolly .com<br>

                                •   TJ Members are encouraged and generate revenue nudging businesses they have personally enjoyed themselves & may personally know the staff.
                                </p><br>
                                    <p>1.  To learn more about how to be paid for your time and efforts by nudging businesses to become a TJ Pin Partner Businesses & easily generate yourself additional ongoing revenue click here.<br>

                                        2.  Click Here about how to make money with your Itineraries.<br>

                                        3.  Add itionally , there are currently several other ways to earn revenue with TJ as a member Click Here.<br>

                                        4.  Click here to find additional ways to improve your travels in life and be paid for it. We are also trying to hire at light speed . Send your resume and setup an appointment with us. Learn more about our hiring needs.<br>
                                        •   Example: Want a press pass?
                                </p><br>
                                <p>
                                    <b>Travelers review itineraries (Gold Stars) and easily recognize a member's time and efforts.</b><br><br>

                                    The better your reviews the more travelers will use your itinerary template. You get paid when travelers purchase the deals, treasures & hidden gems that are sprinkled throughout a good itinerary .<br><br>

                                    <b>Success Principle Notes</b>: You cannot give of yourself without receiving. It is the same as to have or find a friend you must first start by being a friend. You can not cozy up to a fireplace and get heat from it unless you do what is needed to create the fire first. A farmer must first work the ground till the soil plant the seed water the field, get rid of the weeds and continually care for the field before he can harvest the field and generate revenue! Such is life!<br><br>
                                    We are in an endless pursuit to not only support you in your needs but the community you live in as well.<br><br>

                                    We are working to make the world a greener place, protect our children and help provide a fully funded higher education for all children.<br><br>

                                    Mission Statement: "Toprotect a parents most valuable investment their  children, and the communities most valuable investment, its future!"<br><br>

                                    We are a part of the rGreenConsortium.com initiative .<br><br>

                                    <b>As you explore more of TJ you will find additional ways to profit from your time and efforts, many things beyond just money , initiatives we have wrapped ourselves around that heal communities. Please do your part its easy , simply do not forget to #ShareTheLove . Use your Super Powers!!!</b>

                                </p>


                            
                      </div>
                      <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                      </div>
                    </div>

                  </div>
                </div>

          </div>

          <div class="clearfix"></div>
          <!-- third page -->



          <div class="third_page">
            <div class="map">
                
                <!-- top -->
              <div class="container-fluid">
                <div class="col-md-3 map_left_padding">
                  <div class="map_left">
                    <div class="col-md-6 mlb">
                      <p>Basic Map</p>
                    </div>
                    <div class="col-md-6 mlb tre">
                      <p>Treasure Map</p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="google_search">
                      <img src="merge_pages/img/map/2.jpg">
                    </div>
                    <div class="clearfix"></div>

                    <div class="map_deals">
                      <div class="md_header">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <p>Featured Places in Orlando</p>
                      </div>
                      <div class="district">
                        <div class="d_top">
                          <p><b>District</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Maingate West</a><p>&nbsp; 189 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Winter Park</a><p>&nbsp;20 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Maingate East</a><p>&nbsp; 19 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">North Orlando</a><p>&nbsp;120 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">West Orlando</a><p>&nbsp; 70 Hotels</p>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="district_margin"></div>
                      <div class="district">
                        <div class="d_top">
                          <p><b>Neighbourhoods</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Kissimmee</a><p>&nbsp; 253 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Orlando CBD</a><p>&nbsp;1381 Hotels</p>
                          </div>
                          
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="district_margin"></div>

                      <div class="district">
                        <div class="d_top">
                          <p><b>Suburbs</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Maingate West</a><p>&nbsp; 189 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Winter Park</a><p>&nbsp;20 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Maingate East</a><p>&nbsp; 19 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">North Orlando</a><p>&nbsp;120 Hotels</p>
                          </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                      <div class="airoplan">
                        <div class="md_header">
                        <i class="fa fa-plane" aria-hidden="true"></i>
                        <p>Airports</p>
                      </div>
                      <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Maingate East</a><p>&nbsp; 19 MCO</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">North Orlando</a><p>&nbsp;120 SFB</p>
                          </div>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Maingate East</a><p>&nbsp; 19 MLB</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">North Orlando</a><p>&nbsp;120 ORL</p>
                          </div>
                        </div>
                      </div>


                      <div class="clearfix"></div>
                      <div class="airoplan">
                        <div class="md_header">
                        <i class="fa fa-home" aria-hidden="true"></i>
                        <p>Places Of Interests</p>
                      </div>

                      <div class="district">
                        <div class="d_top">
                          <p><b>Tourist Regions</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Maingate West</a><p>&nbsp; 189 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Winter Park</a><p>&nbsp;20 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Maingate East</a><p>&nbsp; 19 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">North Orlando</a><p>&nbsp;120 Hotels</p>
                          </div>
                          
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">West Orlando</a><p>&nbsp; 70 Hotels</p>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="district_margin"></div>
                      <div class="district">
                        <div class="d_top">
                          <p><b>Business</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Kissimmee</a><p>&nbsp; 253 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Orlando CBD</a><p>&nbsp;1381 Hotels</p>
                          </div>
                          
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="district_margin"></div>

                      <div class="district">
                        <div class="d_top">
                          <p><b>Entertainment</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="district_margin"></div>

                      <div class="district">
                        <div class="d_top">
                          <p><b>Institutions</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                        </div>
                      </div>


                      <div class="clearfix"></div>
                      <div class="district_margin"></div>

                      <div class="district">
                        <div class="d_top">
                          <p><b>Shopping</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="district_margin"></div>

                      <div class="district">
                        <div class="d_top">
                          <p><b>Sights</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="district_margin"></div>

                      <div class="district">
                        <div class="d_top">
                          <p><b>Things To Do</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                        </div>
                         <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                        </div>
                      </div>
                      </div>
                      <div class="clearfix"></div>

                      <div class="airoplan">
                        <div class="md_header">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <p>Places Nearby Orlando</p>
                      </div>
                      <div class="district">
                        <div class="d_top">
                          <p><b>Cities</b></p>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                        </div>
                         <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                          </div>
                        </div>
                        <div class="d_bottom">
                          <div class="col-md-6 db1">
                            <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                          </div>
                          <div class="col-md-6 db2">
                            <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                          </div>
                        </div>
                      </div>

                      </div>
                    </div>

                  </div>
                </div>
                
                <!-- Middle -->

                <div class="col-md-6">
                  <div class="map_middle">
                    <div class="col-md-2 mm">
                      <p>4 DAYS</p>
                    </div>
                    <div class="col-md-5 mm1">
                      <div class="mm1a">
                        <P>OF CENTRAL FLORIDA MAGIC ON BUDGET!</P>
                      </div>
                    </div>
                    <div class="col-md-5 mm2">
                      <P>FLIGHTS & HOTELS PAID FOR SEPERATELY </P>
                    </div>
                    <div class="clearfix"></div>
                    <div class="sort">
                      <div class="sort_below">
                      <!-- <img src=" img/map/1.jpg"> -->
                      <h4>CLICK THE LINKS BELOW TO ADJUST THE TYPE OF MAP PINS DSIPLAYED</h4>
                      <ul>
                        <li>
                          Show All
                        </li>
                        <li>
                          Retaurants 
                        </li>
                        <li>
                          Things To Do
                        </li>
                        <li>
                          Shopping 
                        </li>
                        <li>
                          <a href=" ">Theme Park </a>
                        </li>
                        <li class="no_border">
                          Bars & Nightlife
                        </li>
                      </ul>
                      <div class="sort_bottom">
                        <button type="button" data-toggle="modal" data-target="#myModal001">Search</button>
                        <div class="sort_more">
                            <p>More</p>
                            <span class="caret"></span>
                        </div>
                        <div class="sort_more sort_more1">
                            <p>Sort By</p>
                            <span class="caret"></span>
                        </div>
                        <div class="full_screen">
                          <img src="merge_pages/img/map/full_screen.jpg">
                        </div>
                      </div>
                    </div>
                    <div class="sort_img">
                      <center><h2>FAT SAVINGS</h2></center>
                      <center><h3>SLIM PRICES</h3></center>
                    </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="google_map">
                      <div class="fist_row_location">
                      <div class="col-md-2">
                        <div class="pin1">
                        <a class="location1" data-toggle="popover2"><img src="merge_pages/img/map/icons/11.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin2">
                        <a class="location1" data-toggle="popover" ><img src="merge_pages/img/map/icons/7.png"></a>
                        </div>
                      </div>
                      
                      <div class="col-md-2">
                        <div class="pin3">
                        <a class="location1" data-toggle="popover3"><img src="merge_pages/img/map/icons/3.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin4">
                        <a class="location1" data-toggle="popover"><img src="merge_pages/img/map/icons/10.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin5">
                        <a class="location1" data-toggle="popover3" ><img src="merge_pages/img/map/icons/18.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin6">
                        <a class="location1" data-toggle="popover2"><img src="merge_pages/img/map/icons/14.png"></a>
                        </div>
                      </div>
                      </div>
                      <div class="clearfix"></div>
                      

                      <!-- second row -->



                      <div class="second_row_location">
                        <div class="col-md-2">
                        <div class="pin7">
                        <a class="location1" data-toggle="popover" ><img src="merge_pages/img/map/icons/1.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin8">
                        <a class="location1"><img src="merge_pages/img/map/icons/15.png"></a>
                        </div>
                      </div>
                      
                      <div class="col-md-2">
                        <div class="pin9">
                        <a class="location1"><img src="merge_pages/img/map/icons/17.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin10">
                        <a class="location1"><img src="merge_pages/img/map/icons/9.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin11">
                        <a class="location1" data-toggle="popover" ><img src="merge_pages/img/map/icons/7.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin12">
                        <a class="location1" ><img src="merge_pages/img/map/icons/2.png"></a>
                        </div>
                      </div>
                      </div>

                      <!-- third row -->
                      <div class="clearfix"></div>

                      <div class="second_row_location">
                        <div class="col-md-2">
                        <div class="pin13">
                        <a class="location1" data-toggle="popover2" ><img src="merge_pages/img/map/icons/14.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin14">
                        <a class="location1"><img src="merge_pages/img/map/icons/18.png"></a>
                        </div>
                      </div>
                      
                      <div class="col-md-2">
                        <div class="pin15">
                        <a class="location1" data-toggle="popover"><img src="merge_pages/img/map/icons/4.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin16">
                        <a class="location1"><img src="merge_pages/img/map/icons/5.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin17">
                        <a class="location1" ><img src="merge_pages/img/map/icons/17.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin18">
                        <a class="location1"><img src="merge_pages/img/map/icons/1.png"></a>
                        </div>
                      </div>
                      </div>


                      <!-- fourth row -->

                       <!-- third row -->
                      <div class="clearfix"></div>

                      <div class="second_row_location">
                        <div class="col-md-4">
                        <div class="pin19">
                        <a class="location1" data-toggle="popover" ><img src="merge_pages/img/map/icons/16.png"></a>
                        </div>
                      </div>
                      <!-- <div class="col-md-2">
                        <div class="pin14">
                        <a class="location1" data-toggle="popover" ><img src="img/map/icons/15.png"></a>
                        </div>
                      </div> -->
                      
                      <div class="col-md-2">
                        <div class="pin20">
                        <a class="location1" data-toggle="popover2"><img src="merge_pages/img/map/icons/14.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin21">
                        <a class="location1"><img src="merge_pages/img/map/icons/1.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin22">
                        <a class="location1" ><img src="merge_pages/img/map/icons/9.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin23">
                        <a class="location1"><img src="merge_pages/img/map/icons/11.png"></a>
                        </div>
                      </div>
                      </div>

                      <!-- fifth row -->

                       <!-- third row -->
                      <div class="clearfix"></div>

                      <div class="fifth_row_location">
                        <!-- <div class="col-md-2">
                        <div class="pin13">
                        <a class="location1" data-toggle="popover" ><img src="img/map/icons/2.png"></a>
                        </div>
                      </div> -->
                      <div class="col-md-4">
                        <div class="pin24">
                        <a class="location1" data-toggle="popover3" ><img src="merge_pages/img/map/icons/6.png"></a>
                        </div>
                      </div>
                      
                      <div class="col-md-2">
                        <div class="pin25">
                        <a class="location1" data-toggle="popover2"><img src="merge_pages/img/map/icons/11.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin26">
                        <a class="location1"><img src="merge_pages/img/map/icons/14.png"></a>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="pin27">
                        <a class="location1"><img src="merge_pages/img/map/icons/11.png"></a>
                        </div>
                      </div>
                      
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-md-12">
                        <div class="pin28">
                        <center><a class="location1" data-toggle="popover" ><img src="merge_pages/img/map/icons/13.png"></a></center>
                        </div>
                      </div>

                      <!-- <div class="third_row_location">
                         <div class="col-md-6">
                          <div class="pin5">
                          <a class="location1" data-toggle="popover" ><img src="img/map/location2.png"></a>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="pin4">
                          <a class="location1" data-toggle="popover" ><img src="img/map/shopping.png"></a>
                          </div>
                        </div>
                      </div> -->

                      <div class="clearfix"></div>
                      <!-- <div class="fourth_row_location">
                        <div class="col-md-4">
                          <div class="pin6">
                          <a class="location1" data-toggle="popover" ><img src="img/map/things-to-do-icon.png"></a>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="pin7">
                          <a class="location1" data-toggle="popover" ><img src="img/map/location1.png"></a>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="pin8">
                          <a class="location1" data-toggle="popover" ><img src="img/map/location2.png"></a>
                          </div>
                        </div>
                      </div> -->

                      <!-- <img src="img/map/1a.jpg"> -->
                    </div>

                  </div>
                </div>

                <!-- Bottom -->


                <div class="col-md-3 map_left_padding">
                  <div class="map_right">
                    <div class="col-md-12 mr_top">
                      <p>YOU ARE PLANING TO TRAVEL 10/22/2018 - 10/22/2018</p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="mr_days">
                      <div class="col-md-3 day_1">
                        <div class="day_p1">
                          <p>Day 1</p>
                        </div>
                      </div>
                      <div class="col-md-3 day_2">
                        <div class="day_p">
                          <p>Day 2</p>
                        </div>
                      </div>
                      <div class="col-md-3 day_3">
                        <div class="day_p">
                          <p>Day 3</p>
                        </div>
                      </div>
                      <div class="col-md-3 day_4">
                        <div class="day_p">
                          <p>Day 4</p>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="coco">
                      <img src="merge_pages/img/map/3.jpg">

                    </div>

                     <div class="coco-1">
                      <img src="merge_pages/img/map/coco.png">

                    </div>


                    <div class="black_adds">
                      <div class="col-md-12 black_1">
                        <p>ADD A 2ND ACTIVITY TO THIS DAY IF DESIRED</p>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="black_adds">
                      <div class="col-md-12 black_1">
                        <p>ADD A 3RD ACTIVITY TO THIS DAY IF DESIRED</p>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="rating_offer">
                      <div class="col-md-4 ro_img">
                        <img src="merge_pages/img/map/img1.jpg">
                      </div>
                      <div class="col-md-8 ro_content">
                      <div class="ro_content">
                        <h4>Upto 40% Off on Food </h4>
                        <p> 
                          Modern hotel offering a restaurant, an outdoor pool many more..
                        </p>
                        <div class="ro_stars">
                          <div class="col-md-6 ul_star">
                          <ul>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                          </ul>
                          (12)
                          </div>
                          <div class="col-md-6 ro_offer">
                            <p><span>$12</span>&nbsp;&nbsp; $7</p>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>

                    <div class="rating_offer">
                      <div class="col-md-4 ro_img">
                        <img src="merge_pages/img/map/img2.jpg">
                      </div>
                      <div class="col-md-8 ro_content">
                      <div class="ro_content">
                        <h4>Upto 40% Off on Lunch </h4>
                        <p> 
                          Modern hotel offering a restaurant, an outdoor pool many more..
                        </p>
                        <div class="ro_stars">
                          <div class="col-md-6 ul_star">
                          <ul>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                          </ul>
                          (4,65)
                          </div>
                          <div class="col-md-6 ro_offer">
                            <p><span>$30</span>&nbsp;&nbsp; $18</p>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>

                    <div class="rating_offer">
                      <div class="col-md-4 ro_img">
                        <img src="merge_pages/img/map/img3.jpg">
                      </div>
                      <div class="col-md-8 ro_content">
                      <div class="ro_content">
                        <h4>Upto 32% Off on Seafood </h4>
                        <p> 
                          Modern hotel offering a restaurant, an outdoor pool many more..
                        </p>
                        <div class="ro_stars">
                          <div class="col-md-6 ul_star">
                          <ul>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                          </ul>
                          (132)
                          </div>
                          <div class="col-md-6 ro_offer">
                            <p><span>$38</span>&nbsp;&nbsp; $26</p>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>

                    <div class="rating_offer">
                      <div class="col-md-4 ro_img">
                        <img src="merge_pages/img/map/img4.jpg">
                      </div>
                      <div class="col-md-8 ro_content">
                      <div class="ro_content">
                        <h4>Upto 31% Off Howl </h4>
                        <p> 
                          Modern hotel offering a restaurant, an outdoor pool many more..
                        </p>
                        <div class="ro_stars">
                          <div class="col-md-6 ul_star">
                          <ul>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                            <li>
                              <i class="fa fa-star"></i>    
                            </li>
                          </ul>
                          (125)
                          </div>
                          <div class="col-md-6 ro_offer">
                            <p><span>$50</span>&nbsp;&nbsp; $35</p>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>

                    <div class="airfair">
                      <div class="col-md-12 airfair_header">
                        <p>Airfare & Accommodation Paid Separately</p>
                      </div>
                      <div class="clearfix"></div>
                      <div class="airfair_one">
                        <div class="col-md-4">
                          <p>Day 1</p>
                        </div>
                        <div class="col-md-3">
                          <p>$200</p>
                        </div>
                        <div class="col-md-5 air_red">
                          <p><span>$400</span>&nbsp;&nbsp;50% OFF</p>
                        </div>
                      </div>

                      <div class="airfair_one a_two">
                        <div class="col-md-4">
                          <p>Day 2</p>
                        </div>
                        <div class="col-md-3">
                          <p>$150</p>
                        </div>
                        <div class="col-md-5 air_red">
                          <p><span>$300</span>&nbsp; &nbsp;50% OFF</p>
                        </div>
                      </div>

                      <div class="airfair_one">
                        <div class="col-md-4">
                          <p>Day 3</p>
                        </div>
                        <div class="col-md-3">
                          <p>$100</p>
                        </div>
                        <div class="col-md-5 air_red">
                          <p><span>$200</span>&nbsp;&nbsp;50% OFF</p>
                        </div>
                      </div>

                      <div class="airfair_one a_two">
                        <div class="col-md-4">
                          <p>Day 4</p>
                        </div>
                        <div class="col-md-3">
                          <p>$250</p>
                        </div>
                        <div class="col-md-5 air_red">
                          <p><span>$500</span>&nbsp; &nbsp;50% OFF</p>
                        </div>
                      </div>

                      <div class="airfair_one total">
                        <div class="col-md-4">
                          <p>Total</p>
                        </div>
                        <div class="col-md-3">
                          <p>$700</p>
                        </div>
                        <!-- <div class="col-md-5">
                          <p><span>$400</span>&nbsp;50% OFF</p>
                        </div> -->
                      </div>

                    </div>

                  </div>
                </div>


                <div class="sections">
                  <div class="col-md-9 sec">
                    <div class="col-md-6 section_deals">
                      <img src="merge_pages/img/map/s1.jpg">
                    </div>
                    <div class="col-md-6 section_deals sd_right">
                      <img src="merge_pages/img/map/s2.jpg">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal fade treasures-around-me" id="myModal001" role="dialog">
                          <div class="modal-dialog map_dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="">
                                  <div class="col-md-12">
                                      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                  </div>
                              </div>
                              <div class="modal-body map_modal_body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <h5>Food & Drinks</h5>
                                        <input type="checkbox" name=" " value=" ">Dinner<br>
                                        <input type="checkbox" name=" " value=" ">Lunch<br>
                                        <input type="checkbox" name=" " value=" ">Fast Food<br>
                                        <input type="checkbox" name=" " value=" ">Breakfast<br>
                                        <input type="checkbox" name=" " value=" ">Cafe Dessert Bakes<br>
                                        <input type="checkbox" name=" " value=" ">Club & Bars<br>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>Things To Do</h5>
                                        <input type="checkbox" name=" " value=" ">Adventure<br>
                                        <input type="checkbox" name=" " value=" ">Kid Activities<br>
                                        <input type="checkbox" name=" " value=" ">Sporting Events<br>
                                        <input type="checkbox" name=" " value=" ">Education<br>
                                        <input type="checkbox" name=" " value=" ">Design<br>
                                        <input type="checkbox" name=" " value=" ">Training & Development<br>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>Local Services</h5>
                                        <input type="checkbox" name=" " value=" ">Fasion<br>
                                        <input type="checkbox" name=" " value=" ">Home & Appliances<br>
                                        <input type="checkbox" name=" " value=" ">Kid Stores<br>
                                        <input type="checkbox" name=" " value=" ">Shopping<br>
                                        <input type="checkbox" name=" " value=" ">Acountancy Services<br>
                                        <input type="checkbox" name=" " value=" ">Photo Studio<br>
                                    </div>
                                    <div class="col-md-2">
                                        <h5>Travel</h5>
                                        <input type="checkbox" name=" " value=" ">Reservation<br>
                                        <input type="checkbox" name=" " value=" ">Rent a Car<br>
                                        <input type="checkbox" name=" " value=" ">Outdoor<br>
                                        <input type="checkbox" name=" " value=" ">Tourism<br>
                                        
                                    </div>
                                </div>
                              </div>
                              <div class="modal-footer map_modal_footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #3479b4;    color: white;">Close</button>
                              </div>
                            </div>
                            
                          </div>
                        </div>

          </div>


          <!-- fourth -->

          <div class="clearfix"></div>

          <div class="fourth_page">
            <div class="hiring">
      <div class="hpre">
        <div class="container-fluid"> 
          <div class="h-header">
            <center><h3>
              THE 4 PILLARS OF HOW TO MAKE MONEY WITH TJ
            </h3></center>
          </div>
        </div>
      </div>

      <!--  -->
      

<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-6 col-sm-6 ">
          <div class="service1">
            <div class="hcircle">
                <p>1</p>
            </div>
            <div class="service-box">
                <div class="service-icon yellow">
                    <div class="front-content">
                        <!-- <i class="fa fa-trophy"></i> -->
                        <h3>Download our TJ Prime Gift Card App and send Complimentary $249 Gift Cards to every contact you have. </h3>
                    </div>
                </div>
                <div class="service-content">
                    <h5>You get paid $50.00 for every new full membership!</h5>
                    <!-- <button>Read More</button> -->
                </div>
            </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 ">
          <div class="service1">
            <div class="hcircle">
                <p>2</p>
            </div>
            <div class="service-box">
                <div class="service-icon orange">
                    <div class="front-content">
                        <!-- <i class="fa fa-anchor"></i> -->
                        <h3>You Get paid to build your TJ <img src="merge_pages/img/coming_soon/map2.png"> Partner Network</h3>
                        <p>Keep an eye on TJ Treasure Maps and Harvest revenue collectingQuality Red Pin Border Businesses you know onto your TJ Pin Partner Network.</p>
                        <p>
                        Simply Nudge QualityRed Pin Border Restaurants, Pubs/Bars, Activities&Businesses you see and know on our TJ Treasure Maps.</p>
                        <p>By Nudging Red Pin Border Businesses, you know, you Incentivize & motivate them to become TJ <img src="merge_pages/img/coming_soon/map2.png">  Partner.</p>
                        <p>We Freely / Incentivize, businesses on your behalf with 5 Gift Cards = $1996.00 that we pay for and we do it with “No Set Up Fees”!

                      </p>
                    </div>
                </div>
                <div class="service-content">
                    
                    <p>Please only select businesses that have in your past, provided you great service. Once they join your network we financially reward them & you greatly, for all the time & efforts!</p>
                    <p>
                     The TJ Web Portal Team & The Entire TJ Membership Network all work together to promote your TJ <img src="merge_pages/img/coming_soon/map2.png"> Partner Network.</p>
                    <p>Every time your TJ <img src="merge_pages/img/coming_soon/map2.png"> Partners transact business you get paid!</p>
                    <p>Businesses are incentivized to become TJ <img src="merge_pages/img/coming_soon/map2.png">  Partnersby providing Deal Discounts of up to 90% off goods and services for both the citizens within the communities they serve and visiting tourist/ travelers as well! </p>
                    <p>Businesses profit alongside TJ Members by becoming TJ <img src="merge_pages/img/coming_soon/map2.png">  Partners!</p>
                    <p>What is Nudging? <a href="" data-toggle="modal" data-target="#pillar2" style="color: #ff7e44;outline: none;">Click Here</a></p>
                    </p>
                </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="service1">
            <div class="hcircle">
                <p>3</p>
            </div>
            <div class="service-box ">
                <div class="service-icon red">
                    <div class="front-content">
                        <!-- <i class="fa fa-trophy"></i> -->
                        <h3>Get Paid to Create Brilliant Itineraries with built in Treasure Mapsusing our Power Engine Planning Tool!</h3>
                        <p>Easily build-in Treasure Maps with Hidden Gems of amazing Restaurants, Pubs/ Clubs, Things to Do, Places to See.</p>
                    </div>
                </div>
                <div class="service-content">
                  <div class="third_content">
                    <p>You get paid from each deal listed-on, the Treasure Maps you create within the Itineraries!</p>
                    <p>
                    Add Green TJ Pins / Border Businesses into itineraries you create. </p>
                    <p>Green TJ Pins are TJ <img src="merge_pages/img/coming_soon/map2.png"> Partners with offeringDeals of up to 90% off. </p>
                    <p>These Little Green Pins / Treasures Pay you, when you add them to TJ Itineraries you create with the TJ Trip Planner. </p>
                    <p>Tourist & Locals use TJ Members Pre-Made Itineraries with built in Treasure Maps you create.They save money purchasing the Hidden Gems you suggest, they save time & enjoy themselves more every day! 
                    </p>
                  </div>
                </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="service1">
            <div class="hcircle">
                <p>4</p>
            </div>
            <div class="service-box">
                <div class="service-icon grey">
                    <div class="front-content">
                        <!-- <i class="fa fa-paper-plane-o"></i> -->
                        <h3>Fully Licensed Travel Jolly Members Earn Rewards!</h3>
                    </div>
                </div>
                <div class="service-content">
                  <div class="fourth_content">
                    <p>The TJ Rewards Program is really, really, really good and can easily match the average person full time income without needing to be employed at all! <br><a href="" style="color: #ff7e44;outline: none;" data-toggle="modal" data-target="#pillar4">Learn More…</a></p>
                    </div>
                    <!-- <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure</p> -->
                </div>
            </div>
          </div>
        </div>
    </div>
</div>

      <!--  -->

      <div class="h-body">
        <!-- <div class="container-fluid">
          <div class="col-md-6 hcard">
            <div class="h-card">
              
              <div class="hcircle">
                <p>1</p>
              </div>
              <p>Download our gift card app and send gift cards to every contact you have. You get paid $50.00 for every new full membershio!</p>
            </div>
          </div>
           <div class="col-md-6 htreasure">
            <div class="h-card h-treasure">
              <div class="hcircle">
                <p>2</p>
              </div>
              <p>Using our treasure Maps, nudge the business you know highleted in red to create a deaj for TJ members and collect a commision on every sale.!</p>
            </div>
          </div>
          <div class="col-md-6 htreasure">
            <div class="h-card h-saving">
             
              <div class="hcircle">
                <p>3</p>
              </div>
              <p>Create Travel Jolly treasure Maps, when you our members use them they save time. Enjoy themsalves more and you get paid from each deal listed on the treasure Map you create.</p>
            </div>
          </div>
           <div class="col-md-6 htreasure">
            <div class="h-card h-security">
              <div class="hcircle">
                <p>4</p>
              </div>
              <p>Fully licensed Travel jolly Club members also earn rewards program is really good and can easily match the avarge person full time income without being employed at all!</p>
            </div>
          </div>
          </div> -->
          
          <div class="clearfix"></div>

          <div class="we_hiring">
            <div class="container-fluid">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="hiring1">
                  <img src="merge_pages/img/coming_soon/home1.jpg">
                </div>
                <div class="hiring1 hiring3">
                  <img src="merge_pages/img/coming_soon/learning.jpg">
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="hiring2">
                  <img src="merge_pages/img/coming_soon/hiring.jpg">
                </div>
              </div> 
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="hiring1 hiring_1">
                  <img src="merge_pages/img/coming_soon/travel.png">
                </div>
                <div class="hiring1">
                  <img src="merge_pages/img/coming_soon/responsive.png">
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="hiring-bottom">
            <div class="container-fluid">
              <h3>Positions we are hiring for</h3>

              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Marketing Blogger</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Freelance Bloggers</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Blogger & Social Media Content Writer</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Senior Publisher</p>
                  </div>
                </div>
              </div>

               <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Publicity Manager</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Sr. Manager, User Acquisition</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>District: Sales Manager</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Regional Sales Manager</p>
                  </div>
                </div>
              </div>

              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Marketing Business Development Manager</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Outside Business Development Representative</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>In House Sales Operations Manager</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Project Sales Leader</p>
                  </div>
                </div>
              </div>

               <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Convention Sales Leader</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Freelance Project Manager</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Food and Lifestyle Publicist</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="hb-1">
                  <div class="hb1-top">
                    <h3>Help<br>
                      Wanted
                    </h3>
                  </div>
                  <div class="hb1-bottom">
                    <p>Digital Marketing Strategist</p>
                  </div>
                </div>
              </div>

            </div>
          </div>


      </div>  
      
    
    
      <div class="footer">
        <div class="container-fluid">
          <center><p>Travel jolly | All Right Reserved</p></center>
        </div>
      </div>
  </div>
    
  <!-- Modal -->
<div id="pillar2" class="modal fade" role="dialog">
  <div class="modal-dialog pillars">

    <!-- Modal content-->
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div> -->
      <div class="modal-body">
        <ul>
          <li>
            <p>Nudging is clicking a button on the Businesses TJMap Profile Page.</p>    
          </li>
        <li>
          <p> Nudging sends thebusiness email address on file,a signup form, after being completed and TJ <img src="merge_pages/img/coming_soon/map2.png">accepts the forms and allows the business to become a TJ <img src="merge_pages/img/coming_soon/map2.png">  Partner. The business then receives 5 Gift Cards = $1996.00 that we issue and pay for!</p>
          
        </li>
        <li>
          <p>Freely Nudge quality businesses you know with overwhelming incentives and when the business becomes a TJ <img src="merge_pages/img/coming_soon/map2.png"> Partner you have claimed that Red Pin Border Business as part of your TJ <img src="merge_pages/img/coming_soon/map2.png">  Partner Network! </p>  
        </li>
        <li>
          <p>We incentivize the businessto make growing your TJ <img src="merge_pages/img/coming_soon/map2.png">  Partner Network easy!
        o (Profile pages provide a phone number to call the business& usually also provide links to their Facebook / Twitter & Social Sites. Feel free to call them if you know them and or post to their social sites to help Nudge them to action.)</p>  
        </li>
        <li>
          <p>When TJ accepts the business as a TJ <img src="merge_pages/img/coming_soon/map2.png">  Partner the business will be issued the Activation Codes for the Gift Cards You Nudged them with. 
        </p>  
        </li>
        
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- modal 2 -->

<div id="pillar4" class="modal fade" role="dialog">
  <div class="modal-dialog pillars">

    <!-- Modal content-->
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div> -->
      <div class="modal-body fourth_p">
        <p>
          Fully Licensed Travel Jolly Membership Includes: 1 Platinum Club Membership License, up to 9 Sub-License Riders and the TJ Rewards Program. </p>
          <p>While supplies last each Fully Licensed Travel Jolly Membership will have no additional fee for additional Sub License Riders or the TJ Rewards Program.</p>
          <p>
            1 Licensee can easily add up to 9 Additional Sublicense Riders = 10 Total Membership Users Per License.<br>
            <ul class="fourth_pillar">
              <li>
               All 10 Members can issue TJ Guest Passes, providing anyone they know wanting to travel up to 65% off over 400,000 destinations worldwide.  
              </li>
              <li>
                Guest Pass users save up to 65% off this amount generally equals hundreds of dollars in savings over the cost of competing booking engine pricing. (OTA Logo’s Here)
              </li>
            </ul>
          </p>
          <p><b>Example:</b> 1 Guest Pass User Per Month Averaging only $250 Savings equals 20,000 Reward Points that has a direct dollar conversion valued at $250 to the original Full Member Licensee that can be immediately converted into gift cards and or travel anywhere within the Full Membership Portal of TJ.<br>
          If altogether monthly those complimentary Guest Passes only generate 1 Traveler that saves just $250 and this happens 1X per month that equals 240,000 Rewards at the end of 12 Months valued at $3,000 worth of gift cards and or travel that can be used by the original Licensee anywhere within the full membership portal of TJ.
          </p>
          <p>
           <b> Example 2:</b> 1 Guest Pass Used Monthly = 20,000 Rewards x 12 Months = 240,000 Rewards = Direct Dollar Conversion Value of $3,000 ($6,000 the end of 2nd year and $15k by the end of the 5th year.)5-30 times the current investment of the membership.
          </p>
          <p>
            <b>Example 3:</b> Using the above example If 2 Guest Passes in total are utilized and save the Guest Pass Users $250 each that is 480,000 Rewards valued at $500 total. 
          </p>
          <p>
            The original Fully Licensed Travel Jolly Membership Licensee would receive 480,000 Reward Points valued at $500 that can be immediately converted to gift cards and or travel that can be used anywhere within the Fully Licensed Travel Jolly Membership Portal of TJ. 
          </p>
          <p>
            If altogether monthly those complimentary Guest Passes only generate 1 Traveler that saves just $250 and this happens 1X per month, that equals 240,000 Rewards at the end of 12 Months that equals $3,000 worth of gift cards and or travel that can be used by the original Licensee anywhere within the Fully Licensed Travel Jolly Membership portal of TJ.
          </p>
          <p>
            If altogether weekly those complimentary Guest Passes only generate 1 Traveler that saves just $250 and this happens 1X per week that equals 1,040,000 Rewards at the end of 52 weeks (1 Year) valued at $13,000 worth of gift cards and or travel that can be used by the original Licensee anywhere within the Fully Licensed Travel Jolly Membership portal of TJ.
          </p>
          <p>
            If altogether weekly those complimentary Guest Passes only generate 1 Traveler that saves just $250 and this happens every day for 1 year that equals 7,300,000 Rewards at the end of 365 days (1 Year) valued at $91,250 worth of gift cards and or travel that can be used by the original Licensee anywhere within the Fully Licensed Travel Jolly Membership portal of TJ.
          </p>
          <p>
            What if you double, triple, quadruple or quintuple these numbers? Do you have a marketing plan or need one? Reach out to us and let’s talk! We make dreams a reality! Let the world be your oyster!
          </p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    </div>

    <!-- fifth page -->

    <div class="clearfix"></div>
    <div class="fifth_page">
      

      <div class="founding_partners">
      <div class="fp_pre">
        <div class="container-fluid"> 
          <div class="fp_header">
            <div class="col-md-2">
              <div class="fp_header_img">
                <img src="merge_pages/img/coming_soon/groupon.png">
              </div>
            </div>
            <div class="col-md-8">
               <div class="fp_h">
                  <h3 class="hleft">We pay</h3>
                       <h3><span>Founding <img src="merge_pages/img/coming_soon/location.png"> Partners</span></h3>
                      <h3 class="hright">upfront !</h3>
               </div>
            </div>
            <div class="col-md-2">
              <div class="fp_header_img">
                <img src="merge_pages/img/coming_soon/business-1.png">
              </div>
            </div>


           
          </div>
          <div class="clearfix"></div>
          <div class="fp_add">
            <h3>TJ Promotes your business for you!</h3>
            <h4>No upfront fees to get started!</h4>
          </div>
        </div>
      </div>

      <div class="fp_body">
        <div class="container-fluid">
          <div class="col-md-3 col-sm-3">
            <div class="fp_card">
              <img src="merge_pages/img/coming_soon/card1.png">
            </div>
          </div>
          <div class="col-md-9 col-sm-9">
            <div class="fp_text">
              

              <h3>
               <a>Sign up NOW</a> and become a <span class="fp_top">Founding <img src="merge_pages/img/coming_soon/map1.png"> Partner </span>on <span  class="fp_tj">TravelJolly.com</span></h3>
                <br>
                <h3 class="auto">You automatically receive:</h3>
                <h4 class="gc">
                 A $1,000 Booking Engine Gift Card!<br>
                 A Golden VIP Invitation to the Grand Opening Startup Event of The Year!<br>
               <!--   <h4 class="vip"> --><span>*</span>VIP Invitations = Guest + 1</h4>
                <br>
                 <h4 class="co_work">As a <span class="co_work">Founding  <img src="merge_pages/img/coming_soon/map1.png"> Partner</span> we want to go the extra mile and celebrate our new working relationship together.</h4><br>
                 <h4 class="co_work1">
                   To kick off celebrations TJ is investing into your business by freely providing Incentive's to not just an owner or Manager but to those Co-Workers whom constantly interact with your customers. We want to support your needs to motivate and inspire them. Use these 4 additional VIP Invitations & Gift Cards to kick things off and if you need more just reach out to us with a plan and we will do more. Maybe a lot more! How can we help?

                 </h4>
              
              <div class="fp_t">
                <div class="col-md-12 col-sm-12 fp_tsmal">
                  <div class="fp_texth">
                    <ul>
                      <li>
                         You also Receive Four Additional Complimentary $249 Booking Engine Gift Cards to provide employees, each with their own VIP Invitations to our Grand Opening Startup Event! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red" style="background: none">*</span><span class="ul_span" style="background: none">VIP Invitations=Guest + 1</span>
                      </li>
                      <li>
                        Extremely low <span class="ul_text">Founding  <img src="merge_pages/img/coming_soon/map1.png"> Partner</span> Deal Fee’s!
                      </li>
                      <li>
                        50% - 50% Payout at 10 & 30 days!
                      </li>
                      <li> 
                        All the best features, reports, and rules you would expect from the best!
                      </li>
                    </ul>
                  
                    
                  </div>
                </div>
                <!-- <div class="col-md-3 col-sm-3 groupon">
                  <div class="col-md-12 col-sm-12 col-xs-12"> 
                    
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12"> 
                    <img src="img/coming_soon/business-1.png">
                  </div>
                </div> -->
              </div>
              <div class="clearfix"></div>
              <!-- <div class="fp_tbottom">
                <h3><b>lncentivize</b> your Co-Workers <span>Founding <img src="img/coming_soon/map1.png"> Partner </span>on TravelJolly.com</h3>
              </div> -->
              <div class="clearfix"></div>
              <div class="fp_bottom_img">
                <div class="col-md-3 col-sm-3 col-xs-6">
                  <img src="merge_pages/img/coming_soon/card.png">
                </div>
                  <img src="merge_pages/img/coming_soon/card.png">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                  <img src="merge_pages/img/coming_soon/card.png">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                  <img src="merge_pages/img/coming_soon/card.png">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
             <div class="sign_up">
                <div class="col-md-4 su_img">
                  <a href="" title="signup now"><img src="merge_pages/img/coming_soon/signup.png"></a>
                </div>
                <div class="col-md-8">
                  <h4>The offer is being Sponsored by <span class="sgreen">TravelJolly.com</span> and our associated<br> <span class="red" style="background: none">Grand Opening Celebration </span>Startup Event Partners. 
                  </h4>
                  <h3>This will not be available going forward!</h3>
                </div>
              </div>
              <div class="sign_up_bottom">
                <img src="merge_pages/img/coming_soon/signupbg.png">
              </div>
          </div>
 

        </div>
       

      </div>

        
      
    
    
      <div class="footer">
        <div class="container-fluid">
          <center><p>Travel jolly | All Right Reserved</p></center>
        </div>
      </div>
  </div>

    </div>


    
      <script type="text/javascript" src="merge_pages/js/jquery-3.3.1.min.js"></script>
      <script type="text/javascript" src="merge_pages/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="merge_pages/js/jquery.js"></script>
       <script type="text/javascript" src="merge_pages/jquery.sticky.js"></script>
       <script>
      // $(document).ready(function(){
      //     $('[data-toggle="tooltip"]').tooltip();   
      // });

      $(document).ready(function(){
        $('.location1').tooltip('show');
          $('[data-toggle="popover"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="merge_pages/img/map/green-banner.jpg" class="media-object" alt="Sample Image" class="img-responsive"></a></div>'
          });
          $('[data-toggle="popover2"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="merge_pages/img/map/red-banner.jpg" class="media-object" alt="Sample Image"></a></div>'
          });
          $('[data-toggle="popover3"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="merge_pages/img/map/light-green-banner.jpg" class="media-object" alt="Sample Image" ></a></div>'
          });
          $('[data-toggle="popover4"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="merge_pages/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });
          $('[data-toggle="popover5"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="merge_pages/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });
          $('[data-toggle="popover6"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="merge_pages/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });
          $('[data-toggle="popover7"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="merge_pages/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });
          $('[data-toggle="popover8"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="merge_pages/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });
          $('[data-toggle="popover9"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="merge_pages/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });

      });
      
      $('button').click(function(e){
             $('#myDiv').toggleClass('fullscreen'); 
      });
      </script>
    <!-- Start of REVE Chat Script-->
    <script type='text/javascript'>
    window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account='9571569';w.__revechat_version=2;
      r._= []; var rc = d.createElement('script'); rc.type = 'text/javascript'; rc.async = true; rc.setAttribute('charset', 'utf-8');
      rc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.revechat.com/widget/scripts/new-livechat.js?'+new Date().getTime();
      var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(rc, s);
    })(document, window);
    </script>
    <!-- End of REVE Chat Script -->
    </body>
    </html>