<?php
include('admin.php');

$obj=new Admin();


 $username = $_SESSION['username'];

 // echo $username;
 // exit();

if(!$username)
{
   echo '<script type="text/javascript">window.location ="login.php";</script>';
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Landing Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link rel="stylesheet" type="text/css" href="map/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="map/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="map/css/style-1.css">
    <link rel="stylesheet" type="text/css" href="map/css/responsive.css">
     <link rel="stylesheet" type="text/css" href="map/css/coming_soon.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" type="images/x-icon" href="map/img/favicon.png">
    <script type="text/javascript" src="map/jquery.sticky.js"></script>
    <style type="text/css">
      @media(min-width: 1600px){
      .media{
        padding-right: 0px!important;  
        margin: 0px !important;
      }
      .popover {
      padding-right: 0px!important;
      width: 400px!important;
      margin: 0px !important;
      text-align: center;
        }
      }
    </style>
</head>
  <body>
    
    <div class="map">
      
      <!-- top -->
    <div class="container-fluid">
      <div class="col-md-3 map_left_padding">
        <div class="map_left">
          <div class="col-md-6 mlb">
            <p>Basic Map</p>
          </div>
          <div class="col-md-6 mlb tre">
            <p>Treasure Map</p>
          </div>
          <div class="clearfix"></div>
          <div class="google_search">
            <img src="map/img/map/2.jpg">
          </div>
          <div class="clearfix"></div>

          <div class="map_deals">
            <div class="md_header">
              <i class="fa fa-map-marker" aria-hidden="true"></i>
              <p>Featured Places in Orlando</p>
            </div>
            <div class="district">
              <div class="d_top">
                <p><b>District</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Maingate West</a><p>&nbsp; 189 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Winter Park</a><p>&nbsp;20 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Maingate East</a><p>&nbsp; 19 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">North Orlando</a><p>&nbsp;120 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">West Orlando</a><p>&nbsp; 70 Hotels</p>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="district_margin"></div>
            <div class="district">
              <div class="d_top">
                <p><b>Neighbourhoods</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Kissimmee</a><p>&nbsp; 253 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Orlando CBD</a><p>&nbsp;1381 Hotels</p>
                </div>
                
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="district_margin"></div>

            <div class="district">
              <div class="d_top">
                <p><b>Suburbs</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Maingate West</a><p>&nbsp; 189 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Winter Park</a><p>&nbsp;20 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Maingate East</a><p>&nbsp; 19 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">North Orlando</a><p>&nbsp;120 Hotels</p>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="airoplan">
              <div class="md_header">
              <i class="fa fa-plane" aria-hidden="true"></i>
              <p>Airports</p>
            </div>
            <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Maingate East</a><p>&nbsp; 19 MCO</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">North Orlando</a><p>&nbsp;120 SFB</p>
                </div>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Maingate East</a><p>&nbsp; 19 MLB</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">North Orlando</a><p>&nbsp;120 ORL</p>
                </div>
              </div>
            </div>


            <div class="clearfix"></div>
            <div class="airoplan">
              <div class="md_header">
              <i class="fa fa-home" aria-hidden="true"></i>
              <p>Places Of Interests</p>
            </div>

            <div class="district">
              <div class="d_top">
                <p><b>Tourist Regions</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Maingate West</a><p>&nbsp; 189 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Winter Park</a><p>&nbsp;20 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Maingate East</a><p>&nbsp; 19 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">North Orlando</a><p>&nbsp;120 Hotels</p>
                </div>
                
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">West Orlando</a><p>&nbsp; 70 Hotels</p>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="district_margin"></div>
            <div class="district">
              <div class="d_top">
                <p><b>Business</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Kissimmee</a><p>&nbsp; 253 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Orlando CBD</a><p>&nbsp;1381 Hotels</p>
                </div>
                
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="district_margin"></div>

            <div class="district">
              <div class="d_top">
                <p><b>Entertainment</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="district_margin"></div>

            <div class="district">
              <div class="d_top">
                <p><b>Institutions</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
              </div>
            </div>


            <div class="clearfix"></div>
            <div class="district_margin"></div>

            <div class="district">
              <div class="d_top">
                <p><b>Shopping</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="district_margin"></div>

            <div class="district">
              <div class="d_top">
                <p><b>Sights</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="district_margin"></div>

            <div class="district">
              <div class="d_top">
                <p><b>Things To Do</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
              </div>
               <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
              </div>
            </div>
            </div>
            <div class="clearfix"></div>

            <div class="airoplan">
              <div class="md_header">
              <i class="fa fa-map-marker" aria-hidden="true"></i>
              <p>Places Nearby Orlando</p>
            </div>
            <div class="district">
              <div class="d_top">
                <p><b>Cities</b></p>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
              </div>
               <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">Downtown Orlando</a><p>&nbsp; 80 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Kissimmee</a><p>&nbsp;1381 Hotels</p>
                </div>
              </div>
              <div class="d_bottom">
                <div class="col-md-6 db1">
                  <a href="">International Drive</a><p>&nbsp; 631 Hotels</p>
                </div>
                <div class="col-md-6 db2">
                  <a href="">Celebration</a><p>&nbsp;20 Hotels</p>
                </div>
              </div>
            </div>

            </div>
          </div>

        </div>
      </div>
      
      <!-- Middle -->

      <div class="col-md-6">
        <div class="map_middle">
          <div class="col-md-2 mm">
            <p>4 DAYS</p>
          </div>
          <div class="col-md-5 mm1">
            <div class="mm1a">
              <P>OF CENTRAL FLORIDA MAGIC ON BUDGET!</P>
            </div>
          </div>
          <div class="col-md-5 mm2">
            <P>FLIGHTS & HOTELS PAID FOR SEPERATELY </P>
          </div>
          <div class="clearfix"></div>
          <div class="sort">
            <div class="sort_below">
            <!-- <img src=" img/map/1.jpg"> -->
            <h4>CLICK THE LINKS BELOW TO ADJUST THE TYPE OF MAP PINS DSIPLAYED</h4>
            <ul>
              <li>
                Show All
              </li>
              <li>
                Retaurants 
              </li>
              <li>
                Things To Do
              </li>
              <li>
                Shopping 
              </li>
              <li>
                <a href=" ">Theme Park </a>
              </li>
              <li class="no_border">
                Bars & Nightlife
              </li>
            </ul>
            <div class="sort_bottom">
              <button type="button" data-toggle="modal" data-target="#myModal">Search</button>
              <div class="sort_more">
                  <p>More</p>
                  <span class="caret"></span>
              </div>
              <div class="sort_more sort_more1">
                  <p>Sort By</p>
                  <span class="caret"></span>
              </div>
              <div class="full_screen">
                <img src="map/img/map/full_screen.jpg">
              </div>
            </div>
          </div>
          <div class="sort_img">
            <center><h2>FAT SAVINGS</h2></center>
            <center><h3>SLIM PRICES</h3></center>
          </div>
          </div>

          <div class="clearfix"></div>
          <div class="google_map">
            <div class="fist_row_location">
            <div class="col-md-2">
              <div class="pin1">
              <a class="location1" data-toggle="popover2"><img src="map/img/map/icons/11.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin2">
              <a class="location1" data-toggle="popover" ><img src="map/img/map/icons/7.png"></a>
              </div>
            </div>
            
            <div class="col-md-2">
              <div class="pin3">
              <a class="location1" data-toggle="popover3"><img src="map/img/map/icons/3.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin4">
              <a class="location1" data-toggle="popover"><img src="map/img/map/icons/10.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin5">
              <a class="location1" data-toggle="popover3" ><img src="map/img/map/icons/18.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin6">
              <a class="location1" data-toggle="popover2"><img src="map/img/map/icons/14.png"></a>
              </div>
            </div>
            </div>
            <div class="clearfix"></div>
            

            <!-- second row -->



            <div class="second_row_location">
              <div class="col-md-2">
              <div class="pin7">
              <a class="location1" data-toggle="popover" ><img src="map/img/map/icons/1.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin8">
              <a class="location1"><img src="map/img/map/icons/15.png"></a>
              </div>
            </div>
            
            <div class="col-md-2">
              <div class="pin9">
              <a class="location1"><img src="map/img/map/icons/17.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin10">
              <a class="location1"><img src="map/img/map/icons/9.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin11">
              <a class="location1" data-toggle="popover" ><img src="map/img/map/icons/7.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin12">
              <a class="location1" ><img src="map/img/map/icons/2.png"></a>
              </div>
            </div>
            </div>

            <!-- third row -->
            <div class="clearfix"></div>

            <div class="second_row_location">
              <div class="col-md-2">
              <div class="pin13">
              <a class="location1" data-toggle="popover2" ><img src="map/img/map/icons/14.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin14">
              <a class="location1"><img src="map/img/map/icons/18.png"></a>
              </div>
            </div>
            
            <div class="col-md-2">
              <div class="pin15">
              <a class="location1" data-toggle="popover"><img src="map/img/map/icons/4.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin16">
              <a class="location1"><img src="map/img/map/icons/5.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin17">
              <a class="location1" ><img src="map/img/map/icons/17.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin18">
              <a class="location1"><img src="map/img/map/icons/1.png"></a>
              </div>
            </div>
            </div>


            <!-- fourth row -->

             <!-- third row -->
            <div class="clearfix"></div>

            <div class="second_row_location">
              <div class="col-md-4">
              <div class="pin19">
              <a class="location1" data-toggle="popover" ><img src="map/img/map/icons/16.png"></a>
              </div>
            </div>
            <!-- <div class="col-md-2">
              <div class="pin14">
              <a class="location1" data-toggle="popover" ><img src="img/map/icons/15.png"></a>
              </div>
            </div> -->
            
            <div class="col-md-2">
              <div class="pin20">
              <a class="location1" data-toggle="popover2"><img src="map/img/map/icons/14.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin21">
              <a class="location1"><img src="map/img/map/icons/1.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin22">
              <a class="location1" ><img src="map/img/map/icons/9.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin23">
              <a class="location1"><img src="map/img/map/icons/11.png"></a>
              </div>
            </div>
            </div>

            <!-- fifth row -->

             <!-- third row -->
            <div class="clearfix"></div>

            <div class="fifth_row_location">
              <!-- <div class="col-md-2">
              <div class="pin13">
              <a class="location1" data-toggle="popover" ><img src="img/map/icons/2.png"></a>
              </div>
            </div> -->
            <div class="col-md-4">
              <div class="pin24">
              <a class="location1" data-toggle="popover3" ><img src="map/img/map/icons/6.png"></a>
              </div>
            </div>
            
            <div class="col-md-2">
              <div class="pin25">
              <a class="location1" data-toggle="popover2"><img src="map/img/map/icons/11.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin26">
              <a class="location1"><img src="map/img/map/icons/14.png"></a>
              </div>
            </div>
            <div class="col-md-2">
              <div class="pin27">
              <a class="location1"><img src="map/img/map/icons/11.png"></a>
              </div>
            </div>
            
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <div class="pin28">
              <center><a class="location1" data-toggle="popover" ><img src="map/img/map/icons/13.png"></a></center>
              </div>
            </div>

            <!-- <div class="third_row_location">
               <div class="col-md-6">
                <div class="pin5">
                <a class="location1" data-toggle="popover" ><img src="img/map/location2.png"></a>
                </div>
              </div>
              <div class="col-md-6">
                <div class="pin4">
                <a class="location1" data-toggle="popover" ><img src="img/map/shopping.png"></a>
                </div>
              </div>
            </div> -->

            <div class="clearfix"></div>
            <!-- <div class="fourth_row_location">
              <div class="col-md-4">
                <div class="pin6">
                <a class="location1" data-toggle="popover" ><img src="img/map/things-to-do-icon.png"></a>
                </div>
              </div>
              <div class="col-md-4">
                <div class="pin7">
                <a class="location1" data-toggle="popover" ><img src="img/map/location1.png"></a>
                </div>
              </div>
              <div class="col-md-4">
                <div class="pin8">
                <a class="location1" data-toggle="popover" ><img src="img/map/location2.png"></a>
                </div>
              </div>
            </div> -->

            <!-- <img src="img/map/1a.jpg"> -->
          </div>

        </div>
      </div>

      <!-- Bottom -->


      <div class="col-md-3 map_left_padding">
        <div class="map_right">
          <div class="col-md-12 mr_top">
            <p>YOU ARE PLANING TO TRAVEL 10/22/2018 - 10/22/2018</p>
          </div>
          <div class="clearfix"></div>
          <div class="mr_days">
            <div class="col-md-3 day_1">
              <div class="day_p1">
                <p>Day 1</p>
              </div>
            </div>
            <div class="col-md-3 day_2">
              <div class="day_p">
                <p>Day 2</p>
              </div>
            </div>
            <div class="col-md-3 day_3">
              <div class="day_p">
                <p>Day 3</p>
              </div>
            </div>
            <div class="col-md-3 day_4">
              <div class="day_p">
                <p>Day 4</p>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="coco">
            <img src="map/img/map/3.jpg">

          </div>

           <div class="coco-1">
            <img src="map/img/map/coco.png">

          </div>


          <div class="black_adds">
            <div class="col-md-12 black_1">
              <p>ADD A 2ND ACTIVITY TO THIS DAY IF DESIRED</p>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="black_adds">
            <div class="col-md-12 black_1">
              <p>ADD A 3RD ACTIVITY TO THIS DAY IF DESIRED</p>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="rating_offer">
            <div class="col-md-4 ro_img">
              <img src="map/img/map/img1.jpg">
            </div>
            <div class="col-md-8 ro_content">
            <div class="ro_content">
              <h4>Upto 40% Off on Food </h4>
              <p> 
                Modern hotel offering a restaurant, an outdoor pool many more..
              </p>
              <div class="ro_stars">
                <div class="col-md-6 ul_star">
                <ul>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                </ul>
                (12)
                </div>
                <div class="col-md-6 ro_offer">
                  <p><span>$12</span>&nbsp;&nbsp; $7</p>
                </div>
              </div>
            </div>
            </div>
          </div>

          <div class="rating_offer">
            <div class="col-md-4 ro_img">
              <img src="map/img/map/img2.jpg">
            </div>
            <div class="col-md-8 ro_content">
            <div class="ro_content">
              <h4>Upto 40% Off on Lunch </h4>
              <p> 
                Modern hotel offering a restaurant, an outdoor pool many more..
              </p>
              <div class="ro_stars">
                <div class="col-md-6 ul_star">
                <ul>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                </ul>
                (4,65)
                </div>
                <div class="col-md-6 ro_offer">
                  <p><span>$30</span>&nbsp;&nbsp; $18</p>
                </div>
              </div>
            </div>
            </div>
          </div>

          <div class="rating_offer">
            <div class="col-md-4 ro_img">
              <img src="map/img/map/img3.jpg">
            </div>
            <div class="col-md-8 ro_content">
            <div class="ro_content">
              <h4>Upto 32% Off on Seafood </h4>
              <p> 
                Modern hotel offering a restaurant, an outdoor pool many more..
              </p>
              <div class="ro_stars">
                <div class="col-md-6 ul_star">
                <ul>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                </ul>
                (132)
                </div>
                <div class="col-md-6 ro_offer">
                  <p><span>$38</span>&nbsp;&nbsp; $26</p>
                </div>
              </div>
            </div>
            </div>
          </div>

          <div class="rating_offer">
            <div class="col-md-4 ro_img">
              <img src="map/img/map/img4.jpg">
            </div>
            <div class="col-md-8 ro_content">
            <div class="ro_content">
              <h4>Upto 31% Off Howl </h4>
              <p> 
                Modern hotel offering a restaurant, an outdoor pool many more..
              </p>
              <div class="ro_stars">
                <div class="col-md-6 ul_star">
                <ul>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                  <li>
                    <i class="fa fa-star"></i>    
                  </li>
                </ul>
                (125)
                </div>
                <div class="col-md-6 ro_offer">
                  <p><span>$50</span>&nbsp;&nbsp; $35</p>
                </div>
              </div>
            </div>
            </div>
          </div>

          <div class="airfair">
            <div class="col-md-12 airfair_header">
              <p>Airfare & Accommodation Paid Separately</p>
            </div>
            <div class="clearfix"></div>
            <div class="airfair_one">
              <div class="col-md-4">
                <p>Day 1</p>
              </div>
              <div class="col-md-3">
                <p>$200</p>
              </div>
              <div class="col-md-5 air_red">
                <p><span>$400</span>&nbsp;&nbsp;50% OFF</p>
              </div>
            </div>

            <div class="airfair_one a_two">
              <div class="col-md-4">
                <p>Day 2</p>
              </div>
              <div class="col-md-3">
                <p>$150</p>
              </div>
              <div class="col-md-5 air_red">
                <p><span>$300</span>&nbsp; &nbsp;50% OFF</p>
              </div>
            </div>

            <div class="airfair_one">
              <div class="col-md-4">
                <p>Day 3</p>
              </div>
              <div class="col-md-3">
                <p>$100</p>
              </div>
              <div class="col-md-5 air_red">
                <p><span>$200</span>&nbsp;&nbsp;50% OFF</p>
              </div>
            </div>

            <div class="airfair_one a_two">
              <div class="col-md-4">
                <p>Day 4</p>
              </div>
              <div class="col-md-3">
                <p>$250</p>
              </div>
              <div class="col-md-5 air_red">
                <p><span>$500</span>&nbsp; &nbsp;50% OFF</p>
              </div>
            </div>

            <div class="airfair_one total">
              <div class="col-md-4">
                <p>Total</p>
              </div>
              <div class="col-md-3">
                <p>$700</p>
              </div>
              <!-- <div class="col-md-5">
                <p><span>$400</span>&nbsp;50% OFF</p>
              </div> -->
            </div>

          </div>

        </div>
      </div>


      <div class="sections">
        <div class="col-md-9 sec">
          <div class="col-md-6 section_deals">
            <img src="map/img/map/s1.jpg">
          </div>
          <div class="col-md-6 section_deals sd_right">
            <img src="map/img/map/s2.jpg">
          </div>
        </div>
      </div>

      


    </div>
  </div>
    

    <!-- modal -->

      <div class="modal fade treasures-around-me" id="myModal" role="dialog">
                          <div class="modal-dialog map_dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="">
                                  <div class="col-md-12">
                                      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                  </div>
                              </div>
                              <div class="modal-body map_modal_body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <h5>Food & Drinks</h5>
                                        <input type="checkbox" name=" " value=" ">Dinner<br>
                                        <input type="checkbox" name=" " value=" ">Lunch<br>
                                        <input type="checkbox" name=" " value=" ">Fast Food<br>
                                        <input type="checkbox" name=" " value=" ">Breakfast<br>
                                        <input type="checkbox" name=" " value=" ">Cafe Dessert Bakes<br>
                                        <input type="checkbox" name=" " value=" ">Club & Bars<br>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>Things To Do</h5>
                                        <input type="checkbox" name=" " value=" ">Adventure<br>
                                        <input type="checkbox" name=" " value=" ">Kid Activities<br>
                                        <input type="checkbox" name=" " value=" ">Sporting Events<br>
                                        <input type="checkbox" name=" " value=" ">Education<br>
                                        <input type="checkbox" name=" " value=" ">Design<br>
                                        <input type="checkbox" name=" " value=" ">Training & Development<br>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>Local Services</h5>
                                        <input type="checkbox" name=" " value=" ">Fasion<br>
                                        <input type="checkbox" name=" " value=" ">Home & Appliances<br>
                                        <input type="checkbox" name=" " value=" ">Kid Stores<br>
                                        <input type="checkbox" name=" " value=" ">Shopping<br>
                                        <input type="checkbox" name=" " value=" ">Acountancy Services<br>
                                        <input type="checkbox" name=" " value=" ">Photo Studio<br>
                                    </div>
                                    <div class="col-md-2">
                                        <h5>Travel</h5>
                                        <input type="checkbox" name=" " value=" ">Reservation<br>
                                        <input type="checkbox" name=" " value=" ">Rent a Car<br>
                                        <input type="checkbox" name=" " value=" ">Outdoor<br>
                                        <input type="checkbox" name=" " value=" ">Tourism<br>
                                        
                                    </div>
                                </div>
                              </div>
                              <div class="modal-footer map_modal_footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal" style="background: #3479b4;    color: white;">Close</button>
                              </div>
                            </div>
                            
                          </div>
                        </div>


      <script type="text/javascript" src="map/js/jquery-3.3.1.min.js"></script>
      <script type="text/javascript" src="map/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="map/js/jquery.js"></script>
       <script type="text/javascript" src="map/jquery.sticky.js"></script>
      <script>
      // $(document).ready(function(){
      //     $('[data-toggle="tooltip"]').tooltip();   
      // });

      $(document).ready(function(){
        $('.location1').tooltip('show');
          $('[data-toggle="popover"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="map/img/map/green-banner.jpg" class="media-object" alt="Sample Image" class="img-responsive"></a></div>'
          });
          $('[data-toggle="popover2"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="map/img/map/red-banner.jpg" class="media-object" alt="Sample Image"></a></div>'
          });
          $('[data-toggle="popover3"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="map/img/map/light-green-banner.jpg" class="media-object" alt="Sample Image" ></a></div>'
          });
          $('[data-toggle="popover4"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="map/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });
          $('[data-toggle="popover5"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="map/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });
          $('[data-toggle="popover6"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="map/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });
          $('[data-toggle="popover7"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="map/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });
          $('[data-toggle="popover8"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="map/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });
          $('[data-toggle="popover9"]').popover({
              placement : 'bottom',
          trigger : 'hover',
              html : true,
              content : '<div class="media"><a href="#" class=""><img src="map/img/map/tooltip-1.jpg" class="media-object" alt="Sample Image"></a><div class="media-body"><h4 class="media-heading">Lorem Ipsum</h4><p>Lorem Ipsum is simply dummy text.</p></div></div>'
          });

      });
      
      $('button').click(function(e){
             $('#myDiv').toggleClass('fullscreen'); 
      });
      </script>

<!-- Start of REVE Chat Script-->
<script type='text/javascript'>
    window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account='9571569';w.__revechat_version=2;
      r._= []; var rc = d.createElement('script'); rc.type = 'text/javascript'; rc.async = true; rc.setAttribute('charset', 'utf-8');
      rc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.revechat.com/widget/scripts/new-livechat.js?'+new Date().getTime();
      var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(rc, s);
    })(document, window);
    </script>
    <!-- End of REVE Chat Script -->

    </body>
    </html>