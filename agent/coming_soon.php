<?php
include('admin.php');

$obj=new Admin();


 $username = $_SESSION['username'];

 // echo $username;
 // exit();

if(!$username)
{
   echo '<script type="text/javascript">window.location ="login.php";</script>';
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Landing Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link rel="stylesheet" type="text/css" href="coming_soon/1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="coming_soon/1/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="coming_soon/1/css/style-1.css">
    <link rel="stylesheet" type="text/css" href="coming_soon/1/css/coming_soon.css">
    <link rel="stylesheet" type="text/css" href="coming_soon/1/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" type="images/x-icon" href="coming_soon/1/img/favicon.png">
    <script type="text/javascript" src="coming_soon/1/jquery.sticky.js"></script>
    <style type="text/css">
           #custom_carousel .item {
            color: #000;
            background-color: #eee;
            padding: 20px 0;
        }
        
        #custom_carousel .controls {
            overflow-x: auto;
            overflow-y: hidden;
            padding: 0;
            margin: 0;
            white-space: nowrap;
            text-align: center;
            position: relative;
            background: #ddd
        }
        
        #custom_carousel .controls li {
            display: table-cell;
            width: 1%;
            max-width: 90px
        }
        
        #custom_carousel .controls li.active {
            background-color: #eee;
            border-top: 3px solid orange;
        }
        
        #custom_carousel .controls a small {
            overflow: hidden;
            display: block;
            font-size: 10px;
            margin-top: 5px;
            font-weight: bold
        }
    </style>
    
</head>
<body>

    <div class="coming_soon">
        <div class="cs_banner">
            <div class="cs_banner_content">
                <!-- <img src="img/coming_soon_bg.png"> -->
                <div class="cs_tj">
                    <img src="coming_soon/1/img/tj-1.png">
                    <h3>HOW TJʼS TRIP PLANNER <br>WORKS</h3>
                        
                        <div class="csb_left">
                            <div class="csb_lc">
                                <h4>Discover the ultimate Trip Itinerary in<br>
                                    <span>6 EASY STEPS</span>
                                </h4>
                                <p>We simply ask you a series questions to create a trip profile for you using over 250,000.000 data points and based on the results we match that profile to a list of customizable itineraries with built in treasure maps to choose from.</p>
                               
                            </div>
                            <div class="csb_arrow">
                                <img src="coming_soon/1/img/tj-right.png">
                            </div>
                        </div>
                </div>
            </div>

        </div>

        <div class="cs_form_content">
        <div class="cs_form">
            <div id="first-slider">
                            <div id="carousel-example-generic" class="carousel slide carousel-fade" data-interval="false">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                </ol>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <!-- Item 1 -->
                                    <div class="item active slide1">
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <img style="max-width: 100%;"  data-animation="animated zoomInLeft" src="coming_soon/1/img/coming_soon/1.png">
                                            </div>
                                         
                                        </div>
                                           
                                            <a class="right carousel-control first-c" href="#carousel-example-generic" role="button" data-slide="next">
                                               <div class=""> <span>START EXPLORING</span>
                                                </div>
                                            </a>

                                      
                                     </div> 
                                    <!-- Item 2 -->
                                    <div class="item slide2">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                 <img style="max-width: 100%;"  data-animation="animated zoomInLeft" src="coming_soon/1/img/coming_soon/2.png">
                                             </div>
                                            <!-- <div class="col-md-5 text-right">
                                                <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="http://s20.postimg.org/sp11uneml/rack_server_unlock.png">
                                            </div> -->
                                        </div>
                                             <a class="right carousel-control first-c" href="#carousel-example-generic" role="button" data-slide="next">
                                               <div class=""> <span>KEEP EXPLORING</span>
                                                </div>
                                            </a>
                                        </div>
                                    <!-- </div> -->
                                    <!-- Item 3 -->
                                    <div class="item slide3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                 <img style="max-width: 100%;"  data-animation="animated zoomInLeft" src="coming_soon/1/img/coming_soon/3.png">
                                             </div>
                                              
                                        </div>
                                        <a class="right carousel-control first-c" href="#carousel-example-generic" role="button" data-slide="next">
                                               <div class=""> <span>KEEP EXPLORING</span>
                                                </div>
                                            </a>
                                        </div>
                                    <!-- </div> -->
                                    <!-- Item 4 -->
                                    <div class="item slide4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                 <img style="max-width: 100%;"  data-animation="animated zoomInLeft" src="coming_soon/1/img/coming_soon/4.png">
                                             </div>
                                           
                                        </div>
                                         <a class="right carousel-control first-c" href="#carousel-example-generic" role="button" data-slide="next">
                                               <div class=""> <span>KEEP EXPLORING</span>
                                                </div>
                                            </a>
                                        </div>
                                   
                                    <!-- End Item 4 -->
                            
                                </div>
                                <!-- End Wrapper for slides-->
                                
                            </div>
                </div>
             </div>
        </div>
        <div class="b-bottom">
            <h3>HAVE A MUCH MORE EXCITING TRIP - SAVE A LOT OF TIME - SAVE A LOT OF MONEY</h3>
        </div>
    </div>


    <!-- step 2 -->

    <div class="step-second">
        
        <div class="col-md-6 col-md-push-6 ">
            <div class="second-right">
                <div class="second-circle"><p>2</p></div>
                <h3>CHOOSE AN ITINERARY</h3>
                <div class="second-content">
                    <div class="days">
                        <div class="days-left">
                            <p>1-Day</p>
                        </div>
                        <div class="days-right">
                            <p>ON A BUDGET!</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>2-Day</p>
                        </div>
                        <div class="days-right">
                            <p>ON A BUDGET!</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>3-Day</p>
                        </div>
                        <div class="days-right">
                            <p>OF LUXURY</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>4-Day</p>
                        </div>
                        <div class="days-right">
                            <p>OF REST AND RELAXATION ON THE BEACH</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>5-Day</p>
                        </div>
                        <div class="days-right">
                            <p>OF CHEAP BREAKFAST & INCREDIBLE NIGHTLIFE</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>7-Day</p>
                        </div>
                        <div class="days-right">
                            <p>IN AN EXOTIC LAND</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>8-Day</p>
                        </div>
                        <div class="days-right">
                            <p>WITH NO KIDS IN A LAND FAR FAR AWAY</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>10-Day</p>
                        </div>
                        <div class="days-right">
                            <p>AWAY FROM MY FAMILY ALONE ANYWHERE</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>13-Day</p>
                        </div>
                        <div class="days-right">
                            <p>IN YOUR FAVORITE CITY</p>
                        </div>
                    </div>
                    <div class="days clearfix">
                        <div class="days-left">
                            <p>14-Day</p>
                        </div>
                        <div class="days-right">
                            <p>ON A SAFARI</p>
                        </div>
                    </div>

                    <div class="second-content-bottom">
                        <h3>If you can dream it and have a desire to go, <br>
                            Let us help you make it happen!</h3>
                            <h3 class="scb">We want to make your dreams come true!</h3>
                    </div>

                </div>

            </div>
        </div>
        <div class="col-md-6 col-md-pull-6">
            <div class="second-left">
                <h3 class="smain">AFTER A FEW QUICK QUESTIONS</h3>
                <div class="smain2">
                    <h3 class="smain2">
                        We simply <span class="h-pink">Match</span> your
                        <span class="h-green">Trip Profile</span> to highly reviewed 
                        pre-made <span class="h-red">Customizable</span> 
                        <span class="h-dblue">Itineraries</span> that already have <span class="h-blue">Built In </span>
                        <span class="h-yellow">Treasure Maps </span> 
                        loaded with <span class="h-lgreen">Hidden Gems</span>, 
                        <span class="h-blue">Golden Places </span>- Planned<span class="h-pink"> Transportation</span> and 
                        <span class="h-lred">Exciting Things to See and Do</span>.
                    </h3>
                </div>
                <div class="sl-offer">
                    <h3>
                        Save up to 65% on Travel & up to 90% off <br>
                        on Restaurants, Pubs/Clubs & Activities!
                    </h3>   
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
        
        <!-- Step 3 -->

        <div class="step-third">
                
                <div class="col-md-6 col-md-push-6">
                    <div class="third-right">
                        <div class="second-circle thirdc"><p>3</p></div>
                        <div class="third-content">
                            <h3>Now edit the Treasure Map Itinerary <br>
                            of choice to your exact personal desires.</h3>
                            <p>Have fun and customize the Day-by-day Itinerary. <br>
                                The schedule is customizable and already has a built-in <br>
                                treasure map. The itinerary plan includes your desired <br>
                                types of travel activities, attractions, restaurants, tours,<br> 
                                nightlife, events, transportation needs <br>
                                and accommodations that altogether fit your <br>
                                trip profile desires.
                            </p>
                            <p class="p-bold">TJ’s Power Planning Engine continuously scans<br> and optimizes for such details as shown to the far left.<br>
                                Our revolutionary Power Planning Engine has <br>
                                even considered the weather throughout <br>
                                the entire process.</p>
                        </div>
                    </div>
                </div>  
                <div class="col-md-6 col-md-pull-6">
                    <div class="third-left">
                        <div class="col-md-2 tl-img">
                            <img src="coming_soon/1/img/coming_soon/1a.png">
                            <img src="coming_soon/1/img/coming_soon/2a.png">
                            <img src="coming_soon/1/img/coming_soon/3a.png">
                            <img src="coming_soon/1/img/coming_soon/4a.png">
                            <img src="coming_soon/1/img/coming_soon/5a.png">
                            <img src="coming_soon/1/img/coming_soon/6a.png">
                        </div>
                        <div class="col-md-10">
                            <div class="third-map">
                                <!-- <iframe src="https://maps.google.com/maps?q=orlando&t=&z=13&ie=UTF8&iwloc=&output=embed"></iframe> -->
                                <img src="coming_soon/1/img/map2.jpg">
                                <h3>You are <span>NOW EMPOWERED</span> with 
                                    Super Human Trip Planning Powers!</h3>
                            </div>
                        </div>
                    </div>
                </div>         
        </div>
        <div class="clearfix"></div>
        <!-- Step - 4 -->

        <div class="step-fourth">
            
            
            <div class="col-md-6 ">
                <div class="fourth-left">
                    <div class="sixthc fourthc"><p>4</p></div>
                    <h3>CUSTOMIZE YOUR PLAN TO FIT YOUR NEEDS!</h3>
                    <h4>Make necessary changes regarding various features of your plan.</h4>
                    <p>Move any feature of the itinerary along the plan backward and forward. <br>
                        Check to see when any feature of your itinerary is open or closed.<br>
                        Extend or Shorten the duration of any feature of the itinerary.<br>
                        Invite or add co-travelers to access your itinerary if you want.<br> 
                        Receive recommendations on when the best time for visit is.<br> 
                        Minimize travel time and enjoy every moment.<br>
                        Optimize your Itinerary to your hearts content.<br>
                        Attach notes to every feature of the itinerary.<br>
                        Add or Remove any feature of the itinerary.
                    </p>
                        <h4 class="fourth-color">
                            Swap out restaurants to Sushi or 
                            Mexican VS American as an example 
                            or swap out the Night Life and attractions 
                            for a better fit to your needs and budget.
                             It’s that simple!
                        </h4>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="fourth-right">
                    <div class="second-circle fourthc fourthc1"><p>4</p></div>
                    <div class="fourth-content">
                        <img src="coming_soon/1/img/coming_soon/4th-img.png">
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        
        <!-- step fifth  -->

        <div class="step-five">
           
            <div class="col-md-6 col-md-push-6">
                
                 <div class="fifth-right">
                    <div class="second-circle fifthc"><p>5</p></div>
                    <div class="fifth-content">
                        <img src="coming_soon/1/img/5th_step.jpg">
                        <!-- <h4>Now Invest in Your Treasure Map, complete with  Fat Savings & Slim Prices!<br>
                            You have more time, to have a lot more fun!<br>
                            And you have more money for  Souvenirs to bring home!<br>
                            Book the entire itinerary while supplies last and SAVE additional 15% now!<br>
                            Minimum 7 days advance purchase of  your departure dates required!</h4> -->
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <div class="five-left">

                <div class="fifth-content">
                        <img src="coming_soon/1/img/HowtheTJmap.jpg">
                        
                    </div>
                    
                <!--  -->

                <!-- <div class="fifthr-bottom">
                    
                    <h3>Orlando Trip Planner</h3>
                    <div class="stars">
                       <ul>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-full"></i></li>
                        </ul>
                        <p>(335 399)</p>
                            
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <div class="stars-right">
                        
                    </div>
                </div> -->
            </div>

            </div>
        </div>
<div class="clearfix"></div>
         <!-- sixth step -->

        <div class="step-six">
            <div class="col-md-6">
                <div class="step-sixth-border"></div>
                <div class="six-left">
                    <div class=""><div class="sixthc sixc"><p>6</p></div>
                    </div>
                    <h3>Your complete plan in one place!</h3>
                    <h4>Congratulations</h4>
                    <h5>Its time you get paid back for your time and efforts.</h5>
                    <p>You have Completed yet another step in a rewarding Journey with TJ. Feel free to keep your itinerary completely private or share it both online and offline. We encourage you to share your itinerary with your friends, family, and the world! Download your itinerary and not only use it but share it offline!
                    We designed TJ for all of us. & we want you to know how much we value your time and efforts.
                    By combining our efforts, we continue to build on what is to be a wonderful ongoing journey together. A journey that’s full of excitement, provides you & the TJ Community as a whole with the ability to earn additional revenue, play, travel & support our communities as a whole </p><br>
                    <p><b>Once you have traveled and used your itinerary improve upon it if you can.</b><br>
                    •   Challenge yourself to create the very best itinerary with the very best treasure map built in.<br>
                    •   Add that extra hidden gem you may have found while traveling to make it even better, update the details or tell your story about a spec ific product, service, ticket, or place.<br>
                    •   All this helps you reap rewards from your itinerary and makes sure others really enjoy their travels as well.
                    </p><br>
                    <p><b>TJ Members generate revenue when sharing their itineraries that contain built in Treasure Maps.</b>  <a href="" class="six-button" data-toggle="modal" data-target="#myModal">Read More..</a></p><br>
                    <p>

                </div>
                <!-- <div class="six-btn"> 
                 <a href="" class="six-button" data-toggle="modal" data-target="#myModal">Read More..</a>
                </div> -->
                <div class="six-share">
                    <h4>Share On</h4>
                    <ul>
                        <li><a href=""><img src="coming_soon/1/img/facebook-1.png"></a></li>
                        <li><a href=""><img src="coming_soon/1/img/twitter1.png"></a></li>
                        <li><a href=""><img src="coming_soon/1/img/instagram1.png"></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="six-right">
                    <div class="second-circle abc"><p>6</p></div>
                     <div class="sixth-content">
                        <img src="coming_soon/1/img/coming_soon/map.png">
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="coming_soon_boottom">
            <center><h3>www.traveljolly.com</h3></center>
        </div>




<!--  -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog coming_soon_modal">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Your complete plan in one place!</h4>
      </div>
      <div class="modal-body cs-body">
        
               <p> •   Treasure Maps are filled with TJ Pin Partner Businesses that TJ Members have recommended TJ Support with TJ's Marketing services.<br>

                •   These businesses have agreed to discount their products and or services to the local community & tourist visiting their area. These discounts become valuable treasures and hidden gems within TJ.<br>

                •   Agreements produce treasures and hidden gems of up to 90°/o off at Restaurants, Pubs I Clubs, Activities, Things to Do & Places to See within a

                community . <br>Agreements I Deals are then available to the local community and visiting tourist via TravelJolly .com<br>

                •   TJ Members are encouraged and generate revenue nudging businesses they have personally enjoyed themselves & may personally know the staff.
                </p><br>
                    <p>1.  To learn more about how to be paid for your time and efforts by nudging businesses to become a TJ Pin Partner Businesses & easily generate yourself additional ongoing revenue click here.<br>

                        2.  Click Here about how to make money with your Itineraries.<br>

                        3.  Add itionally , there are currently several other ways to earn revenue with TJ as a member Click Here.<br>

                        4.  Click here to find additional ways to improve your travels in life and be paid for it. We are also trying to hire at light speed . Send your resume and setup an appointment with us. Learn more about our hiring needs.<br>
                        •   Example: Want a press pass?
                </p><br>
                <p>
                    <b>Travelers review itineraries (Gold Stars) and easily recognize a member's time and efforts.</b><br><br>

                    The better your reviews the more travelers will use your itinerary template. You get paid when travelers purchase the deals, treasures & hidden gems that are sprinkled throughout a good itinerary .<br><br>

                    <b>Success Principle Notes</b>: You cannot give of yourself without receiving. It is the same as to have or find a friend you must first start by being a friend. You can not cozy up to a fireplace and get heat from it unless you do what is needed to create the fire first. A farmer must first work the ground till the soil plant the seed water the field, get rid of the weeds and continually care for the field before he can harvest the field and generate revenue! Such is life!<br><br>
                    We are in an endless pursuit to not only support you in your needs but the community you live in as well.<br><br>

                    We are working to make the world a greener place, protect our children and help provide a fully funded higher education for all children.<br><br>

                    Mission Statement: "Toprotect a parents most valuable investment their  children, and the communities most valuable investment, its future!"<br><br>

                    We are a part of the rGreenConsortium.com initiative .<br><br>

                    <b>As you explore more of TJ you will find additional ways to profit from your time and efforts, many things beyond just money , initiatives we have wrapped ourselves around that heal communities. Please do your part its easy , simply do not forget to #ShareTheLove . Use your Super Powers!!!</b>

                </p>


            
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>

  </div>
</div>

<script>
            $(document).ready(function (ev) {
                $('#custom_carousel').on('slide.bs.carousel', function (evt) {
                    $('#custom_carousel .controls li.active').removeClass('active');
                    $('#custom_carousel .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
                })
            });
        </script>

    <script type="text/javascript" src="coming_soon/1/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="coming_soon/1/js/bootstrap.min.js"></script>
    <!-- Start of REVE Chat Script-->
    <script type='text/javascript'>
    window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account='9571569';w.__revechat_version=2;
      r._= []; var rc = d.createElement('script'); rc.type = 'text/javascript'; rc.async = true; rc.setAttribute('charset', 'utf-8');
      rc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.revechat.com/widget/scripts/new-livechat.js?'+new Date().getTime();
      var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(rc, s);
    })(document, window);
    </script>
    <!-- End of REVE Chat Script -->
</body>
</html>