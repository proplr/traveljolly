<?php
include('admin.php');

$obj=new Admin();


 $username = $_SESSION['username'];

 // echo $username;
 // exit();

if(!$username)
{
   echo '<script type="text/javascript">window.location ="login.php";</script>';
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Landing Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link rel="stylesheet" type="text/css" href="coming_soon3/1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="coming_soon3/1/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="coming_soon3/1/css/style-1.css">
      <link rel="stylesheet" type="text/css" href="coming_soon3/1/css/coming_soon.css">
    <link rel="stylesheet" type="text/css" href="coming_soon3/1/css/responsive.css">
     <!-- <link rel="stylesheet" type="text/css" href="css/coming_soon.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" type="images/x-icon" href="coming_soon3/1/img/favicon.png">
    <script type="text/javascript" src="coming_soon3/1/jquery.sticky.js"></script>
   
</head>
  <body>
    <div class="founding_partners">
      <div class="fp_pre">
        <div class="container-fluid"> 
          <div class="fp_header">
            <div class="col-md-2">
              <div class="fp_header_img">
                <img src="coming_soon3/1/img/coming_soon/groupon.png">
              </div>
            </div>
            <div class="col-md-8">
               <div class="fp_h">
                  <h3 class="hleft">We pay</h3>
                       <h3><span>Founding <img src="coming_soon3/1/img/coming_soon/location.png"> Partners</span></h3>
                      <h3 class="hright">upfront !</h3>
               </div>
            </div>
            <div class="col-md-2">
              <div class="fp_header_img">
                <img src="coming_soon3/1/img/coming_soon/business-1.png">
              </div>
            </div>


           
          </div>
          <div class="clearfix"></div>
          <div class="fp_add">
            <h3>TJ Promotes your business for you!</h3>
            <h4>No upfront fees to get started!</h4>
          </div>
        </div>
      </div>

      <div class="fp_body">
        <div class="container-fluid">
          <div class="col-md-3 col-sm-3">
            <div class="fp_card">
              <img src="coming_soon3/1/img/coming_soon/card1.png">
            </div>
          </div>
          <div class="col-md-9 col-sm-9">
            <div class="fp_text">
              

              <h3>
               <a>Sign up NOW</a> and become a <span class="fp_top">Founding <img src="coming_soon3/1/img/coming_soon/map1.png"> Partner </span>on <span  class="fp_tj">TravelJolly.com</span></h3>
                <br>
                <h3 class="auto">You automatically receive:</h3>
                <h4 class="gc">
                 A $1,000 Booking Engine Gift Card!<br>
                 A Golden VIP Invitation to the Grand Opening Startup Event of The Year!<br>
               <!--   <h4 class="vip"> --><span>*</span>VIP Invitations = Guest + 1</h4>
                <br>
                 <h4 class="co_work">As a <span class="co_work">Founding  <img src="coming_soon3/1/img/coming_soon/map1.png"> Partner</span> we want to go the extra mile and celebrate our new working relationship together.</h4><br>
                 <h4 class="co_work1">
                   To kick off celebrations TJ is investing into your business by freely providing Incentive's to not just an owner or Manager but to those Co-Workers whom constantly interact with your customers. We want to support your needs to motivate and inspire them. Use these 4 additional VIP Invitations & Gift Cards to kick things off and if you need more just reach out to us with a plan and we will do more. Maybe a lot more! How can we help?

                 </h4>
              
              <div class="fp_t">
                <div class="col-md-12 col-sm-12 fp_tsmal">
                  <div class="fp_texth">
                    <ul>
                      <li>
                         You also Receive Four Additional Complimentary $249 Booking Engine Gift Cards to provide employees, each with their own VIP Invitations to our Grand Opening Startup Event! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red">*</span><span class="ul_span">VIP Invitations=Guest + 1</span>
                      </li>
                      <li>
                        Extremely low <span class="ul_text">Founding  <img src="coming_soon3/1/img/coming_soon/map1.png"> Partner</span> Deal Fee’s!
                      </li>
                      <li>
                        50% - 50% Payout at 10 & 30 days!
                      </li>
                      <li> 
                        All the best features, reports, and rules you would expect from the best!
                      </li>
                    </ul>
                  
                    
                  </div>
                </div>
                <!-- <div class="col-md-3 col-sm-3 groupon">
                  <div class="col-md-12 col-sm-12 col-xs-12"> 
                    
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12"> 
                    <img src="img/coming_soon/business-1.png">
                  </div>
                </div> -->
              </div>
              <div class="clearfix"></div>
              <!-- <div class="fp_tbottom">
                <h3><b>lncentivize</b> your Co-Workers <span>Founding <img src="img/coming_soon/map1.png"> Partner </span>on TravelJolly.com</h3>
              </div> -->
              <div class="clearfix"></div>
              <div class="fp_bottom_img">
                <div class="col-md-3 col-sm-3 col-xs-6">
                  <img src="coming_soon3/1/img/coming_soon/card.png">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                  <img src="coming_soon3/1/img/coming_soon/card.png">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                  <img src="coming_soon3/1/img/coming_soon/card.png">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                  <img src="coming_soon3/1/img/coming_soon/card.png">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
             <div class="sign_up">
                <div class="col-md-4 su_img">
                  <a href="" title="signup now"><img src="coming_soon3/1/img/coming_soon/signup.png"></a>
                </div>
                <div class="col-md-8">
                  <h4>The offer is being Sponsored by <span class="sgreen">TravelJolly.com</span> and our associated<br> <span class="red">Grand Opening Celebration </span>Startup Event Partners. 
                  </h4>
                  <h3>This will not be available going forward!</h3>
                </div>
              </div>
              <div class="sign_up_bottom">
                <img src="coming_soon3/1/img/coming_soon/signupbg.png">
              </div>
          </div>
 

        </div>
       

      </div>

        
      
    
    
      <div class="footer">
        <div class="container-fluid">
          <center><p>Travel jolly | All Right Reserved</p></center>
        </div>
      </div>
  </div>
    
      <script type="text/javascript" src="coming_soon3/1/js/jquery-3.3.1.min.js"></script>
      <script type="text/javascript" src="coming_soon3/1/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="coming_soon3/1/js/jquery.js"></script>
       <script type="text/javascript" src="coming_soon3/1/jquery.sticky.js"></script>
    <!-- Start of REVE Chat Script-->
    <script type='text/javascript'>
    window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account='9571569';w.__revechat_version=2;
      r._= []; var rc = d.createElement('script'); rc.type = 'text/javascript'; rc.async = true; rc.setAttribute('charset', 'utf-8');
      rc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.revechat.com/widget/scripts/new-livechat.js?'+new Date().getTime();
      var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(rc, s);
    })(document, window);
    </script>
    <!-- End of REVE Chat Script -->
    </body>
    </html>