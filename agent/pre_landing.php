<?php
include('admin.php');

$obj=new Admin();


 $username = $_SESSION['username'];

 // echo $username;
 // exit();

if(!$username)
{
   echo '<script type="text/javascript">window.location ="login.php";</script>';
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Landing Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link rel="stylesheet" type="text/css" href="prelanding/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="prelanding/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="prelanding/css/style-1.css">
    <link rel="stylesheet" type="text/css" href="prelanding/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" type="images/x-icon" href="img/favicon.png">
    <script type="text/javascript" src="prelanding/jquery.sticky.js"></script>
   
</head>
  <body>
    <div class="pre-landing">
      <div class="pl-top pre">
        <div class="container-fluid">
          <div class="pre-top">
            <center><h4>Connected in more and more ways than you have ever witnessed before</h4></center>
          </div>
          <div class="pre-header">
            <center><h3>
              You decide where you want top go! Let us supply the editable intenerary!<br>
              You have a great time, save money and enjoy your self MORE!
            </h3></center>
          </div>

          <!-- <div class="pl-toph">
            <h3>
              You decide where you want top go! Let us supply the editable intenerary!<br>
              You have a great time, save money and enjoy your self MORE!
            </h3>
          </div>
          <div class="pl-top-strip">
            <p>Connected in more and more ways than you have ever witnessed before</p>
          </div> -->
        </div>
      </div>

      <div class="pl-content abbh">
        <div class="container-fluid">
          <div class="pl-c abhi">
            <p>Travel Local Nearby Deals at Upto 90% off Treasure Map Itineraries, 3 Million Points of Interest, 230,000,000 Data Points, Local Support, Tourist Support, Multi Currency including Bitcoins, Easy to use Trip Planner, 900+ Scheduled Chartered & No Frill Airlines, 800,000 + Hotels & Resorts, 25,000 + Car Hire Locations, 80 + Bed Banks, 140,000 Destinations, 30,00 + Transfer Locations, I5,000 + World's best Attractions, Major Cruise Lines, Multi Language, Membership, Restaraunts, Bars / Pubs, Nightlife, Activities, Shopping, Gift Cards & Hidden Gems, If you own a business let us incentivize you and your employess to create a deal we can market for you! We will make you money and you will be ecstatic wth the relationship you have with TravelJolly.com and our team of waorries! Please be aware we are hiring in your area, help us put the planet at your feet on a silver platter and on an affordable budget so you can have a lot more fun in life!</p>
          </div>    
        </div>
      </div>

      <div class="pl-offers">
        <div class="container-fluid"> 
          <div class="">
              <div class="col-md-4 col-sm-12 col-xs-12 pl-o">
                <div class="o1">
                  <h3>  Upto 90% Off</h3>
                  <p>Places to Stay - Your Itinerary<br> 
                    Places to See - Things to Do<br>
                    Theme Parks - Attractions<br>
                    Eats / Drinks - Shopping
                  </p>
                </div>
               <div class="o2">
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-1">
                      <h4>25,000+</h4>
                      <p>Car hire locations <br>/ worldwide</p>  
                  </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-r">
                    <div class="o2-content c-2">
                      <h4>30,000+</h4>
                      <p>Transfer loctions <br>/  Worldwide</p>  
                    </div>
                  </div>
                
                   <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-3">
                      <h4>140,000+</h4>
                      <p>Destinations</p>  
                    </div>
                   </div>
                   <div class="col-md-6 col-sm-6 col-xs-6 o2-r">
                      <div class="o2-content c-4">
                        <p>Major</p>  
                        <h4>Cruise lines</h4>
                      </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 o2-bottom">
                      <div class="o2-b c-5">
                        <h4>180+ Countries</h4>
                      </div>
                    </div>
                </div>
              </div>
              <!-- second second -->
              
              <div class="col-md-4 col-sm-12 col-xs-12 pl-o">
                <!-- <div class="o-3">
                  <h3>  Upto 90% Off</h3>
                  <p>Places to Stay - Your Itinerary<br> 
                    Places to See - Things to Do<br>
                    Theme Parks - Attractions<br>
                    Eats / Drinks - Shopping
                  </p>
                </div> -->
               
                  
                <div class="o-3">
                  <div class="col-md-5 col-sm-5 col-xs-5 o-3-sections1">
                      <div class="o-3-content c-6">
                        <h4>800,000+</h4>
                        <p>Hotels, Villas and<br>Appartments</p>  
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-3 o-3-sections2">
                      <div class="o-3-content c-1">
                        <h4>80+</h4>
                        <p>bed banks</p>  
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4 o-3-sections1">
                      <div class="o-3-content c-6 c-6a">
                        <h4>Multi <br>Currency</h4>
                        <!-- <p>Car hire locations <br>/ worldwide</p>   -->
                    </div>
                  </div>
                </div>

                  <div class="o2">
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-7">
                      <h4>900+</h4>
                      <p>Scheduled, Chartered <br>& no-frills airlines</p>  
                  </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-r">
                    <div class="o2-content c-8">
                      <h4>Bitcoin </h4>
                      <p>Currencey</p>  
                    </div>
                  </div>
                
                   <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-9">
                      <h4>15,000+</h4>
                      <p>Worlds best <br>attractions</p>  
                    </div>
                   </div>
                   <div class="col-md-6 col-sm-6 col-xs-6 o2-r">
                      <div class="o2-content c-10">
                        <p>Tourist Support</p>  
                        <h4>Been There Done That</h4>
                      </div>
                    </div>
                    <div class="for_lg">
                    <div class="col-md-12 col-sm-12 col-xs-12 o3-bottom">
                     <div class="o1 c-12">
                      <h3>  Easy To Use Trip Planner</h3>
                      <p>4 easy steps to Ian your trip itinerary
                      </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- third section -->
              
              <div class="col-md-4 col-sm-12 col-xs-12 pl-o">
               <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="o3-main c-13">
                 <h3>Nearby Deals</h3>
                 <p>Restaraunts <br>
                    Things To Do<br>
                    Activities, Nightlife<br>
                    Bars/Pubs, Monuments<br>
                    Theme Parks Museams
                  </p>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6 ">
                 <div class="o3-main-r1 c-14">
                   <h3>3 Million+</h3>
                   <p>Points of Interest</p>
                 </div>
                 <div class="o3-main-r2 c-15 clearfix">
                   <h3>Nearby </h3>
                   <p>Thats it!</p>
                 </div>
               </div>
               <div class="clearfix"></div>
               <div class="o2">
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-16">
                      <h4>Multi <br> Language</h4>
                      <!-- <p>Scheduled, Chartered <br>& no-frills airlines</p>   -->
                  </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 c-16 o2-r">
                    <div class="o2-content c-8">
                     <h4>Iteneraries </h4>
                      <p>Pre-Made</p>    
                    </div>
                  </div>
              </div>
              <div class="o2">
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-l">
                    <div class="o2-content c-7">
                      <h4>Local Support</h4>
                      <p>Been There <br>Done That</p>  
                  </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 o2-r">
                    <div class="o2-content c-17">
                      <h4>Treasure Maps </h4>
                      <p>Editable</p>  
                    </div>
                  </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="pre_load_img">
                  <img src="prelanding/img/pre_landing.jpg">
                </div>
              </div>
              <div class="for_xs">
                 <div class="col-md-12 col-sm-12 col-xs-12 o3-bottom">
                     <div class="o1 c-12">
                      <h3>  Easy To Use Trip Planner</h3>
                      <p>4 easy steps to Ian your trip itinerary
                      </p>
                      </div>
                    </div>
              </div>

          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="straight-line"></div>
    </div>

    <div class="trip-planner-section">
      <div class="container-fluid">
        <div class="trip-planner">
          <div class="tp-top">
            <center><h4>We provide everyone the world's best trip planner, saving you hours of frustration in just a few minutes, Simply modify one of our itineraries and be filled with excitment on every single step of your journy</h4></center>
          </div>
          <div class="tp-body">
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="prelanding/img/tp-1.png">
              </div>
              <center><p><b>Tell as where you want to go?</b><br><br>Simply answer a few questions about the trip you want to plan.</p></center>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="prelanding/img/tp-2.png">
              </div>
               <center><p><b>Choose a Treasure Map Iteneary based on your needs?</b></p></center>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="prelanding/img/tp-3.jpg">
              </div>
              <div class="arrow">
                  <img src="prelanding/img/arrow.png">
              </div>
              <center><p><b>Get a personalized plan</b><br><br>A complete day-by-day itinerary based on your preferences</p></center>
              
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="prelanding/img/tp-4.jpg">
              </div>
              <div class="arrow">
                  <img src="prelanding/img/arrow.png">
              </div>
              <center><p><b>Customize it </b><br><br>best routes and schedules <br>Refine your plan. We'll find that</p></center>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="prelanding/img/tp-5.jpg">
              </div>
              <div class="arrow1">
                  <img src="prelanding/img/arrow.png">
              </div>
              <center><p><b>Book it</b><br><br>Choose your activities, hotel, restaurants, Nightlife & transportation UN° 90% off Make ticket, hotel reservations</p></center>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
              <div class="tpb-img">
                <img src="prelanding/img/tp-6.jpg">
              </div>
              <center><p><b>Manage it online & Use it offline</b><br><br>Everything in one place.<br>
                Everyone on the same page.<br>Download the plan as your on-the go!</p></center>
            </div>
            <div class="clearfix"></div>
            <div class="tp-footer">
              <center><h4>Customizable Treasure Map Itineraries created by both locals and tourist, claiming to have been there, done that and bought the T-Shirt!</h4></center>
            </div>

          </div>
        </div>
      </div>
    </div>
      
    
      <div class="footer">
        <div class="container-fluid">
          <center><p><b>How Travel Jolly's Trip Planner Works</b><br>
Discover how to create ycur trip itinerary in 6 steps
 </p></center>
        </div>
      </div>

    
      <script type="text/javascript" src="prelanding/js/jquery-3.3.1.min.js"></script>
      <script type="text/javascript" src="prelanding/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="prelanding/js/jquery.js"></script>
       <script type="text/javascript" src="prelanding/jquery.sticky.js"></script>
    <!-- Start of REVE Chat Script-->
    <script type='text/javascript'>
    window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account='9571569';w.__revechat_version=2;
      r._= []; var rc = d.createElement('script'); rc.type = 'text/javascript'; rc.async = true; rc.setAttribute('charset', 'utf-8');
      rc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.revechat.com/widget/scripts/new-livechat.js?'+new Date().getTime();
      var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(rc, s);
    })(document, window);
    </script>
    <!-- End of REVE Chat Script -->
    </body>
    </html>