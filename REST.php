<?php
 
 require("search/iSDK/isdk.php");
 $app = new iSDK;
 $app->cfgCon("connection");

// get the HTTP method, path and body of the request
$method = $_SERVER['REQUEST_METHOD'];
//$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));

if (isset($_GET["action"]))
{
  switch ($_GET["action"])
    {
        case "searchByPhoneNumber":
            searchByPhoneNumber($app);
            break;
        case "searchByEmail":
            searchByEmail($app);
            break;
        case "updateContact":
            updateContact($app);
            break;
        case "addContact":
            addContact($app);
            break;
    }
}

function searchByPhoneNumber($app) 
{
    // InfusionSoft will always return the phone 
    // number formated as (xxx) xxx-xxxx so we 
    // need to strip what's entered & format it 
    // for the search
    // There's no match on less than 10 digits
    $input = json_decode(file_get_contents('php://input'),true);
    $ph = $input["phonenumber"];
    $ph = str_replace('-', '', $ph); 
    $ph = preg_replace('/[^0-9]/','',$ph);
    // This may be overkill, a hacked preg_replace
    // may have been enough
    if(strlen($ph) > 10) {
        $countryCode = substr($ph, 0, strlen($ph)-10);
        $areaCode = substr($ph, -10, 3);
        $nextThree = substr($ph, -7, 3);
        $lastFour = substr($ph, -4, 4);
        //	$ph = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour; // leave out country code
            $ph = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($ph) == 10) {
        $areaCode = substr($ph, 0, 3);
        $nextThree = substr($ph, 3, 3);
        $lastFour = substr($ph, 6, 4);
            $ph = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($ph) == 7) {
        $nextThree = substr($ph, 0, 3);
        $lastFour = substr($ph, 3, 4);
            $ph = $nextThree.'-'.$lastFour;
        }

    searchPhone($app, $ph);
}
function searchPhone($app, $phone) {
    $returnFields = array('Id','FirstName','LastName','Email','Phone1','Groups');
    $query = array('Phone1' => $phone);
    $contacts = $app->dsQuery("Contact",10,0,$query,$returnFields);
        
    // $myObj = new stdClass();
    // $myObj->count = count($contacts);
    // $myObj->name = "John";
    // $myObj->age = 30;
    // $myObj->city = "New York";
    // $myObj->method = $method;
    // $myObj->phonenumber = $input["phonenumber"];
    // $myJSON = json_encode($myObj);
    // echo $myJSON;
    //custom fields _customfieldname
    returnData($contacts);
}
function searchByEmail($app) 
{
    $input = json_decode(file_get_contents('php://input'),true);
    $em = $input["email"];
    searchEmail($app, $em, FALSE);
}
function searchEmail($app, $email, $returnInternal) {
    $returnFields = array('Id','FirstName', 'LastName','Email','Phone1','Groups');
    $contacts = $app->findByEmail($email, $returnFields);
    if ($returnInternal)
        return $contacts;
    else
        returnData($contacts);
}
function validateData($input) {
    $data = array();
    $firstNameRequired = false;
    $lastNameRequired = false;
    $phone1Required = false;
    $emailRequired = false;
    if (isset($input["firstNameRequired"]))
        $firstNameRequired = $input["firstNameRequired"];
    if (isset($input["lastNameRequired"]))
        $lastNameRequired = $input["lastNameRequired"];
    if (isset($input["phone1Required"]))
        $phone1Required = $input["phone1Required"];
    if (isset($input["emailRequired"]))
        $emailRequired = $input["emailRequired"];

    if (isset($input["id"]) && !empty($input["id"])) {
        $data["Id"] = $input["id"];
    } else{
        if ($_GET["action"] == updateContact) {
            throw new Exception('Id is required.');
        }
    }
    if (isset($input["fname"]) && !empty($input["fname"])) {
        $data["FirstName"] = $input["fname"];
    } else
        if ($firstNameRequired)
            throw new Exception('First name is required.');
    if (isset($input["lname"]) && !empty($input["lname"])) {
        $data["LastName"] = $input["lname"];
    } else
        if ($lastNameRequired)
            throw new Exception('Last name is required.');
    if (isset($input["phone1"]) && !empty($input["phone1"])) {
        $data["Phone1Type"] = "Mobile";
        $data["Phone1"] = $input["phone1"];
    } else
        if ($phone1Required)
            throw new Exception('Phone is required.');
    if (isset($input["email"]) && !empty($input["email"])) {
        $data["Email"] = $input["email"];
    } else
    if ($phone1Required)
        throw new Exception('Email is required.');
    //$groups = $input["groups"];
    if (isset($input["groups"]) && !empty($input["groups"])) {
        $data["Groups"] = $input["groups"];
    }
    return $data;
}
function updateContact($app) 
{
    $input = json_decode(file_get_contents('php://input'),true);
    $update = array();
    try {

        $update = validateData($input);
        if (!isset($update["Id"]) || empty($update["Id"]))
            throw new Exception('Id not provided. Update fails.');

        $updateCon = $app->dsUpdate("Contact", $update["Id"], $update);
        if (isset($update["Groups"]) && !empty($update["Groups"]))
            addTag($app, $update["Id"], $update["Groups"]);
        searchEmail($app, $update["Email"], FALSE);

    } catch(Exception $exception) {
        returnData(array(array("error" => $exception->getMessage())));
    }

}
function addContact($app) {
    //check if email exists first
    // The last variable determines how you 
	// want to match records. In this case 
	// I only matched based on 'Email', but 
	// we could have used 'EmailAndName', or
    // 'EmailAndNameAndCompany'
    $input = json_decode(file_get_contents('php://input'),true);
    $update = array();
    try {

        $update = validateData($input);
        $contactId = $app->addWithDupCheck($update, 'Email');
        $contacts = searchEmail($app, $update["Email"], TRUE);//to get the added record by email
        if ($contacts[0].Id !== NULL && !empty($contacts[0].Id)) {
            addTag($app, $contacts[0].Id, $update["Groups"]);
            returnData($contacts);
        }

    } catch(Exception $exception) {
        returnData(array(array("error" => $exception->getMessage())));
    }
}
function addTag($app, $contactId, $groups) {
     $result = $app->grpAssign($contactId, $groups);
}
function returnData($data) {
    if (is_array($data)){
        if (empty($data)) {
            echo json_encode(array(array("nodata" => $_GET["action"] . " failed to return data")));
        }
        else {
            echo json_encode($data);
        }
    }
    else {
        echo json_encode(array(array("error" => $_GET["action"] . " failed to retrieve data")));
    }
}
?>	