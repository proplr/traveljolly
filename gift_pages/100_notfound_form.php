<!DOCTYPE html>
<html>
    <head>
        <title>Landing Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/style-1.css?v=<?php echo time(); ?>">
        <link rel="stylesheet" type="text/css" href="css/responsive.css?v=<?php echo time(); ?>">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="shortcut icon" type="images/x-icon" href="img/favicon2.png">
        <script type="text/javascript" src="jquery.sticky.js"></script>
        <style type="text/css">
            .ab {
                background-color: rgba(0, 0, 0, 0.88);


            }
        </style>
        <script>(function (d, s, id) {
                var js, params, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                params = "justcall_token=PN06006ddfbf06e2f8e52905907b16037a^35dc295ba8e51e1df61c6920142d4f35&buttonSize=large";
                js = d.createElement(s);
                js.id = id;
                js.src = "https://justcall.io/clicktocall/justcall-click2call.js#" + params;
                fjs.parentNode.insertBefore(js, fjs);
            }(document, "script", "justcall-ajs"))</script>
    </head>
    <body>

    </body>
</html>
<!-- <div class="header">   -->
<!-- <div id="header-sroll">  -->
<div class="head" style="display:none">
    <div class="container-fluid">
        <div class="col-md-2 col-sm-4 col-xs-6 logo-md">
            <div class="head-img">
                <img src="img/logo.png" class="img-responsive">
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 h-sma">
            <div class="head-mid">
                <h3>The World’s  Newest & Greatest Booking Engine!</h3>
                <div class="mid-bottom">
                    <p class="offer">Coming The Summer 2018!</p>
                    <p class="fat"><span>FAT SAVINGS </span>SLIM PRICES</p>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2 padding-0 top-button">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 padding-0 m-login">
                    <a href="https://wholesale.traveljolly.com/" target="_blank" class="btn btn-default header-button">Member Login</a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 padding-0 l-guest">
                    <a href="http://travelsavingmoney.traveljolly.com/" target="_blank" class="btn btn-default header-button">Login as Guest</a>
                </div>
            </div>
        </div>
        <div class="col-md-2 right-md">
            <p class="head-p-r">Booking Engine Gift Cards are useable  for 1 year from date of issue</p>
        </div>
    </div>
</div>
<div class="up_part">
    <div class="second-p">
        <div class="container-fluid s-p">
            <div class="sec-logo">
                <img src="img/logo-1.png">
            </div>
            <div class="second-head">
                <h3>The World’s  Newest & Greatest Booking Engine!</h3>
                <p>UPTO 90% OFF ON NEARBY DEALS - BRILLIANT TREASURE MAPS ALONGSIDE MONEY & TIME SAVING ITINERARIES!<br>

                    <span class="bk_me">Coming The Summer 2018!</span><span class="bk_me1"> (Exact Date TBD)</span>
                </p>

            </div>
            <div class="second-button">
                <div class="col-md-6 col-sm-6 col-xs-6 padding-0 m-login">
                    <a href="https://wholesale.traveljolly.com/" target="_blank" class="btn btn-default header-button">Member Login</a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 padding-0 l-guest">
                    <a href="http://travelsavingmoney.traveljolly.com/" target="_blank" class="btn btn-default header-button">Login as Guest</a>
                </div>
            </div>
        </div>
    </div>

    <div class="third-p">
        <div class="banner">
            <img src="img/banner.png">
        </div>
        <div class="banner-content">
            <h3><span>Orlando Florida<br>
                    Mega Startup Event</span><br>
                (Let us pay for your trip or part of it!)</h3>
        </div>
        <div class="banner-bottom">
            <h3>CLAIM YOUR GIFT CARD NOW AND RECEIVE A VIP INVITATION TO THE PARTY EVENT OF THE YEAR!</h3>
            <p>WORLD CLASS EVERYTHING & EVERYBODY!  &nbsp;&nbsp;-&nbsp;&nbsp;  DO NOT MISS THIS EVENT OR YOU WILL REGRET IT FOR THE REST OF YOUR LIFE!  &nbsp;&nbsp;-&nbsp;&nbsp;  REMEMBER WOODSTOCK!</p>
        </div>
    </div>



    <div class="fourth">
        <div class="container-fluid">
            <h3>As Seen In & What Other Are Saying: </h3>
            <p>Traveljolly.com has delivered beyond my wildest expectation - Revolutionary - Utterly Amazing - Everything has changed travel will never be the same! - Samed my whole group time and money - As described.</p>
            <div class="fourth-bottom">
                <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
                    <div class="fourth-1">
                        <img src="img/c-1.png">
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
                    <div class="fourth-1">
                        <img src="img/c-2.png">
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
                    <div class="fourth-1">
                        <img src="img/c-3.png">
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
                    <div class="fourth-1">
                        <img src="img/c-4.png">
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
                    <div class="fourth-1">
                        <img src="img/c-5.png">
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 sm-clients">
                    <div class="fourth-1">
                        <img src="img/c-6.png">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="down_part" style="display: none;">
    <div class="fifth">
        <div class="container-fluid">
        <div class="row desktop_content hidden-sm hidden-xs">
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <!--LEFT SIDE: START-->
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 free-gift-xs">
                            <div class="free-gift">
                                <img src="img/free-gift.png">
                                <br/>
                                <div class="phone_number_wrapper hidden-sm hidden-xs">
                                    <span>ENTER</span><input type="text" placeholder="MOBILE NUMBER HERE " id="phone_number" class="phone_number"><label><i class="fa fa-search" onclick="get_part8();"></i></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12 g-det client_right_part">
                            <div class="row right_top_part">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <iframe style="width: 100%; height: 180px; background-color: rgb(0, 0, 0); border: 0px;" id="iframe6314" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen class="ui-selectee" src="https://fast.wistia.net/embed/iframe/6iq5v6ueu6?videoFoam=true"></iframe>
                                    <p>Introduction Video</p>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <iframe style="width: 100%; height: 180px; background-color: rgb(0, 0, 0); border: 0px;" id="iframe6514" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen class="ui-selectee" src="https://fast.wistia.net/embed/iframe/olg2hj9tli?videoFoam=true"></iframe>
                                    <p>Getting Paid</p>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <iframe style="width: 100%; height: 180px; background-color: rgb(0, 0, 0); border: 0px;" id="iframe6614" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen class="ui-selectee" src="https://fast.wistia.net/embed/iframe/kwyd3bykmt?videoFoam=true"></iframe>
                                    <p>We're Investing In You!!!</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="g-detail">
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                                        <img src="img/g-1.png">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                                        <img src="img/g-2.png">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                                        <img src="img/g-3.png">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                                        <img src="img/g-4.png">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                                        <img src="img/g-5.png">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp smallcom-p">
                                        <p>.. and many MORE!</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-6 big_guy">
                            <div class="tj-o">
                                <img src="img/guy.png" class="">
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 col-xs-12 card_block">
                            <div class="hidden-md hidden-lg mobile_card_block">
                                <img src="img/let-us-pay.png" >
                                <br/>
                            </div>
                            
                            <div class="card_main_div">
                                <div class="card_top">
                                    <p class="booking_engine">Booking Engine</p>
                                    <div class="gift_249_card row">
                                        <div class="col-md-2 col-sm-3 col-lg-2 col-xs-3">
                                            <img src="img/chip.png" width="20" alt="chip">
                                        </div>
                                        <div class="col-md-10 col-sm-9 col-lg-10 col-xs-9 gift_249_text">
                                            <p><font>$249</font> <span>gift card</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="middle_part_card">
                                    <div class="part1">
                                        <p class="card_label">We could not find your Gift Card. Let's try finding it with additional information.</p>
                                        <form action="#" class="form-group row">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder="*First Name" name="fname" id="fname">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder="*Last Name" name="lname">
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="email" class="form-control" placeholder="Email" name="email">
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="button" class="form-control btn btn-success" onclick="get_part2();" value="Find">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="card_bottom">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6 store_icon" style="display: none;">
                                            <img src="img/appstore.png" alt="chip">
                                        </div>
                                        <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6 store_icon" style="display: none;">
                                            <img src="img/googleplay.png" alt="chip">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-lg-4 col-xs-12 card_pin_icon">
                                            <span>Pin # PCGH</span>
                                        </div>
                                        <div class="col-md-4 col-sm-7 col-lg-4 col-xs-7 col-md-offset-1 col-sm-offset-1 col-lg-offset-1 col-xs-offset-1 sposor_text">
                                            <p>Sponsored by:</p>
                                        </div>
                                        <div class="col-md-3 col-sm-5 col-lg-3 col-xs-5 sponsor_img">
                                            <img src="img/traveljolly_logo_white.png" alt="chip">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <p class="info_msg_booking">Message & Data Rates May Apply. Reply STOP to cancel. Reply HELP for info. Consent is not any condition of any purchase.</p>
                            <p class="booking_msg">Booking Engine Gift Cards are usable for up to one year from date of issue.</p>
                        </div>
                    </div>

                    <!--LEFT SIDE : END-->
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 right_gift_part">
                    <!--RIGHT SIDE : START-->
                    <div class="card">
                        <img src="img/act-now.png">
                    </div>
                    <div class="card-bottom">
                        <center>
                            <div class="cb-top">
                                <img src="img/dollar.png">
                                <p>WORLDS TOP BOOKING ENGINES!</p>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                                <div id="let_us_play">
                                    <img src="img/let-us-pay.png" >
                                </div>
                            </div>
                        </center>
                        <div class="cb-bottom">
                            <div class="row">
                                <div class="col-md-8 col-sm-9 col-xs-9 call_widget">
                                    <div class="justcall-click2call-event justcall-click2call-button"></div>
                                    <p class="travel_2weeks">If traveling  within 2 weeks</p>
                                </div>
                                <div class="col-md-4 col-sm-3 col-xs-3">
                                    <div id="limited_gift">
                                        <img src="img/limited_gift.png" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <div class="g-door-small">
                                <div class="g-door-2">
                                    <br/>
                                    <!--<h4>Request a VIP Invitation and Gift Card!</h4>-->
                                    <p>(Redemption is based on limited BETA availability)</p>
                                </div>
                            </div>
                            <center>
                                <div class="visitors" id="visitors" style="display: none;">
                                    <div class="visitors-c">
                                        <p><span class="visitor_no">100</span> other people are on this page right now</p>
                                        <a href=""><div class="visitors-a">
                                                X  
                                            </div></a>
                                    </div>
                                </div>
                            </center>
                        </div>

                    </div>
                    <!--RIGHT SIDE : END-->
                </div>
                <a href="" data-toggle="modal" data-target="#myModal2"><div class="back-to-home"><img src="img/back.png">Back To Home</div></a>
            </div>

            <div class="mobile_content hidden-lg hidden-md">
                <div class="">
                    <!--RIGHT SIDE : START-->
                    <div class="card">
                        <img src="img/act-now.png">
                    </div>
                    <div class="card-bottom">
                        <center>
                            <div class="cb-top">
                                <img src="img/dollar.png">
                                <p>WORLDS TOP BOOKING ENGINES!</p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="">
                                <div class="g-detail">
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                                        <img src="img/g-1.png">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                                        <img src="img/g-2.png">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                                        <img src="img/g-3.png">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                                        <img src="img/g-4.png">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp">
                                        <img src="img/g-5.png">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-6 small-comp smallcom-p">
                                        <p>.. and many MORE!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 free-gift-xs">
                                <div class="free-gift">
                                    <img src="img/free-gift.png">
                                </div>
                            </div>
                            <div>
                                <div class="phone_number_wrapper hidden-lg hidden-md">
                                    <span>ENTER</span><input type="text" placeholder="MOBILE NUMBER HERE " id="phone_number" class="phone_number"><label><i class="fa fa-search" onclick="get_part8();"></i></label>
                                </div>
                            </div>
                            <div class="card_main_div">
                                <div class="card_top">
                                    <p class="booking_engine">Booking Engine</p>
                                    <div class="gift_249_card row">
                                        <div class="col-md-2 col-sm-3 col-lg-2 col-xs-3">
                                            <img src="img/chip.png"  alt="chip">
                                        </div>
                                        <div class="col-md-10 col-sm-9 col-lg-10 col-xs-9 gift_249_text">
                                            <p><font>$249</font> <span>gift card</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="middle_part_card">
                                    <div class="part1" >
                                        <p class="card_label">We could not find your Gift Card. Let's try finding it with additional information.</p>
                                        <form action="#" class="form-group row">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder="*First Name" name="fname" id="fname">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" placeholder="*Last Name" name="lname">
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="email" class="form-control" placeholder="Email" name="email">
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="button" class="form-control btn btn-success" onclick="get_part2();" value="Find">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="card_bottom">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6 store_icon" style="display: none;">
                                            <img src="img/appstore.png" alt="chip">
                                        </div>
                                        <div class="col-md-2 col-sm-6 col-lg-2 col-xs-6 store_icon" style="display: none;">
                                            <img src="img/googleplay.png" alt="chip">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-lg-4 col-xs-12 card_pin_icon">
                                            <span>Pin # PCGH</span>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-lg-4 col-xs-6 col-md-offset-1 col-sm-offset-1 col-lg-offset-1 col-xs-offset-1 sposor_text">
                                            <p>Sponsored by:</p>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6 sponsor_img">
                                            <img src="img/traveljolly_logo_white.png" alt="chip">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hidden-md hidden-lg mobile_card_block">
                                <img src="img/let-us-pay.png" >
                                <br/>
                            </div>
                            <div class="right_top_part">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <iframe style="width: 100%; height: 180px; background-color: rgb(0, 0, 0); border: 0px;" id="iframe6314" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen class="ui-selectee" src="https://fast.wistia.net/embed/iframe/6iq5v6ueu6?videoFoam=true"></iframe>
                                    <p>Introduction Video</p>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <iframe style="width: 100%; height: 180px; background-color: rgb(0, 0, 0); border: 0px;" id="iframe6514" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen class="ui-selectee" src="https://fast.wistia.net/embed/iframe/olg2hj9tli?videoFoam=true"></iframe>
                                    <p>Getting Paid</p>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <iframe style="width: 100%; height: 180px; background-color: rgb(0, 0, 0); border: 0px;" id="iframe6614" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen class="ui-selectee" src="https://fast.wistia.net/embed/iframe/kwyd3bykmt?videoFoam=true"></iframe>
                                    <p>We're Investing In You!!!</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-sm-9 col-xs-9 call_widget">
                                    <p class="travel_2weeks">If traveling  within 2 weeks</p>
                                    <div class="justcall-click2call-event justcall-click2call-button"></div>
                                </div>
                                <div class="col-md-4 col-sm-3 col-xs-3">
                                    <div id="limited_gift">
                                        <img src="img/limited_gift.png" class="img-responsive">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <p>(Redemption is based on limited BETA availability)</p>
                            </div>
                            <div class="visitors" id="visitors" style="display: none;">
                                <div class="visitors-c">
                                    <p><span class="visitor_no">100</span> other people are on this page right now</p>
                                    <a href=""><div class="visitors-a">
                                            X  
                                        </div></a>
                                </div>
                            </div>
                            <div>
                                <p class="info_msg_booking">Message & Data Rates May Apply. Reply STOP to cancel. Reply HELP for info. Consent is not any condition of any purchase.</p>
                                <p class="booking_msg">Booking Engine Gift Cards are usable for up to one year from date of issue.</p>
                            </div>



                        </center>
                    </div>
                    <!--RIGHT SIDE : END-->
                </div>
                <a href="" data-toggle="modal" data-target="#myModal2"><div class="back-to-home"><img src="img/back.png">Back To Home</div></a>
            </div>
        </div>      
    </div>
</div>

<div class="footer">
    <div class="container-fluid">
        <p><img src="img/copyright.png">&nbsp;2018 Travel Jolly. All Right Reserved </p>
    </div>
</div>

<!-- </div> -->

<!-- body -->


  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->

<!-- <div class="bottomMenu">
  <button data-toggle="modal" data-target="#myModal">Door 1</button>
  <button data-toggle="modal" data-target="#myModal">Door 2</button>
</div> -->



<!-- Modal1-->
<div id="myModal" class="modal fade ab" role="dialog">
    <div class="modal-dialog modal2-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <!-- <div class="modal-body"> -->
            <div class="fifth fifth-modal">
                <div class="container-fluid">
                    <div class="gift-section">
                        <div class="col-md-12">

                            <div class="doors doors-sm doors-xxs">

                                <div class="col-md-12 doors-p"> 
                                    <div class="door-1-lap">
                                        <div class="dl-head">
                                            <h3>Register, Claim & Save Your Gift Card While Supplies Last! </h3>
                                            <p>Once Registered Gift Cards are redeemable for 1 year from date of issue</p>
                                        </div>
                                        <div class="dl-body">
                                            <div class="col-md-6 col-sm-6 col-xs-6 now-small">
                                                <img src="img/now-never.png">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 now-small">
                                                <div class="dl-body-r">
                                                    <h3>Your Gift Card Number :</h3>
                                                    <input type="" name="" placeholder="4789 - 9869 - 8989 - 7878">
                                                    <p>Unique to you, One card per registration</p>
                                                    <div class="dl-body-r-bottom">
                                                        <h4>PIN : </h4><input type="text" name="" placeholder="#PCGH">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="dl-body-p">
                                            <center><p>NOTE: NO OBLIGATION NECESSARY TO  CLAIM GIFT</p></center>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="dl-body-bottom">
                                            <div class="col-md-6 col-sm-6 col-xs-6 dl-b-yout dl-b-yout-small">
                                                <a href="http://traveljolly.com/explainervideo.html" target="_blank"><img src="img/youtube-1.png"></a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 dl-b-r-small1">
                                                <div class="dl-body-bottom-r">
                                                    <div class="dl-body-f">
                                                        <h4>Login To :</h4><img src="img/facebook.png">
                                                    </div>
                                                    <div class="dl-body-b">
                                                        <h4>Or Sign In With :</h4>
                                                        <div class="dl-body-ul">
                                                            <ul>
                                                                <li><img src="img/twitter.png"></li>
                                                                <li><img src="img/gmail.png"></li>
                                                                <li><img src="img/mail.png"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="dl-body-p dl-p-xs">

                                            <p class="dl-p1">WE NEVER POST WITHOUT PERMISSION </p><p class="dl-p2"><a href="" class="d-h-a">DONT HAVE ACCOUNT?</a>&nbsp;&nbsp;<a href="">CREATE AN ACCOUNT</a></p>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-modal" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<!-- Modal2 -->
<div id="myModal1" class="modal fade ab" role="dialog">
    <div class="modal-dialog modal2-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <!-- <div class="modal-body"> -->
            <!-- <div class="fifth door2-bg"> -->
            <div class="fifth fifth-modal">
                <div class="container-fluid">
                    <div class="gift-section">
                        <div class="col-md-12 xs">

                            <div class="g-day">


                            </div>
                            <div class="clearfix"></div>
                            <div class="doors ds doors-xxs">

                                <div class="col-md-12 col-xs-12"> 
                                    <div class="door-1-lap door-l">
                                        <div class="d2-content">
                                            <div class="d2-head">
                                                <a href="http://traveljolly.com/explainervideo.html" target="_blank"><img src="img/play.png"></a>
                                            </div>
                                            <div class="d2-phone">
                                                <div class="d2-img">
                                                    <img src="img/phone.png"> 
                                                </div>
                                                <div class="d2-p">
                                                    <p>1-407-269-8858</p>
                                                </div>
                                            </div>
                                            <div class="d2-phone d2-phone-xs">
                                                <input type="text" name="share_code" placeholder="Call For Share Code Now!">
                                                <button type="button" id="share_codebtn">Go</button>
                                            </div>
                                        </div>

                                        <div class="dl-body-p dl-mar">
                                            <center><p>NOTE: NO OBLIGATION NECESSARY TO  CLAIM GIFT</p></center>
                                        </div>
                                    </div>

                                    <!--  <div class="door-1">
                                       <a href="" data-toggle="modal" data-target="#myModal">
                                         <img src="img/d-11.png" title="click to open the door">
                                       </a>  
                                     </div>
                                     <div class="door-2">
                                       <a href="" data-toggle="modal" data-target="#myModal1">
                                         <img src="img/d-22.png" title="click to open the door">
                                       </a>
                                     </div> -->

                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-modal" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<!-- modal 3 -->
<!-- Modal3-->
<div id="myModal2" class="modal fade ab" role="dialog">
    <div class="modal-dialog modal2-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <h4 class="modal-title"><span>FAT SAVINGS</span> SLIM PRICES</h4>
            </div>
            <!-- <div class="modal-body"> -->
            <div class="fifth fifth-modal card-s">
                <div class="container-fluid">
                    <div class="gift-section">
                        <div class="col-md-12">

                            <div class="doors doors-sm doors-xxs ">

                                <div class="col-md-12 doors-p"> 
                                    <div class="door-1-lap cards-section border-none">
                                        <div class="cards-left">
                                            <img src="img/card-vip.png">
                                        </div>
                                        <div class="cards-right">
                                            <h3>INDUSTRIES</h3>
                                            <h4>UPGRADED</h4>
                                            <div class="cr-client">
                                                <div class="cr-client-l">
                                                    <img src="img/g-1.png">
                                                </div>
                                                <div class="cr-client-r">
                                                    <img src="img/g-2.png">
                                                </div>
                                            </div>
                                            <div class="cr-client">
                                                <div class="cr-client-l">
                                                    <img src="img/g-3.png">
                                                </div>
                                                <div class="cr-client-r">
                                                    <img src="img/g-4.png">
                                                </div>
                                            </div>
                                            <div class="cr-client">
                                                <div class="cr-client-l">
                                                    <img src="img/g-5.png">
                                                </div>
                                                <div class="cr-client-r">
                                                    <p>.. and many MORE!</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="cl-r-btm">
                                            <img src="img/top-deal.png">                   
                                        </div>
                                        <div class="cl-r-btm-r">
                                            <img src="img/travel.png">
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="index.php"> <button type="button" class="btn btn-default btn-modal">Close</button></a>
            </div>
        </div>

    </div>
</div>


</body>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="jquery.sticky.js"></script>
<script type="text/javascript">

                                        $(document).ready(function () {

                                            var visitors = 98;
                                            setInterval(function () {

                                                $(".visitors").fadeIn("slow");
                                                if (visitors > 300)
                                                {
                                                    $(".visitor_no").text(100);
                                                    visitors = 100;
                                                }
                                                else {
                                                    $(".visitor_no").text(visitors);
                                                }
                                                visitors += 13;
                                                $(".visitors").fadeOut(4000);
                                            }, 5000);


                                            var tech = window.location.search.substring(1);
                                            if (tech == 'gift_section') {
                                                $(".down_part").show();
                                                $(".up_part").hide();
                                                $(".head").hide();

                                            } else {
                                                var lastScrollTop = 0;
                                                $(window).scroll(function (event) {
                                                    var st = $(this).scrollTop();
                                                    if (st > lastScrollTop) {
                                                        // downscroll code 
                                                        window.setTimeout(function () {
                                                            window.location.replace("index100.php?gift_section");
                                                        }, 2000);
                                                    }

                                                    lastScrollTop = st;
                                                });
                                            }
                                            $('#share_codebtn').click(function () {
                                                var share_code = $('input[name=share_code]').val();
                                                // alert(share_code); 
                                                if (share_code == "")
                                                {
                                                    alert("Please enter your code");
                                                    return false;

                                                }
                                                else
                                                {
                                                    window.open("https://join.shoretel.com/conference/" + share_code, '_blank');
                                                }
                                            });


                                            var phoneBar = $('.phone_number');
                                            var phCount = 0;

                                            function printLetter(string, el) {
                                                var arr = string.split(''),
                                                        input = el,
                                                        origString = string,
                                                        curPlace = $(input).attr("placeholder"),
                                                        placeholder = curPlace + arr[phCount];

                                                setTimeout(function () {
                                                    $(input).attr("placeholder", placeholder);
                                                    phCount++;
                                                    if (phCount < arr.length) {
                                                        printLetter(origString, input);
                                                    } else {
                                                        phCount = 0;
                                                        $(phoneBar).attr("placeholder", "");
                                                        printLetter("MOBILE NUMBER HERE      ", phoneBar);
                                                    }
                                                }, 400);
                                            }

                                            function placeholder() {
                                                if ($(phoneBar).val() == '') {
                                                    $(phoneBar).attr("placeholder", "");
                                                    phCount = 0;
                                                    printLetter("MOBILE NUMBER HERE      ", phoneBar);
                                                }
                                            }
                                            placeholder();
                                            get_part8();
                                        });

                                        function get_part8() {
                                            $('.card_pin_icon').hide();
                                            $('.store_icon').show();
                                            $(".customer_phone").html($(".phone_number").val());
                                            $(".middle_part_card > div").hide();
                                            $('.card_main_div').css('background-color','#003580'); //BLUE
                                            $('.gift_249_text').css('color','#f1d653');
                                            $('.sposor_text').css('color','#f1d653');
                                            $(".part1").show();
                                        }
</script>
<script type="text/javascript">
    (function blink() {
        $('.blink_me').fadeOut(500).fadeIn(500, blink);
    })();
</script>
<!-- Start of REVE Chat Script-->
<script type='text/javascript'>
    window.$_REVECHAT_API || (function (d, w) {
        var r = $_REVECHAT_API = function (c) {
            r._.push(c);
        };
        w.__revechat_account = '9571569';
        w.__revechat_version = 2;
        r._ = [];
        var rc = d.createElement('script');
        rc.type = 'text/javascript';
        rc.async = true;
        rc.setAttribute('charset', 'utf-8');
        rc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.revechat.com/widget/scripts/new-livechat.js?' + new Date().getTime();
        var s = d.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(rc, s);
    })(document, window);
</script>
<!-- End of REVE Chat Script -->
</html>